--transformlu olanlar haritalar = {"@435425", "@435427", "@435430", "@435433","200","201","202","203","204","205","206","207","208","209","210"}
-- DİLlER --
ceviriler = {
	TR = {
		giris = '<fc>#transform mini oyununa hoş geldin!<br><vp>https://mforum.ist/threads/official-minigame-transform.4345/',
		yardimac = "Yardım aç",
		yardimkapa = "Yardım kapa",
		kkutu = "Küçük kutu",
		bkutu = "Büyük kutu",
		kkalas = "Kısa kalas",
		bkalas = "Uzun kalas",
		ors = "Örs",
		top = "Top",
	},
	EN = {
		giris = '<fc>Welcome to the #transform minigame!<br><vp>https://mforum.ist/threads/official-minigame-transform.4345/',
		yardimac = "Open help",
		yardimkapa = "Hide help",
		kkutu = "Small box",
		bkutu = "Large box",
		kkalas = "Small plank",
		bkalas = "Large plank",
		ors = "Anvil",
		top = "Beach ball",
	}
}
dil = tfm.get.room.community
if not ceviriler[dil] then
    dil = 'EN' -- eğer o dil yoksa gavurların dilini yazacak
end

function mesajGetir(mesajAdi)
    return ceviriler[dil][mesajAdi]
end
function ana()
    yasayanlar = 0
    tfm.exec.disableAutoShaman(true)
    tfm.exec.disableAutoNewGame(true)
    tfm.exec.setRoomMaxPlayers(15)
    tfm.exec.setUIMapName("<b>#transform</b>")
    for kik in pairs(tfm.get.room.playerList) do
        eventNewPlayer(kik)
    tfm.exec.newGame("#14", true)
end
end

function eventNewGame()
    tfm.exec.disableAutoShaman(true)
    tfm.exec.disableAutoNewGame(true)
    tfm.exec.disableAutoTimeLeft(true)
    tfm.exec.disablePhysicalConsumables(true)
    tfm.exec.setRoomMaxPlayers(15)
    yasayanlar = 0
    for kik in pairs(tfm.get.room.playerList) do
        yasayanlar = yasayanlar + 1
    end
    tfm.exec.setUIMapName("<b>#transform</b>")
end

function eventNewPlayer(kik)
    tfm.exec.chatMessage(mesajGetir("giris"),kik)
end

function eventPlayerWon(n)
    yasayanlar = yasayanlar - 1
    if yasayanlar <= 0 then
        tfm.exec.newGame("#14", true)
    end
end
function eventPlayerLeft(n)
    yasayanlar = yasayanlar - 1
    if yasayanlar <= 0 then
        tfm.exec.newGame("#14", true)
    end
end
function eventPlayerDied(n)
    yasayanlar = yasayanlar - 1
    if yasayanlar <= 0 then
        tfm.exec.newGame("#14", true)
    end
end

function eventLoop(t,r)
    if r <= 0 then
        tfm.exec.newGame("#14", true)
    end
end

ana()