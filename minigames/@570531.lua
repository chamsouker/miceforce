function eventNewPlayer(n)
    tfm.exec.addImage("mapbackground-png.21945", "?1", 0, 0, n)
    tfm.exec.addImage("whatever.21946", "!2", 0, 0, n)
    tfm.exec.addImage("dance-icondance-png.21989", "?3", 100, 180, n)
    tfm.exec.addImage("clap-iconclap-png.21992", "?4", 450, 50, n)
    tfm.exec.addImage("laugh-iconlaugh-png.21993", "?5", 570, 50, n)

    ui.addTextArea(0, "<p align=\"center\"><a href=\"event:click\"><font size=\"18\" color=\"#000\">"..clickCount.."/25</font></p>\n\n\n", n, 583, 310, 150, 60, 0x324650, 0x000000, 0, true)

end

function eventNewGame()
    clickCount = 0
    tfm.exec.addPhysicObject(1,658,341,{
        type=10,
        restitution=0.2,
        friction=0.3,
        width=160,
        height=70,
        miceCollision=true,
        groundCollision=true
    })

    for n,p in pairs(tfm.get.room.playerList) do
        eventNewPlayer(n)
    end
end

function eventTextAreaCallback(id, n, cb)
    if cb == "click" then
        if clickCount < 25 then
            if (math.abs(655 - tfm.get.room.playerList[n].x) < 100 and math.abs(305 - tfm.get.room.playerList[n].y) < 100) then
                clickCount = clickCount + 1
                ui.addTextArea(0, "<p align=\"center\"><a href=\"event:click\"><font size=\"18\" color=\"#000\">"..clickCount.."/25</font></p>\n\n\n", nil, 583, 310, 150, 60, 0x324650, 0x000000, 0, true)
            end
        else
            ui.removeTextArea(0)
            tfm.exec.removePhysicObject(1)
        end
    end
end

function eventEmotePlayed(n, id)
    if id == 0 and (math.abs(111 - tfm.get.room.playerList[n].x) < 50 and math.abs(225 - tfm.get.room.playerList[n].y) < 20) then
        tfm.exec.movePlayer(n,370,360,false,0,0,false)
        tfm.exec.displayParticle(37,370,360,0,0,0,0)

    elseif id == 5 and (math.abs(465 - tfm.get.room.playerList[n].x) < 50 and math.abs(85 - tfm.get.room.playerList[n].y) < 20) then
        tfm.exec.movePlayer(n,580,85,false,0,0,false)
        tfm.exec.displayParticle(35,560,85,0,0,0,0)
        
    elseif id == 1 and (math.abs(590 - tfm.get.room.playerList[n].x) < 50 and math.abs(85 - tfm.get.room.playerList[n].y) < 20) then
        tfm.exec.movePlayer(n,115,82,false,0,0,false)
        tfm.exec.displayParticle(37,115,82,0,0,0,0)
    end
end
