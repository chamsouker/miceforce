admins = {Kmlcan = math.huge, Kinta = 1, Soulzone = 1, Sevenops = 1, Lee = 1, Invalnorious = 1}
players = {}
propList = {}
commands = {"title", "pw", "max", "prop", "m", "map", "score", "c", "p", "o", "r", "respawn", "afk", "help", "alive"}
maps = {407776, 408034, 408049, 408447, 415252, 416704, 417074, 434418}
game = {
	spawn = {},
	time = 0,
	remaining = 0,
	length = 1600,
	--newSeeker = true
}

text = {
	en = {
		help1 = "<N>As a mouse, <BL>hide from the seeker<N>! Press <ROSE>E <N>on an object to turn into it, and press <ROSE>space <N>to <BL>completely hide yourself<N>. You can change your foreground - background setting by clicking on your prop <CH>when you're locked in<N>.\n\n\n\n\n\n\n\n\n\nAt 30 seconds remaining everyone will get cheese, so try to rush to the hole and <R>don't let the seeker catch you<N>! After there's 15 seconds left <BL>you won't be able to hide anymore<N>, so be careful!\n\nUse <ROSE>!c [text] <N>to send a message that the seeker can't see.",
		help2 = "<N>When you're the seeker you will spawn after 30 seconds. <R>Do not forget to move when the screen is dark<N>, or you might die to the AFK protection.\n\nYou can press <ROSE>space <N>to look for people near you. You will have three lives when the round starts. If you don't find anyone after 3 presses you will die, so you have to be careful while looking!\n\n\n\n\n\n\n\n\n\nYou can double jump by <J>holding down your jump button<N> and <J>releasing it<N>, this will help you chase the rebels who don't want to lock in!",
		help3 = "<N>Seekers are given three different ranges to kill the mice. <BL>The highest range is 100<N>. They can kill the mice from this range when they're not locked in. When there's less than 30 seconds left, <R>their range will lower to 50<N>.\n\n<BL>The lowest range is 25 <N>and it applies to players that are locked in, which is the reason for seekers needing to go next to the props to look for the hidden mice.\n\n<VP><p align='center'><font face='Courier New' size='20'><b>C O M M A N D S</b></font></p><textformat tabstops='[90]'><J>!afk\t<N>Prevents adding rounds to your stats while being AFK.\n<J>!alive\t<N>Shows the players that are still alive.\n<J>!c [text]\t<N>Send a message that the seeker can't see.\n<J>!help\t<N>Shows this window.\n<J>!p [name]\t<N>Check someone's profile.\n\n<p align='center'><BL>This minigame was created by <CH><a href='event:linkKmlcan'>Kmlcan</a><BL>. Please report any bugs to him.</p>",
	},
}

props = {
	grass = {"26a8dd48ca5711e7b5f688d7f643024a.png", "121d08feca5711e7b5f688d7f643024a.png", -34, -27, 5},
	bush = {"3641f8b6ca5711e7b5f688d7f643024a.png", "2dd37d80ca5711e7b5f688d7f643024a.png", -30, -10, 0},
	beer = {"cbdf78d8ca5911e7b5f688d7f643024a.png", "d625c450ca5911e7b5f688d7f643024a.png", -6, -14, 31},
	red = {"red_left.5733", "red_right.5734", -6, -8, 11},
	blue = {"blue_left.5728", "blue_right.5730", -7, -3, 3},
	skull = {"skull_left.5738", "skull_right.5739", -16, -8, 106},
	sign = {"sign_left.5743", "sign_right.5744", -24, -39, 4},
	barrel = {"a821dff6ca5b11e7b5f688d7f643024a.png", "9d825030ca5b11e7b5f688d7f643024a.png", -26, -46, 103},
	umbrella = {"059c6294ca5e11e7b5f688d7f643024a.png", "fa93e61aca5d11e7b5f688d7f643024a.png", -51, -54, 7},
	chicken = {"6212d436ca5e11e7b5f688d7f643024a.png", "6a0d3708ca5e11e7b5f688d7f643024a.png", -25, -32, 21},
	bear = {"477cb03cca5f11e7b5f688d7f643024a.png", "3d3a83baca5f11e7b5f688d7f643024a.png", -34, -46, 26},
	candle = {"175e3068ca6111e7b5f688d7f643024a.png", "21ad89d8ca6111e7b5f688d7f643024a.png", -12, -20, 55},
	torch = {"7375811cca6111e7b5f688d7f643024a.png", "7e11c1a8ca6111e7b5f688d7f643024a.png", -35, -111, 44},
	alchemy = {"b98c5892ca6111e7b5f688d7f643024a.png", "c3872bbaca6111e7b5f688d7f643024a.png", -40, -46, 93},
	plant = {"2dbe8d8eca6211e7b5f688d7f643024a.png", "39b7b142ca6211e7b5f688d7f643024a.png", -27, -53, 20},
	flower = {"7eed233cca6211e7b5f688d7f643024a.png", "6f0477c2ca6211e7b5f688d7f643024a.png", -18, -22, 32},
	tree = {"dd32174cca6511e7b5f688d7f643024a.png", "e8e11a52ca6511e7b5f688d7f643024a.png", -72, -193, 51},
	seed = {"3921f4a4ca6c11e7b5f688d7f643024a.png", "4682283aca6c11e7b5f688d7f643024a.png", -19, -16, 2},
	palm = {"620f665eca7f11e7b5f688d7f643024a.png", "533c9c3cca7f11e7b5f688d7f643024a.png", -25, -141, 6, -80},
	blloon = {"87f8fcaeca7f11e7b5f688d7f643024a.png", "90fc385cca7f11e7b5f688d7f643024a.png", -39, -128, 40},
	thorns = {"thorns_right.5732", "thorns_left.5731", -31, -4, 12},
	shelf = {"6b1f288aca8211e7b5f688d7f643024a.png", "746efce4ca8211e7b5f688d7f643024a.png", -36, -119, 22},
	snowman = {"4ee36702ca8311e7b5f688d7f643024a.png", "573997aaca8311e7b5f688d7f643024a.png", -38, -58, 50},
	vase = {"bf95e150ca8311e7b5f688d7f643024a.png", "c5f0dd02ca8311e7b5f688d7f643024a.png", -28, -26, 88},
	bucket = {"6c3b87b6ca8411e7b5f688d7f643024a.png", "750298d0ca8411e7b5f688d7f643024a.png", -10, -4, 10},
	shelf2 = {"830b3d74ca8411e7b5f688d7f643024a.png", "8a4b5830ca8411e7b5f688d7f643024a.png", -17, -26, 33},
	seaplant = {"c9d41a04ca8511e7b5f688d7f643024a.png", "d352cf8aca8511e7b5f688d7f643024a.png", -37, -17, 78},
	rocks = {"469b0596ce7a11e7b5f688d7f643024a.png", "39fa01f2ce7a11e7b5f688d7f643024a.png", -95, -42, 84},
	rocks2 = {"1fbcf1fcca8611e7b5f688d7f643024a.png", "24fa275cca8611e7b5f688d7f643024a.png", -50, 0, 83},
	bats = {"6adb3004ca8611e7b5f688d7f643024a.png", "63966e30ca8611e7b5f688d7f643024a.png", -78, -44, 43},
	seabush = {"3ad8b544ca9911e7b5f688d7f643024a.png", "565a4a58ca9911e7b5f688d7f643024a.png", -24, -63, 87},
	fridge = {"f32b6122caef11e7b5f688d7f643024a.png", "03722bd8caf011e7b5f688d7f643024a.png", -35, -98, 28},
	elise = {"dd24969acaf011e7b5f688d7f643024a.png", "e884a822caf011e7b5f688d7f643024a.png", -21, -42, 115},
	fence = {"fence_left.5736", "fence_right.5737", -24, -35, 13},
	chest = {"6001ab16caf111e7b5f688d7f643024a.png", "680b36b0caf111e7b5f688d7f643024a.png", -32, -27, 80},
	broom = {"f6245030caf111e7b5f688d7f643024a.png", "01e40f5acaf211e7b5f688d7f643024a.png", -33, -73, 37},
	lchair = {"6410ab70caf211e7b5f688d7f643024a.png", "5bde1ececaf211e7b5f688d7f643024a.png", -22, -49, 67},
	fence2 = {"65e1a092caf411e7b5f688d7f643024a.png", "6f5cae28caf411e7b5f688d7f643024a.png", -77, -53, 47},
	pumpkin = {"bbf03ddccb0711e7b5f688d7f643024a.png", "c96c2796cb0711e7b5f688d7f643024a.png", -33, -36, 48},
	tv = {"10367b7ccb0811e7b5f688d7f643024a.png", "176d1cf2cb0811e7b5f688d7f643024a.png", -45, -68, 30},
	radio = {"64fd7eb2cb0811e7b5f688d7f643024a.png", "5dd7edd4cb0811e7b5f688d7f643024a.png", -22, -29, 25},
	couch = {"d22d2212cb0811e7b5f688d7f643024a.png", "d93d3c0ecb0811e7b5f688d7f643024a.png", -34, -43, 19},
	flowers = {"368e2aeecb0911e7b5f688d7f643024a.png", "3f1e713ccb0911e7b5f688d7f643024a.png", -21, -34, 18},
	von = {"e5363e18ce7611e7b5f688d7f643024a.png", "db9863e0ce7611e7b5f688d7f643024a.png", -44, -88, 112},
	stone = {"7cb20970ce7711e7b5f688d7f643024a.png", "71044110ce7711e7b5f688d7f643024a.png", -33, -105, 46},
	sleigh = {"1660cbd8ce7811e7b5f688d7f643024a.png", "0f651802ce7811e7b5f688d7f643024a.png", -67, -49, 64},
	coffin = {"586056c0ce7811e7b5f688d7f643024a.png", "60957c1cce7811e7b5f688d7f643024a.png", -31, -108, 98},
	sock = {"c839b2f2ce7811e7b5f688d7f643024a.png", "c039c9d4ce7811e7b5f688d7f643024a.png", -13, -37, 54},
	--shovel = {"0f121c0ace7911e7b5f688d7f643024a.png", "165c7848ce7911e7b5f688d7f643024a.png", -15, 8, 8},
	castle = {"6f06284ace7911e7b5f688d7f643024a.png", "67f2d990ce7911e7b5f688d7f643024a.png", -25, -11, 8},
	menu = {"b5007648ce7911e7b5f688d7f643024a.png", "bcb2942ace7911e7b5f688d7f643024a.png", -34, -58, 69},
	rose = {"fd6d5734ce7911e7b5f688d7f643024a.png", "f68cab4ace7911e7b5f688d7f643024a.png", -9, -24, 73},
	normtree = {"tree_left.5741", "tree_right.5742", -71, -187, 1}
}

function main()
	tfm.exec.disableAutoShaman(true)
	tfm.exec.disableAutoNewGame(true)
	tfm.exec.disableAutoTimeLeft(true)
	tfm.exec.disablePhysicalConsumables(true)
	tfm.exec.disableAutoScore(true)

	local com = tfm.get.room.community:lower()
	game.lang = text[com] and com or "en"

	for n in pairs(tfm.get.room.playerList) do eventNewPlayer(n) end
	for k, v in pairs(commands) do system.disableChatCommandDisplay(v) end

	tfm.exec.newGame(maps[math.random(#maps)])
end

function eventNewGame()
	ui.removeTextArea(100, getShaman())
	ui.removeTextArea(101, getShaman())
	local high, highP = -1
	propList = {}
	game.started = nil
	game.first = nil
	checkProps()
	addGrounds()

	ui.removeTextArea(0)
	ui.removeTextArea(1)

	for k, v in pairs(players) do
		if tfm.get.room.playerList[k] then
			v.isShaman = nil
			v.isDead = nil
			v.prop = nil
			v.lock = nil
			v.hearts = {}
			if not v.isAFK then
				v.score = tfm.get.room.playerList[k].score

				if v.score > high and not game.newSeeker then
					high = v.score
					highP = k
				end
			end
		end
	end

	for k, v in pairs(tfm.get.room.playerList) do
		if k == highP then
			players[k].stats.roundSeeker = players[k].stats.roundSeeker + 1
			if getUI(k) and getUI(k) == 1 then
				showProfile(k)
			end
		else
			if not players[k].isAFK then
				players[k].stats.roundMouse = players[k].stats.roundMouse + 1
			else
				tfm.exec.killPlayer(k)
			end
		end
		savePlayerData(k)
	end

	if highP then
		players[highP].isShaman = true
		players[highP].lastKey = nil
		tfm.exec.movePlayer(highP, 800, -340)
		ui.addTextArea(100, "", highP, -5000, -5000, 11600, 11600, 1, 1, 1)
		ui.addTextArea(101, "\n\n\n\n\n\n\n\n\n\n\n\n\n<p align='center'><font size='20'>Don't forget to move, or you might die to the AFK protection.\n<R>You haven't moved yet.", highP, 5, 5, 800, 400, 1, 1, 1)

		for i = 1, 3 do
			table.insert(players[highP].hearts, tfm.exec.addImage("be6d7448ca7211e7b5f688d7f643024a.png", "$"..highP, i * 10 - 28, -35))
		end
	end

	tfm.exec.setGameTime(180)
	setMapName()
end

function eventLoop(t, r)
	game.time, game.remaining = t, r
	if t >= 30000 and not game.started and getShaman() then
		game.started = true
		tfm.exec.movePlayer(getShaman(), game.spawn[1], game.spawn[2])
		ui.removeTextArea(100)
		ui.removeTextArea(101)
		ui.addTextArea(100, "", getShaman(), 5, 5, 800, 400, 0, 0, 0)
	end
	if r <= 0 and not game.newSeeker then
		if getShaman() then
			tfm.exec.setPlayerScore(getShaman(), 0)
		end
		tfm.exec.newGame(maps[math.random(#maps)])
	elseif r >= 29500 and r <= 30000 then
		for k, v in pairs(tfm.get.room.playerList) do
			local p = players[k]
			if not p.isDead and not p.isShaman then
				tfm.exec.giveCheese(k)
			end
		end
	elseif r >= 14500 and r <= 15100 then
		for k, v in pairs(tfm.get.room.playerList) do
			local p = players[k]
			if not p.isDead and not p.isShaman then
				if p.lock then
					tfm.exec.removeImage(p.fixedIMG)
					tfm.exec.movePlayer(k, p.x, p.y)
					p.lock = nil
					p.lastLock = os.time()
				end
			end
		end
	end
end

function eventNewPlayer(n)
	players[n] = {
		helpPage = 0,
		isAFK = false,
		-- DATA
		id = system.bilgi(n).code,
		data = "",
		lastLock = os.time(),
		lastJump = os.time(),
		fixedIMG = -1,
		lastKey = 2,
		hearts = {},
		isDead = true,
		propChoices = {},
		stats = {
			roundMouse = 0,
			surviveMouse = 0,
			wonMouse = 0,
			usedProps = 0,
			roundSeeker = 0,
			wonSeeker = 0,
			killedMice = 0,
		}
	}

	for _, k in pairs({0, 1, 2, 32, 69, 72, 80}) do
		tfm.exec.bindKeyboard(n, k, true, true)
	end
	system.loadPlayerData("prophunt_"..players[n].id)
	system.bindMouse(n, true)
	tfm.exec.bindKeyboard(n, 1, false, true)
	ui.removeTextArea(999, n)
	tfm.exec.chatMessage("<J>Welcome to #prophunt, <CH>"..n.."<J>. Type <CH>!help <J>or press <CH>H <J>for more information about the minigame.", n)
	addGrounds()
end

function eventPlayerWon(n)
	local p = players[n]
	if n ~= getShaman() and not game.first then
		game.first = true
	end
	tfm.exec.setPlayerScore(n, countAlive(), true)

	if n == getShaman() then
		p.stats.wonSeeker = p.stats.wonSeeker + 1
	else
		p.stats.wonMouse = p.stats.wonMouse + 1
	end
	eventPlayerDied(n)
end

function eventPlayerDied(n, won)
	local p, ch = players[n], getShaman()
	if p and p.isDead then return end
	--if not game.newSeeker then
		p.isDead = true
	--end
	setMapName()
	savePlayerData(n)
	if countAlive() == 0 then
		if ch then
			tfm.exec.setPlayerScore(ch, 0)
		end
		tfm.exec.setGameTime(5)
		tfm.exec.newGame(maps[math.random(#maps)])
	elseif p.isShaman and not game.first then
		for k, v in pairs(tfm.get.room.playerList) do
			tfm.exec.giveCheese(k)
			players[k].stats.surviveMouse = players[k].stats.surviveMouse + 1
		end
		tfm.exec.setGameTime(30)
	else
		if countAlive() == 1 and ch and not players[ch].isDead and not game.first then
			tfm.exec.giveCheese(ch)
			tfm.exec.setGameTime(30)
		end
	end
end

function eventPlayerLeft(n)
	eventPlayerDied(n)
	players[n] = nil
end

function eventMouse(n, x, y)
	local p = players[n]
	if p.x and p.lock then
		if math.abs(p.x - x) <= 25 and math.abs(p.y - y) <= 25 then
			p.foreground = not p.foreground
			tfm.exec.removeImage(p.fixedIMG)
			local prop = props[p.prop]
			p.fixedIMG = tfm.exec.addImage(props[p.prop][math.ceil(p.lastKey / 2 + 1)], (p.foreground and "!" or "?")..999, p.x + (p.lastKey == 0 and (prop[6] or prop[3]) or prop[3]), p.y + props[p.prop][4])
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	local p = players[n]
	if has(k, 0, 2) and not p.isDead then
		if p.lock or p.lastKey and k == p.lastKey then return end

		if p.isShaman and not game.started and game.time > 3500 then
			ui.addTextArea(101, "\n\n\n\n\n\n\n\n\n\n\n\n\n<p align='center'><font size='20'>Don't forget to move, or you might die to the AFK protection.\n<VP>You succesfully moved.", n, 5, 5, 800, 400, 1, 1, 1)
			if getUI(n) then
				if getUI(n) == 1 then
					showProfile(n)
				elseif getUI(n) == 0 then
					showHelp(n)
				end
			end
		end

		p.lastKey = k
		if p.propIMG then
			tfm.exec.removeImage(p.propIMG)
		end
		if p.prop then
			local prop = props[p.prop]
			p.propIMG = tfm.exec.addImage(prop[math.ceil(k / 2 + 1)], "%"..n, p.lastKey == 0 and (prop[6] or prop[3]) or prop[3], prop[4])
		end
	elseif k == 1 and p.isShaman and os.time() - p.lastJump >= 1500 then
		if not game.started and game.time > 3100 then
			ui.addTextArea(101, "\n\n\n\n\n\n\n\n\n\n\n\n\n<p align='center'><font size='20'>Don't forget to move, or you might die to the AFK protection.\n<VP>You succesfully moved.", n, 5, 5, 800, 400, 1, 1, 1)
			if getUI(n) then
				if getUI(n) == 1 then
					showProfile(n)
				elseif getUI(n) == 0 then
					showHelp(n)
				end
			end
		end

		if d then
			p.jumped = os.time()
		else
			if p.jumped and os.time() - p.jumped >= 400 then
				tfm.exec.movePlayer(n, 0, 0, nil, 0, -60)
				p.lastJump = os.time()
			end
		end
	elseif k == 32 then
		if p.isShaman then
			if game.started then
				local killed = 0
				for k, v in pairs(players) do
					local pN = tfm.get.room.playerList[k]
					if not v.isDead then
						if v.lock then
							if math.abs(x - v.x) <= 25 and math.abs(y - v.y) <= 25 then
								tfm.exec.killPlayer(k)
								tfm.exec.removeImage(v.fixedIMG)
								killed = killed + 1
								p.stats.killedMice = p.stats.killedMice + 1
							end
						else
							if pN and math.abs(x - pN.x) <= (game.remaining <= 30000 and 50 or 100) and math.abs(y - pN.y) <= (game.remaining <= 30000 and 50 or 100) and k ~= n then
								tfm.exec.killPlayer(k)
								killed = killed + 1
								p.stats.killedMice = p.stats.killedMice + 1
							end
						end
					end
				end
				if killed == 0 and not p.isDead then
					tfm.exec.removeImage(players[n].hearts[1])
					table.remove(players[n].hearts, 1)
					if #players[n].hearts == 0 then
						tfm.exec.killPlayer(n)
					end
				end
			end
		else
			if not game.newSeeker and game.remaining <= 15000 or p.isDead or not p.prop then return end
			if os.time() - p.lastLock >= 1500 or p.lock then
				p.lock = not p.lock
				p.lastLock = os.time()
				if p.lock then
					p.x, p.y = x, y
					local prop = props[p.prop]
					p.fixedIMG = tfm.exec.addImage(props[p.prop][math.ceil(p.lastKey / 2 + 1)], (p.foreground and "!" or "?")..999, x + (p.lastKey == 0 and (prop[6] or prop[3]) or prop[3]), y + props[p.prop][4])
					tfm.exec.movePlayer(n, x <= 800 and math.max(x, 400) or math.min((game.length - 400), x), -340)
				else
					tfm.exec.removeImage(p.fixedIMG)
					tfm.exec.movePlayer(n, p.x, p.y)
				end
			end
		end
	elseif k == 69 and not p.isShaman then
		changeProp(n, x, y)
	elseif k == 72 then
		showHelp(n)
	elseif k == 80 then
		showProfile(n)
	end
end

function eventChatCommand(n, c)
	local a = split(c, "%s")

	if has(a[1], "help") then
		showHelp(n)
	elseif has(a[1], "alive") and n ~= getShaman() then
		local pl = {}
		for k, v in pairs(tfm.get.room.playerList) do
			if not players[k].isDead and not players[k].isShaman then
				table.insert(pl, k)
			end
		end
		table.sort(pl)
		sendLog(n, "Alive players: "..table.concat(pl, ", ")..".")
	elseif has(a[1], "afk") then
		players[n].isAFK = not players[n].isAFK
		if players[n].isAFK then
			ui.addTextArea(102, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n<p align='center'><font size='20'>You are now AFK, type <J>!afk <N>to get back in the game.", n, 5, 5, 790, 400, 0, 1, 1)
			tfm.exec.killPlayer(n)
			if getUI(n) then
				if getUI(n) == 1 then
					showProfile(n)
				elseif getUI(n) == 0 then
					showHelp(n)
				end
			end
		else
			ui.removeTextArea(102, n)
		end
	elseif has(a[1], "p", "o") then
		local player = a[2] and a[2]:sub(1, 1):upper()..a[2]:sub(2):lower() or n
		if players[player] then
			showProfile(n, player)
		end
	elseif has(a[1], "c") and #c > 2then
		if players[n].isShaman then
			tfm.exec.chatMessage("<V>• [Seeker <a href='event:x_mj;"..n.."'>"..fix(n).."</a>] <CH>"..c:sub(3))
		else
			for k, v in pairs(tfm.get.room.playerList) do
				if not players[k].isShaman then
					tfm.exec.chatMessage((admins[n] and "<R>" or "<V>").."Ξ [<a href='event:x_mj;"..n.."'>"..fix(n).."</a>] <N>"..c:sub(3), k)
				end
			end
		end
	elseif admins[n] then
		if a[1] == "prop" and props[a[2]] then
			players[n].prop = a[2]
			players[n].lastKey = nil
		elseif a[1] == "score" and tonumber(2) then
			tfm.exec.setPlayerScore(a[3] or n, tonumber(a[2]))
		elseif has(a[1], "m", "map") then
			tfm.exec.newGame(a[2] or maps[math.random(#maps)])
		elseif has(a[1], "r", "respawn") then
			if a[2] and not players[a[2]] then return end
			tfm.exec.killPlayer(a[2] or n)
			tfm.exec.respawnPlayer(a[2] or n)
		elseif has(a[1], "cheese") then
			tfm.exec.giveCheese(a[2] or n)
		elseif has(a[1], "pw") then
			tfm.exec.setRoomPassword(a[2] or "")
			sendLog(n, "Password: "..(a[2] or ""))
		elseif has(a[1], "max") then
			tfm.exec.setRoomMaxPlayers(tonumber(a[2]) or 50)
			sendLog(n, "Player limit: "..(tonumber(a[2]) or 50))
		elseif has(a[1], "save") then
			savePlayerData(n)
		end
	end
end

function eventTextAreaCallback(i, n, c)
	if c == "close" then
		i = i - 17000
		if i == 0 then
			showHelp(n)
		elseif i == 1 then
			showProfile(n)
		end
	elseif c:sub(1, 4) == "link" then
		if c:sub(5) == "Kmlcan" then
			sendLog(n, "http://mforum.ist/members/kmlcan.30462/")
		end
	elseif c:sub(1, 4) == "help" then
		showHelp(n, c:sub(5))
	end
end

function showHelp(n, u)
	local p = players[n]
	if p.ui and p.ui[0] then
		removeButton(10, n)
		removeButton(11, n)
		ui.removeTextArea(300, n)
		ui.removeTextArea(301, n)

		if u then
			if u == "previous" then
				p.helpPage = p.helpPage > 0 and p.helpPage - 1 or p.helpPage
			elseif u == "next" then
				p.helpPage = p.helpPage < 2 and p.helpPage + 1 or p.helpPage
			end
		else
			removeUI(0, n)
			p.helpPage = 0
			return
		end
	elseif p.ui and getUI(n) and getUI(n) ~= 0 then
		return
	end

	addUI(0, "<J><p align='center'><b><font face='Courier New' size='20'>"..spaceName("PROP HUNT").."</font></b></p><font size='12'>"..text[game.lang]["help"..(p.helpPage + 1)], n, 150, 55, 500, 320)
	local x, w = p.helpPage == 0 and 165 or 285, ((p.helpPage == 0 or p.helpPage == 2) and 350) or p.helpPage > 0 and 230 or 470
	addButton(0, "<a href='event:close'>Close", n, x, 345, w)
	--addButton(0, "<a href='event:close'>Close", n, 265, 345, 270)
	if p.helpPage >= 0 and p.helpPage < 2 then
		if p.helpPage == 0 then
			table.insert(p.ui[0], tfm.exec.addImage("21db59a2cdaa11e7b5f688d7f643024a.png", "&1", 190, 142, n))
			table.insert(p.ui[0], tfm.exec.addImage("edef9d36cdab11e7b5f688d7f643024a.png", "&1", 337, 142, n))
			table.insert(p.ui[0], tfm.exec.addImage("f4f2eba6cdab11e7b5f688d7f643024a.png", "&1", 484, 142, n))
		elseif p.helpPage == 1 then
			table.insert(p.ui[0], tfm.exec.addImage("ccb86fb0cdda11e7b5f688d7f643024a.png", "&1", 190, 183, n))
		end
		addButton(10, "", n, 535, 345, 100)
		ui.addTextArea(300, "<CH><a href='event:helpnext'><p align='center'><font size='18'>»</font>\n", n, 535, 339, 100, nil, 0, 0, 0)
	end
	if p.helpPage ~= 0 then
		addButton(11, "", n, 165, 345, 100)
		ui.addTextArea(301, "<CH><a href='event:helpprevious'><p align='center'><font size='18'>«</font>\n", n, 165, 339, 100, nil, 0, 0, 0)
	end
end

function showProfile(n, n2)
	local n2 = n2 or n
	local p, p2 = players[n], players[n2]

	if p.ui and p.ui[1] then
		removeUI(1, n)
		ui.removeTextArea(200, n)
		ui.removeTextArea(201, n)
		return
	elseif p.ui and getUI(n) and getUI(n) ~= 1 then
		return
	end

	local ratio1, ratio2 = math.floor((p2.stats.wonMouse * 100) / p2.stats.roundMouse), math.floor((p2.stats.wonSeeker * 100) / p2.stats.roundSeeker)
	addUI(1, "", n, 140, 75, 520, 280)
	addButton(1, "<a href='event:close'>Close", n, 155, 325, 295)
	ui.addTextArea(200, "<p align='center'><BL>Favorite prop", n, 465, 85, 180, nil, 0, 0, 0)
	ui.addTextArea(201, "\n<"..(admins[n2] and "R" or "CH").."><p align='center'><b><font face='Courier New' size='20'>"..spaceName(n2):upper().."</font></b></p><font size='12'><J><p align='center'>Mouse stats</p><BL>• Rounds played: <V>"..p2.stats.roundMouse.."\n<BL>• Rounds survived: <V>"..p2.stats.surviveMouse.."\n<BL>• Rounds won: <V>"..p2.stats.wonMouse.."\n<BL>• Win ratio (%): <V>"..(tonumber(tostring(ratio1)) or 0).."\n<BL>• Amount of props used: <V>"..p2.stats.usedProps.."\n\n<J><p align='center'>Seeker stats</p><BL>• Rounds played: <V>"..p2.stats.roundSeeker.."\n<BL>• Rounds won: <V>"..p2.stats.wonSeeker.."\n<BL>• Win ratio (%): <V>"..(tonumber(tostring(ratio2)) or 0).."\n<BL>• Killed mice: <V>"..p2.stats.killedMice, n, 156, 75, 295, 230, 1, 1, 0)
	
	if p2.favProp then
		print(p2.favProp)
		table.insert(p.ui[1], tfm.exec.addImage(props[p2.favProp][2], "&0", 445 + props[p2.favProp][3] + 110 - (p2.favProp == "palmtree" and 22 or 0), 311 + props[p2.favProp][4], n))
	end
	table.insert(p.ui[1], tfm.exec.addImage("da736902cdc111e7b5f688d7f643024a.png", "&0", 465, 105, n))
end

function addButton(i, t, n, x, y, w)
	ui.addTextArea(14000 + i, "", n, x + 1, y + 3, w, 13, 0x11171C, 0x11171C, 1)
	ui.addTextArea(15000 + i, "", n, x - 1, y + 1, w, 13, 0x5D7D90, 0x5D7D90, 1)
	ui.addTextArea(16000 + i, "", n, x, y + 2, w, 13, 0x3C5064, 0x3C5064, 1)
	ui.addTextArea(17000 + i, "<p align='center'>"..t.."\n", n, x, y, w, 17, 0, 0, 0)
end

function addUI(i, t, n, x, y, w, h, b)
	ui.addTextArea(10000 + i, "", n, x, y, w, h, 0x241A14, 0x241A14, 0.7)
	ui.addTextArea(11000 + i, "", n, x + 1, y + 1, w - 2, h - 2, 0x986742, 0x986742, 1)
	ui.addTextArea(12000 + i, "", n, x + 4, y + 4, w - 8, h - 8, 0x24474D, 0x0C191C, 1)
	ui.addTextArea(13000 + i, t, n, x + 6, y + 6, w - 12, h - 12, 0x122528, 0x183337, 1)
	for k, v in pairs(tfm.get.room.playerList) do
		if n == nil or n == k then
			if not players[k].ui then
				players[k].ui = {}
			elseif players[k].ui[i] then
				for key, value in pairs(players[k].ui[i]) do
					tfm.exec.removeImage(value)
				end
			end
			players[k].ui[i] = {}
			table.insert(players[k].ui[i], tfm.exec.addImage("9d638314ccac11e7b5f688d7f643024a.png", "&0", x - 6, y - 7, k))
			table.insert(players[k].ui[i], tfm.exec.addImage("14519084ccac11e7b5f688d7f643024a.png", "&0", x - 6, h + y - 22, k))
			table.insert(players[k].ui[i], tfm.exec.addImage("d19ed16accac11e7b5f688d7f643024a.png", "&0", x + w - 20, y - 7, k))
			table.insert(players[k].ui[i], tfm.exec.addImage("119a2d46ccad11e7b5f688d7f643024a.png", "&0", x + w - 20, h + y -22, k))
		end
	end

	if b then
		addButton(i, b, n, x + 15, y + h - 30, w - 30)
	end
end

function removeButton(i, n)
	for k = 4, 7 do
		ui.removeTextArea(10000 + i + (k * 1000), n)
	end
end

function removeUI(i, n)
	if players[n] and players[n].ui[i] then
		for k = 0, 3 do
			ui.removeTextArea(10000 + i + (k * 1000), n)
		end
		removeButton(i, n)
		for k, v in pairs(players[n].ui[i]) do
			tfm.exec.removeImage(v)
		end
		players[n].ui[i] = nil
	end
end

function eventPlayerDataLoaded(n, d)
	if n:sub(1, 9) == "prophunt_" then
		local id, player = tonumber(n:sub(10))
		for k, v in pairs(tfm.get.room.playerList) do
			if system.bilgi(k).code == id then
				player = k
			end
		end
		if player then
			local p = players[player]
			local data = split(d, ";")
			p.stats.roundMouse = tonumber(data[1]) or 0
			p.stats.surviveMouse = tonumber(data[2]) or 0
			p.stats.wonMouse = tonumber(data[3]) or 0
			p.stats.usedProps = tonumber(data[4]) or 0
			p.stats.roundSeeker = tonumber(data[5]) or 0
			p.stats.wonSeeker = tonumber(data[6]) or 0
			p.stats.killedMice = tonumber(data[7]) or 0
			if data[8] then
				local choices = split(data[8], ",")
				for k, v in pairs(choices) do
					local t = split(v, ":")
					p.propChoices[t[1]] = tonumber(t[2])
				end
				favProp(player)
			end
		end
	end
end

function savePlayerData(n)
	if n:sub(1, 1) ~= "*" and players[n] and count(tfm.get.room.playerList) > 3 then
		local p, choices = players[n], {}

		-- roundMouse;surviveMouse;wonMouse;props;roundSeeker;wonSeeker;killedMice;choices
		for k, v in pairs(p.propChoices) do
			table.insert(choices, k..":"..v)
		end
		local data = p.stats.roundMouse..";"..p.stats.surviveMouse..";"..p.stats.wonMouse..";"..p.stats.usedProps..";"..p.stats.roundSeeker..";"..p.stats.wonSeeker..";"..p.stats.killedMice..";"..table.concat(choices, ",")
		system.savePlayerData("prophunt_"..p.id, data)
	end
end

function getUI(n)
	local p, i = players[n]
	if p and p.ui then
		for k, v in pairs(p.ui) do
			i = k
		end
	end
	return i
end

function spaceName(n)
	local p = ""
	for i = 1, #n do
		p = p..n:sub(i, i)..(i == #n and "" or " ")
	end
	return p
end

function checkProps()
	local map, i = tfm.get.room.xmlMapInfo, 0
	for k in map.xml:gmatch('<P (.-) />') do
		i = i + 1
		if i == 1 then
			game.length = k:match('LENGTH="(.-)"') or k:match('L="(.-)"') or 800
			game.length = tonumber(game.length)
		elseif i > 1 then
			table.insert(propList, {x = tonumber(k:match('X="(.-)"')), y = tonumber(k:match('Y="(.-)"')), t = tonumber(k:match('T="(.-)"')),})
		end
	end

	local x, y
	for k in map.xml:gmatch('<DS (.-) />') do
		x = tonumber(k:match('X="(.-)"'))
		y = tonumber(k:match('Y="(.-)"'))
	end

	game.spawn = {x, y}
end

function addGrounds()
	tfm.exec.addPhysicObject(0, game.length / 2, -370, {type = 10, friction = 0.2, width = game.length, height = 10, miceCollision = true})
	tfm.exec.addPhysicObject(1, game.length == 800 and 620 or game.length - 380, -340, {type = 10, friction = 0.2, width = 10, height = 50, miceCollision = true})
	tfm.exec.addPhysicObject(2, game.length / 2, -310, {type = 10, friction = 0.2, width = game.length, height = 10, miceCollision = true})
	tfm.exec.addPhysicObject(3, math.min(game.length / 4 - 20, 380), -340, {type = 10, friction = 0.2, width = 10, height = 50, miceCollision = true})
end

function countAlive(s)
	local i, dead = 0
	for k, v in pairs(players) do
		if s and v.isShaman then
			if v.isDead then
				dead = true
			end
		elseif not v.isDead then
			i = i + 1
		end
	end
	return i, dead
end

function count(t)
	local i = 0
	for k, v in pairs(t) do
		i = i + 1
	end
	return i
end

function getShaman()
	local i
	for k, v in pairs(players) do
		if v.isShaman then
			i = k
			break
		end
	end
	return i
end

function sendLog(n, m, p)
	tfm.exec.chatMessage("<V>[#] <BL>"..m..((p and " = "..tostring(p)) or (p ~= nil and " = false") or ""), n)
end

function favProp(n)
	local p, i = players[n], 0
	if players[n].stats.usedProps > 3 then
		for k, v in pairs(p.propChoices)do
			if v > i then
				i = v
				p.favProp = k
			end
		end
	end
end

function changeProp(n, x, y)
	local p, pType = players[n]
	for k, v in pairs(propList) do
		if math.abs(x - v.x) <= 20 and math.abs(y - v.y) <= 20 and v.t then
			pType = v.t
		end
	end

	for k, v in pairs(props) do
		if tonumber(pType) and v[5] == pType and (p.prop ~= k) then
			p.prop = k
			p.propChoices[k] = p.propChoices[k] and p.propChoices[k] + 1 or 1
			favProp(n)
			p.stats.usedProps = p.stats.usedProps + 1
		end
	end

	if tonumber(pType) and p.prop then
		if p.propIMG then
			tfm.exec.removeImage(players[n].propIMG)
		end
		local prop = props[p.prop]
		p.propIMG = tfm.exec.addImage(props[p.prop][math.ceil(p.lastKey / 2 + 1)], "%"..n, p.lastKey == 0 and (prop[6] or prop[3]) or prop[3], props[p.prop][4])
	end
end

function setMapName()
	local m = tfm.get.room.xmlMapInfo
	if m then
		local alive, sham = countAlive(true)
		ui.setMapName(m.author.." <BL>- @"..m.mapCode.."   <G>|   <N>Seeker : <"..(sham and "R" or "V")..">"..(getShaman() and fix(getShaman()) or "-").."   <G>|   <N>Alive : <V>"..alive)
	end
end

function fix(n)
	if n then
		local plus = n:sub(1, 1) == "+" and true
		if plus then
			n = n:sub(2)
		end
		return (plus and "+" or "")..n:sub(1, 1):upper()..n:sub(2):lower()
	end
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function has(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

main()