tfm.exec.disableAutoShaman(true)
tfm.exec.disablePhysicalConsumables(true)
system.eventMs(400)
consumables = {1,5,25,26,11,24,17,22,2,3,23,21,779,778,2257}
players = {}

translation = {
	["TR"] = {
		["introduction"] = "<ROSE>• [^-^] Balık tutmak için aşağı tuşuna veya S tuşuna basılı tut.",
		["nF"] = "Sana balık YOK! ^_^",
		["nC"] = "<j>İyi yakaladın!",
		["mN"] = "Balıkçılık 2019 <bl>- Koy",
		["hint"] = "<r><b>Bir ipucu:</b><n> Eğer bu dört çim zeminlerden birinin üstünde balık tutmazsan kalıntı parçası kazanamazsın."
	},

	["EN"] = {
		["introduction"] = "<ROSE>• [^-^] Duck or hold down the S key to fish.",
		["nF"] = "No fish for you! ^_^",
		["nC"] = "<j>Nice catch!",
		["mN"] = "Fishing 2019 <bl>- The Cove",
		["hint"] = "<r><b>Here's a hint:</b><n> You can't collect relics unless you are fishing on one of the four grass platforms."
	},

	["RO"] = {
        ["introduction"] = "<ROSE>• [^-^] Folosește săgeata în jos sau tasta S pentru a pescui.",
        ["nF"] = "Nici un pește pentru tine! ^_^",
        ["nC"] = "<j>Frumoasă captură!",
        ["mN"] = "Fishing 2019 <bl>- The Cove",
		["hint"] = "<r><b>Uitați un sfat:</b><n> Nu poți colecta relicve decât dacă ești pe una dintre platformele de sus."
	},

	["HU"] = {
        ["introduction"] = "<ROSE>• [^-^] Guggolj le vagy tartsd lenyomva az S billentyűt a halászáshoz.",
        ["nF"] = "Nem kapsz halat! ^_^",
        ["nC"] = "<j>Szép fogás!",
        ["mN"] = "Halászat 2019 <bl>- Az öböl",
		["hint"] = "<r><b>Itt egy kis segítség:</b><n> Nem tudsz addig ereklyéket gyűjteni, míg a négy füves terület egyikén nem halászol."
	},
	
	["PL"] = {
        ["introduction"] = "<ROSE>• [^-^] Kucnij lub przytrzymaj S, by zacząć łowić.",
        ["nF"] = "Nie ma ryb dla Ciebie! ^_^",
        ["nC"] = "<j>Dobry chwyt!",
        ["mN"] = "Wędkowanie 2019 <bl>- Zatoczka",
		["hint"] = "<r><b>Wskazówka:</b><n> Nie możesz zbierać relikwii, dopóki nie będziesz łowić na jednej z trawiastych platform."
	},
	
	["BR"] = {
        ["introduction"] = "<ROSE>• [^-^] Pato ou pressione a tecla S para poder pescar.", 
        ["nF"] = "Que má sorte... Sem peixes para você! ^_^",
        ["nC"] = "<j>Boooa pescada!",
        ["mN"] = "Pescaria 2019 <bl>- The Cove",
		["hint"] = "<r><b>Aqui vai uma dica:</b><n> Você não pode coletar relíquias a menos que você esteja pescando em uma das quatro plataformas/pisos de grama."
	},
	
	["ES"] = {
        ["introduction"] = "<ROSE>• [^-^] Agacharse o mantener la tecla S para pescar.",
        ["nF"] = "¡No hay peces para ti! ^_^",
        ["nC"] = "<j>¡Buena pesca!",
        ["mN"] = "Fishing 2019 <bl>- The Cove",
		["hint"] = "<r><b>Aquí va una pista:</b><n> No podrás recolectar reliquias a menos de que estés pescando en una de las cuatro platatormas de hierva."
	},
	
	["RU"] = {
        ["introduction"] = "<ROSE>• [^-^] Утка или удерживайте клавишу S, чтобы ловить рыбу.",
        ["nF"] = "Нет рыбы для тебя! ^_^",
        ["nC"] = "<j>Отличный улов!",
        ["mN"] = "Рыбалка 2019 <bl>- Бухта",
		["hint"] = "<r><b>Вот подсказка:</b><n> Вы не можете собирать реликвии, если не ловите рыбу на одной из четырех травяных платформ."
	},
	["AR"] = {
        ["introduction"] = "<ROSE>.اضغط باستمرار على مفتاح S للصيد [^-^] •",
        ["nF"] = "^_^ !لا سمك لك",
        ["nC"] = "<j>صيد جيد ",
        ["mN"] = "الكوف - <bl>الصيد 2019",
		["hint"] = "<r><n>.لا يمكنك جمع الآثار إلا إذا كنت تصطاد على احد المنصات العشبيه الاربعه </n><b>:اليك هذا التلميح</b>"
    }
}

system.community = tfm.get.room.community
if not translation[system.community] then
    system.community = "EN"
end

function getMsg(msgName)
    return translation[system.community][msgName]
end

function param(s, tab)
  return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end))
end

function table.delete(t, v)
    for k in ipairs(t) do
        if v == t[k] then
            table.remove(t, k)
        end
    end
end

function main()
	map = '<C><P Ca="" /><Z><S><S X="22" Y="268" L="58" H="246" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="46" Y="174" L="30" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="70" Y="309" L="44" H="238" T="12" P="0,0,0.3,0.2,-4,0,0,0" o="324650" m="" /><S X="56" Y="191" L="27" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="75" Y="306" L="27" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="129" Y="405" L="58" H="243" T="12" P="0,0,0.3,0.2,-53,0,0,0" o="324650" m="" /><S X="80" Y="377" L="58" H="243" T="12" P="0,0,0.3,0.2,-13,0,0,0" o="324650" m="" /><S X="432" Y="421" L="58" H="732" T="12" P="0,0,0.3,0.2,-89,0,0,0" o="324650" m="" /><S X="748" Y="400" L="133" H="31" T="12" P="0,0,0.3,0.2,-24,0,0,0" o="324650" m="" /><S X="217" Y="158" L="71" H="13" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="388" Y="156" L="121" H="11" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="624" Y="153" L="135" H="14" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="440" Y="286" L="732" H="224" T="9" P="0,0,,,,0,0,0" m="" /><S X="846" Y="204" L="93" H="402" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="-47" Y="205" L="93" H="402" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="397" Y="-6" L="994" H="26" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="136" Y="369" L="129" H="18" T="12" P="0,0,0.3,0.2,32,0,0,0" o="324650" m="" /><S X="432" Y="399" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="462" Y="406" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="637" Y="396" L="204" H="11" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="572" Y="409" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="593" Y="412" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="562" Y="412" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="272" Y="409" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="234" Y="410" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="218" Y="411" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="295" Y="412" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="191" Y="407" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="233" Y="189" L="64" H="10" T="12" P="0,0,0.3,0.2,118,0,0,0" o="324650" m="" /><S X="201" Y="190" L="64" H="10" T="12" P="0,0,0.3,0.2,62,0,0,0" o="324650" m="" /><S X="230" Y="198" L="43" H="10" T="12" P="0,0,0.3,0.2,113,0,0,0" o="324650" m="" /><S X="339" Y="177" L="35" H="10" T="12" P="0,0,0.3,0.2,65,0,0,0" o="324650" m="" /><S X="366" Y="201" L="49" H="10" T="12" P="0,0,0.3,0.2,27,0,0,0" o="324650" m="" /><S X="401" Y="206" L="35" H="10" T="12" P="0,0,0.3,0.2,-21,0,0,0" o="324650" m="" /><S X="426" Y="182" L="50" H="10" T="12" P="0,0,0.3,0.2,-63,0,0,0" o="324650" m="" /><S X="585" Y="206" L="129" H="12" T="12" P="0,0,0.3,0.2,60,0,0,0" o="324650" m="" /><S X="649" Y="207" L="129" H="10" T="12" P="0,0,0.3,0.2,128,0,0,0" o="324650" m="" /><S X="655" Y="211" L="113" H="10" T="12" P="0,0,0.3,0.2,128,0,0,0" o="324650" m="" /><S X="689" Y="160" L="19" H="10" T="12" P="0,0,0.3,0.2,74,0,0,0" o="324650" m="" /><S X="323" Y="413" L="26" T="13" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="190" Y="156" L="14" H="14" T="12" P="0,0,0.3,0.2,-15,0,0,0" o="324650" m="" /><S X="209" Y="154" L="28" H="13" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="226" Y="155" L="10" H="13" T="12" P="0,0,0.3,0.2,12,0,0,0" o="324650" m="" /><S X="240" Y="157" L="20" H="13" T="12" P="0,0,0.3,0.2,5,0,0,0" o="324650" m="" /><S X="337" Y="155" L="13" H="10" T="12" P="0,0,0.3,0.2,-21,0,0,0" o="324650" m="" /><S X="350" Y="152" L="17" H="10" T="12" P="0,0,0.3,0.2,-5,0,0,0" o="324650" m="" /><S X="365" Y="154" L="17" H="10" T="12" P="0,0,0.3,0.2,17,0,0,0" o="324650" m="" /></S><D><DS X="32" Y="132" /></D><O></O></Z></C>'
	tfm.exec.newGame(map)
end

function eventNewPlayer(n)
	tfm.exec.chatMessage(getMsg("introduction"), n)
	tfm.exec.chatMessage(getMsg("hint"), n)
	tfm.exec.chatMessage("<j><a href='https://mforum.ist/threads/fishing-2019-the-cove.10074/' target='_blank'>https://mforum.ist/threads/fishing-2019-the-cove.10074/</a>", n)
    players[n] = {
		down = false,
		time = 0,
        x = 0,
		y = 0,
		random = 0,
		relics = 0,
		--[[first = {2263,2264,2265,2266,2267,2268,2269,2270,2271,2272},
		second = {2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283},
		third = {2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294},
		fourth = {2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305},]]--

		burdancek = {2263,2264,2265,2266,2267,2268,2269,2270,2271,2272,2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 2282, 2283, 2285, 2286, 2287, 2288, 2289, 2290, 2291, 2292, 2293, 2294, 2296, 2297, 2298, 2299, 2300, 2301, 2302, 2303, 2304, 2305},
		give = 0,
		orospu = nil
    }
	
	ui.setMapName(getMsg("mN"), n)
	tfm.exec.addImage("fishing_bg.18953", "?1", 0, 0, n)
	tfm.exec.addImage("fgfgfgfgfgfgfishing.18955", "!2", 0, 0, n)
	
	tfm.exec.bindKeyboard(n, 1, true, true) 
	tfm.exec.bindKeyboard(n, 1, false, true) 

	tfm.exec.bindKeyboard(n, 2, true, true) 
	tfm.exec.bindKeyboard(n, 2, false, true) 

	tfm.exec.bindKeyboard(n, 0, true, true) 
	tfm.exec.bindKeyboard(n, 0, false, true) 

    tfm.exec.bindKeyboard(n, 3, true, true) 
	tfm.exec.bindKeyboard(n, 3, false, true)
	
	tfm.exec.bindKeyboard(n, 32, true, true)
    tfm.exec.bindKeyboard(n, 32, false, true)
end

function eventNewGame()
    for n,p in pairs(tfm.get.room.playerList) do
        eventNewPlayer(n)
    end
end

function eventKeyboard(n, key, down, x, y)
	if key == 3 then
		if down then
		    pl = tfm.get.room.playerList[n]
			if pl  ~= nil and  not  pl.isDead then
				players[n].down = true
				tfm.exec.fishRod(n, true)
			end
		else
			tfm.exec.playEmote(n, 15)
			players[n].time = 0
			players[n].down = false
			players[n].x = x
			players[n].y = y
		end
	end

	if key == 1 or key == 0 then
		if players[n]  ~= nil then
        			players[n].time = 0
        			players[n].down = false
        			end
	end
end

function seks(n)
	players[n].random = math.random(1, 1300)

	if players[n].random >= 1 and players[n].random <= 200 then
		tfm.exec.chatMessage(getMsg("nF"), n)
		tfm.exec.playEmote(n, 15)
	elseif players[n].random > 200 and players[n].random <= 250 then
		if players[n].relics < 4 then
			players[n].relics = players[n].relics + 1
			players[n].give = players[n].burdancek[math.random(#players[n].burdancek)]
			system.giveConsumables(n, players[n].give, 1)
			table.delete(players[n].burdancek, players[n].give)
			tfm.exec.chatMessage(getMsg("nC"), n)
			tfm.exec.animZelda(n)
		else
			
			system.giveConsumables(n, consumables[math.random(#consumables)], 1)
			tfm.exec.animZelda(n)
		end
	elseif players[n].random > 250 and players[n].random <= 1300 then
		
		system.giveConsumables(n, consumables[math.random(#consumables)], 1)
		tfm.exec.animZelda(n)
	end
	
	players[n].time = 0
	players[n].down = false
end

function eventLoop()
	for n,d in pairs(tfm.get.room.playerList) do
		if players[n]  ~= nil and players[n].down then
			players[n].time = players[n].time + 1			
			if players[n].time == 15 then
				if (tfm.get.room.playerList[n].x>=185 and tfm.get.room.playerList[n].x<=251 and tfm.get.room.playerList[n].y>=100 and tfm.get.room.playerList[n].y<=140) or (tfm.get.room.playerList[n].x>=325 and tfm.get.room.playerList[n].x<=450 and tfm.get.room.playerList[n].y>=100 and tfm.get.room.playerList[n].y<=140) or (tfm.get.room.playerList[n].x>=555 and tfm.get.room.playerList[n].x<=695 and tfm.get.room.playerList[n].y>=100 and tfm.get.room.playerList[n].y<=140) or (tfm.get.room.playerList[n].x>=10 and tfm.get.room.playerList[n].x<=65 and tfm.get.room.playerList[n].y>=100 and tfm.get.room.playerList[n].y<=140) then
					seks(n)
				else
					system.giveConsumables(n, consumables[math.random(#consumables)], 1)
					tfm.exec.animZelda(n)
				end
			end				
		else
		if players[n]  ~= nil then
			players[n].time = 0
			players[n].down = false
			end
		end
	end
end


main()