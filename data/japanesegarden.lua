tfm.exec.disablePhysicalConsumables(true)
system.eventMs(800)
bouquets = {"rose_cherry", "calla_shibaza", "cherry_shibaza", "rose_calla"}
players = {}
toDespawn = {}

translation = {
	["EN"] = {
		["welcome"] = "<font color='#FEB1FC'>Welcome to the Flower Garden.</font>",
		["buket"] = "<font color='#FADE55'>You need to collect ${firstAmount} ${firstFlower} and ${secondAmount} ${secondFlower}.</font>",
		["flower1"] = "Roses",
		["flower2"] = "Cherry Blossoms",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Calla Lilies",
		["mapName"] = "Valentine's Day 2019 <bl>- Flower Garden | Yamanashi, Japan (JP)"
	},

	["TR"] = {
		["welcome"] = "<font color='#FEB1FC'>Çiçek bahçesine hoş geldiniz.</font>",
		["buket"] = "<font color='#FADE55'>${firstAmount} ${firstFlower} ve ${secondAmount} ${secondFlower} toplamalısın.</font>",
		["flower1"] = "Gül",
		["flower2"] = "Kiraz çiçeği",
		["flower3"] = "Alev çiçeği",
		["flower4"] = "Zambak",
		["mapName"] = "Sevgililer Günü 2019 <bl>- Çiçek Bahçesi | Yamanashi, Japonya (JP)"
	},

	["ES"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bienvenido al Jardín Floral.</font>",
        ["buket"] = "<font color='#FADE55'>Necesitas recoger ${firstAmount} ${firstFlower} y ${secondAmount} ${secondFlower}.</font>",
        ["flower1"] = "Rosas",
        ["flower2"] = "Flor de Cerezo",
        ["flower3"] = "Shibazakuras",
        ["flower4"] = "Lirios Cala",
        ["mapName"] = "Día de San Valentín 2019 <bl>- Jardín Floral | Yamanashi, Japón (JP)"
    },

    ["BR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bem-vindo ao Jardim de Flores.</font>",
		["buket"] = "<font color='#FADE55'>Você precisa coletar ${firstAmount} ${firstFlower} e ${secondAmount} ${secondFlower}.</font>",
		["flower1"] = "Rosas",
		["flower2"] = "Flores de Cerejeira",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Lírios de Calla",
		["mapName"] = "Dia dos Namorados 2019 <bl>- Jardim de Flores | Yamanashi, Japão (JP)"
	},

	["FR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bienvenu au Jardin de Fleurs.</font>",
		["buket"] = "<font color='#FADE55'>Vous devez collecter ${firstAmount} ${firstFlower} et ${secondAmount} ${secondFlower}.</font>",
		["flower1"] = "Roses",
		["flower2"] = "Fleurs de Cerisier",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Lys de Callas",
		["mapName"] = "Saint Valentin 2019 <bl>- Jardin de Fleurs | Yamanashi, Japon (JP)"
	},

	["RU"] = {
		["welcome"] = "<font color='#FEB1FC'>Добро пожаловать в Цветочный Сад.</font>",
		["buket"] = "<font color='#FADE55'>Вам нужно собрать ${firstAmount} ${firstFlower} и ${secondAmount} ${secondFlower}.</font>",
		["flower1"] = "Розы",
		["flower2"] = "Цветение вишни",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Каллa Лилии",
		["mapName"] = " День Святого Валентина 2019 <bl>- Цветочный сад | Yamanashi, Japan (JP)"
	},

	["RO"] = {
		["welcome"] = "<font color='#FEB1FC'>Bine ai venit în Grădina Florilor.</font>",
		["buket"] = "<font color='#FADE55'>Trebuie să colectezi ${firstAmount} ${firstFlower} și ${secondAmount} ${secondFlower}.</font>",
		["flower1"] = "Trandafiri",
		["flower2"] = "Flori de cireș",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Crini Calla",
		["mapName"] = "Ziua îndrăgostiților 2019 <bl>- Grădina Florilor | Yamanashi, Japonia (JP)"
	},

	["HU"] = {
		["welcome"] = "<font color='#FEB1FC'>Üdv a Virágoskertben.</font>",
		["buket"] = "<font color='#FADE55'>Szükséged van ${firstAmount} ${firstFlower} -ra és ${secondAmount} ${secondFlower} -ra.</font>",
		["flower1"] = "Rózsa",
		["flower2"] = "Cseresznyevirág",
		["flower3"] = "Shibazakuras",
		["flower4"] = "Calla liliom",
		["mapName"] = "Valentin-nap 2019 <bl>- Virágoskert | Yamanashi, Japán (JP)"
	}
}

function main()
	map = '<C><P L="1800" Ca="" /><Z><S><S X="900" Y="389" L="1800" H="30" T="12" P="0,0,0.3,0.2,0,0,0,0" o="324650" m="" /><S X="-12" Y="202" L="23" H="408" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="1812" Y="201" L="23" H="408" T="1" P="0,0,0,0.2,0,0,0,0" m="" /></S><D><DS X="263" Y="359" /></D><O></O></Z></C>'
	tfm.exec.newGame(map)
end

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end


function eventNewPlayer(n)
	tfm.exec.addImage("vday_japanesegarden_map.15591", "?1", 0, 0, n)
	tfm.exec.addImage("vday_flowergarden_foreground.15592", "!2", 0, 0, n)
	ui.setMapName(getMsg("mapName"), n)

	tfm.exec.bindKeyboard(n, 3, true, true)
	tfm.exec.bindKeyboard(n, 32, true, true)
	system.addBot(-9826, "Ariana", 3051, "39;0,16_DFDFDF+EAACF8,0,0,26_DFDFDF+D77FEC,20_DFDFDF+EAACF8,0,0,0,3,0", 925, 359, n)

	players[n] = {
		timestamp=os.time(),
		roseAmount = math.random(11, 25),
		cherryAmount = math.random(11, 25),
		shibaAmount = math.random(11, 25),
		callaAmount = math.random(11, 25),
		roses = 0,
		cherryblossoms = 0,
		shibazakuras = 0,
		callalilies = 0,
		bouquet = bouquets[math.random(#bouquets)],
		tamam = false,
		flower1 = false,
		flower2 = false,
		flower3 = false,
		flower4 = false,
		flowerType = "unknown",
		uyari = false
	}

	tfm.exec.chatMessage(getMsg("welcome"), n)
	if players[n].bouquet == "rose_cherry" then
		players[n].flowerType = "rose_cherry_preview.15719"
		tfm.exec.chatMessage(param(getMsg("buket"), {firstAmount=players[n].roseAmount, firstFlower=getMsg("flower1"), secondAmount=players[n].cherryAmount, secondFlower=getMsg("flower2")}), n)
	elseif players[n].bouquet == "calla_shibaza" then
		players[n].flowerType = "calla_shibaza_preview.15718"
		tfm.exec.chatMessage(param(getMsg("buket"), {firstAmount=players[n].callaAmount, firstFlower=getMsg("flower4"), secondAmount=players[n].shibaAmount, secondFlower=getMsg("flower3")}), n)
	elseif players[n].bouquet == "cherry_shibaza" then
		players[n].flowerType = "cherry_shibaza_preview.15717"
		tfm.exec.chatMessage(param(getMsg("buket"), {firstAmount=players[n].cherryAmount, firstFlower=getMsg("flower2"), secondAmount=players[n].shibaAmount, secondFlower=getMsg("flower3")}), n)
	elseif players[n].bouquet == "rose_calla" then
		players[n].flowerType = "rose_calla_preview.15729"
		tfm.exec.chatMessage(param(getMsg("buket"), {firstAmount=players[n].roseAmount, firstFlower=getMsg("flower1"), secondAmount=players[n].callaAmount, secondFlower=getMsg("flower4")}), n)
	end

	tfm.exec.addImage(players[n].flowerType, "?5", 230, 130, n)
end

function eventNewGame()
	for n,p in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end
end

function eventLoop()
	for i,flower in ipairs(toDespawn) do
		if flower[1] <= os.time()-1000 then
			table.remove(toDespawn,i)
		end
	end

	for n,p in pairs(tfm.get.room.playerList) do
		if players[n].tamam == false then
			if players[n].bouquet == "rose_cherry" then
				if players[n].roses == players[n].roseAmount and players[n].cherryblossoms == players[n].cherryAmount then
					system.giveConsumables(n, math.random(2132, 2137), 1)
					players[n].tamam = true
				end

			elseif players[n].bouquet == "calla_shibaza" then
				if players[n].callalilies == players[n].callaAmount and players[n].shibazakuras == players[n].shibaAmount then
					system.giveConsumables(n, math.random(2132, 2137), 1)
					players[n].tamam = true
				end
				
			elseif players[n].bouquet == "cherry_shibaza" then
				if players[n].cherryblossoms == players[n].cherryAmount and players[n].shibazakuras == players[n].shibaAmount then
					system.giveConsumables(n, math.random(2132, 2137), 1)
					players[n].tamam = true
				end

			elseif players[n].bouquet == "rose_calla" then
				if players[n].roses == players[n].roseAmount and players[n].callalilies == players[n].callaAmount then
					system.giveConsumables(n, math.random(2132, 2137), 1)
					players[n].tamam = true
				end
			end
		else
			if (math.abs(925 - tfm.get.room.playerList[n].x) < 20 and math.abs(359 - tfm.get.room.playerList[n].y) < 20) then
				if players[n].uyari == false then
					system.giveConsumables(n, 2203, 1)
					players[n].uyari = true 
				end
			end
		end
	end
end

function test(h, x, y, n)
	local particles = {5}
    for i = 0, h do
      tfm.exec.displayParticle(particles[math.random(#particles)], x, y, math.cos(i) * 6 * math.random(50), math.sin(i) * 6 * math.random(50), 0, 5, n)
    end
end

function eventKeyboard(n, k, d, x, y)
	if k == 3 or k == 32 then
		if players[n].timestamp < os.time()-400 then
			players[n].timestamp=os.time()
			table.insert(toDespawn,{os.time(),cb})

			if (x>=475 and x<= 821 and y>=359 and y<=380) then
				if players[n].bouquet == "rose_calla" or players[n].bouquet == "rose_cherry" then
					if players[n].roses < players[n].roseAmount then
						tfm.exec.displayParticle(15,x,y,0,-1,0,-1,n)
						players[n].roses = players[n].roses + 1
					elseif players[n].roses == players[n].roseAmount then
						if players[n].flower1 == false then
							test(5, x, y, n)
							players[n].flower1 = true 
						end
					end
				end
			end

			if (x>=830 and x<= 975 and y>=359 and y<=380) then
				if players[n].bouquet == "rose_cherry" or players[n].bouquet == "cherry_shibaza" then
					if players[n].cherryblossoms < players[n].cherryAmount then
						tfm.exec.displayParticle(15,x,y,0,-1,0,-1,n)
						players[n].cherryblossoms = players[n].cherryblossoms + 1
					elseif players[n].cherryblossoms == players[n].cherryAmount then
						if players[n].flower2 == false then
							test(5, x, y, n)
							players[n].flower2 = true 
						end
					end
				end
			end

			if (x>=10 and x<= 135 and y>=359 and y<=380) then
				if players[n].bouquet == "rose_cherry" or players[n].bouquet == "cherry_shibaza" then
					if players[n].cherryblossoms < players[n].cherryAmount then
						tfm.exec.displayParticle(15,x,y,0,-1,0,-1,n)
						players[n].cherryblossoms = players[n].cherryblossoms + 1
					elseif players[n].cherryblossoms == players[n].cherryAmount then
						if players[n].flower2 == false then
							test(5, x, y, n)
							players[n].flower2 = true 
						end
					end
				end
			end

			if (x>=1005 and x<= 1465 and y>=359 and y<=380) then
				if players[n].bouquet == "calla_shibaza" or players[n].bouquet == "cherry_shibaza" then
					if players[n].shibazakuras < players[n].shibaAmount then
						tfm.exec.displayParticle(15,x,y,0,-1,0,-1,n)
						players[n].shibazakuras = players[n].shibazakuras + 1
					elseif players[n].shibazakuras == players[n].shibaAmount then
						if players[n].flower3 == false then
							test(5, x, y, n)
							players[n].flower3 = true 
						end
					end
				end
			end

			if (x>=1500 and x<= 1800 and y>=359 and y<=380) then
				if players[n].bouquet == "calla_shibaza" or players[n].bouquet == "rose_calla" then
					if players[n].callalilies < players[n].callaAmount then
						tfm.exec.displayParticle(15,x,y,0,-1,0,-1,n)
						players[n].callalilies = players[n].callalilies + 1
					elseif players[n].callalilies == players[n].callaAmount then
						if players[n].flower4 == false then
							test(5, x, y, n)
							players[n].flower4 = true 
						end
					end
				end
			end
		end
	end
end

main()