tfm.exec.disablePhysicalConsumables(true)
seriler = {
	["bir"] = {"A", "D"},
	["iki"] = {"A", "D"},
	["uc"] = {"A", "B", "D"}
}
thrownCoins = 0
players = {}
system.eventMs(800)

translation = {
	["EN"] = {
		["welcome"] = "<font color='#FEB1FC'>Welcome to the Fontana di Trevi. Complete the steps and win coins.</font>",
		["toss"] = "<font color='#FEB1FC'>Now you can toss your coins! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>You have ${para} coin(s).</font>",
		["completed"] = "<rose>Congratulations! You've completed the task.</rose>",
		["countdown"] = "<ch>Toss your coin... ${sure} second(s) left. (press E)",
		["fail"] = "<r>Failed. T_T",
		["mapName"] = "Valentine's Day 2019 <bl>- Fontana di Trevi | Rome, Italy (IT)"
	},

	["TR"] = {
		["welcome"] = "<font color='#FEB1FC'>Aşk çeşmesine hoş geldiniz. Adımları tamamlayın ve paranızı kazanın.</font>",
		["toss"] = "<font color='#FEB1FC'>Şimdi paranızı atabilirsiniz! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>${para} TL bozuk paran var.</font>",
		["completed"] = "<rose>Tebrikler! Görevi tamamladın..</rose>",
		["countdown"] = "<ch>Paranı fırlat... ${sure} saniye kaldı. (E tuşuna bas)",
		["fail"] = "<r>Başarısız olundu. T_T",
		["mapName"] = "Sevgililer Günü 2019 <bl>- Aşk Çeşmesi | Roma, İtalya (IT)"
	},

	["ES"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bienvenido a la Fontana di Trevi. Completa las instrucciones y gana monedas.</font>",
        ["toss"] = "<font color='#FEB1FC'>¡Ahora puedes arrojar tus monedas! ^-^</font>",
        ["yhcoins"] = "<font color='#FADE55'>Tú tienes ${para} moneda(s).</font>",
        ["completed"] = "<rose>¡Felicidades! Has completado la tarea.</rose>",
        ["countdown"] = "<ch>Arroja tu moneda... ${sure} segundo(s) restantes. (E)",
        ["fail"] = "<r>Fallaste. T_T",
        ["mapName"] = "Día de San Valentín <bl>- Fontana di Trevi | Roma, Italia (IT)"
    },

    ["BR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bem-vindo à Fontana di Trevi. Complete os passos para ganhar moedas.</font>",
		["toss"] = "<font color='#FEB1FC'>Agora você pode jogar suas moedas! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>Você tem ${para} moeda(s).</font>",
		["completed"] = "<rose>Parabéns! Você acaba de completar a tarefa.</rose>",
		["countdown"] = "<ch>Jogue sua moeda ${sure} segundos restantes. (E)",
		["fail"] = "<r>Falhou. T_T",
		["mapName"] = "Dia dos Namorados 2019 <bl>- Fontana di Trevi | Roma, Itália (IT)"
	},

	["FR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bienvenu à la Fontaine de Trevi. Complétez les étapes et obtenez des pièces de monnaies.</font>",
		["toss"] = "<font color='#FEB1FC'>Maintenant vous pouvez lancer vos pièces de monnaie! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>Vous avez ${para} pièce(s).</font>",
		["completed"] = "<rose>Félicitations! Vous avez complété la tâche.</rose>",
		["countdown"] = "<ch>Jetez vos pièces de monnaie... ${sure} seconde(s) restantes. (E)",
		["fail"] = "<r>Raté. T_T",
		["mapName"] = "Saint Valentin 2019 <bl>- Fontaine de Trevi | Rome, Italy (IT)"
	},

	["RU"] = {
		["welcome"] = "<font color='#FEB1FC'>Добро пожаловать в Фонтан Треви. Завершите шаги и выиграйте монет.</font>",
		["toss"] = "<font color='#FEB1FC'>Теперь вы можете бросить свои монеты! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>У тебя ${para} монета(s).</font>",
		["completed"] = "<rose>Поздравляем! Вы выполнили задание.</rose>",
		["countdown"] = "<ch>Брось свою монету... ${sure} второй(s) слева. (E)",
		["fail"] = "<r>Не удалось. T_T",
		["mapName"] = "День Святого Валентина 2019 <bl>- Фонтан Треви | Rome, Italy (IT)"
	},

	["RO"] = {
		["welcome"] = "<font color='#FEB1FC'>Bine ai venit la Fontana di Trevi. Finalizează pașii și câștigă monedele.</font>",
		["toss"] = "<font color='#FEB1FC'>Acum poți să-ți arunci monedele! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>Ai ${para} monede.</font>",
		["completed"] = "<rose>Felicitări! Ai completat sarcina.</rose>",
		["countdown"] = "<ch>Aruncă moneda... ${sure} secunde rămase. (E)",
		["fail"] = "<r>Ai pierdut. T_T",
		["mapName"] = "Ziua Îndrăgostiților 2019 <bl>- Fontana di Trevi | Roma, Italia (IT)"
	},

	["HU"] = {
		["welcome"] = "<font color='#FEB1FC'>Üdv a Fontana di Trevi-ben. Fejezd be a lépéseket és nyerj érméket.</font>",
		["toss"] = "<font color='#FEB1FC'>Most már feldobhatod az érméidet! ^-^</font>",
		["yhcoins"] = "<font color='#FADE55'>Jelenleg ${para} érméd van.</font>",
		["completed"] = "<rose>Gratulálok! Befejezted a feladatot.</rose>",
		["countdown"] = "<ch>Dobd fel az érmédet... ${sure} másodperced van még. (E)",
		["fail"] = "<r>Failed. T_T",
		["mapName"] = "Valentin-nap 2019 <bl>- Fontana di Trevi | Róma, Olaszország (IT)"
	}
}

function main()
	map = '<C><P Ca="" /><Z><S><S X="19" m="" o="324650" L="37" Y="347" H="10" P="0,0,0.3,0.2,-17,0,0,0" T="12" /><S X="53" m="" o="324650" L="37" Y="333" H="10" P="0,0,0.3,0.2,-27,0,0,0" T="12" /><S X="86" m="" o="324650" L="37" Y="325" H="10" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="114" m="" o="324650" L="37" Y="323" H="10" P="0,0,0.3,0.2,-10,0,0,0" T="12" /><S X="132" m="" o="324650" L="37" Y="316" H="10" P="0,0,0.3,0.2,-36,0,0,0" T="12" /><S X="150" m="" o="324650" L="37" Y="302" H="10" P="0,0,0.3,0.2,-47,0,0,0" T="12" /><S X="176" m="" o="324650" L="37" Y="294" H="10" P="0,0,0.3,0.2,14,0,0,0" T="12" /><S X="209" m="" o="324650" L="37" Y="299" H="10" P="0,0,0.3,0.2,1,0,0,0" T="12" /><S X="237" m="" o="324650" L="37" Y="297" H="10" P="0,0,0.3,0.2,-8,0,0,0" T="12" /><S X="272" m="" o="324650" L="37" Y="297" H="10" P="0,0,0.3,0.2,8,0,0,0" T="12" /><S X="307" m="" o="324650" L="37" Y="303" H="10" P="0,0,0.3,0.2,11,0,0,0" T="12" /><S X="336" m="" o="324650" L="37" Y="307" H="10" P="0,0,0.3,0.2,3,0,0,0" T="12" /><S X="364" m="" o="324650" L="37" Y="307" H="10" P="0,0,0.3,0.2,-3,0,0,0" T="12" /><S X="388" m="" o="324650" L="37" Y="305" H="10" P="0,0,0.3,0.2,-15,0,0,0" T="12" /><S X="422" m="" o="324650" L="37" Y="303" H="10" P="0,0,0.3,0.2,9,0,0,0" T="12" /><S X="446" m="" o="324650" L="37" Y="305" H="10" P="0,0,0.3,0.2,-6,0,0,0" T="12" /><S X="480" m="" o="324650" L="37" Y="308" H="10" P="0,0,0.3,0.2,16,0,0,0" T="12" /><S X="515" m="" o="324650" L="37" Y="317" H="10" P="0,0,0.3,0.2,12,0,0,0" T="12" /><S X="548" m="" o="324650" L="37" Y="318" H="10" P="0,0,0.3,0.2,-11,0,0,0" T="12" /><S X="550" m="" o="324650" L="37" Y="318" H="10" P="0,0,0.3,0.2,-24,0,0,0" T="12" /><S X="583" m="" o="324650" L="37" Y="312" H="10" P="0,0,0.3,0.2,3,0,0,0" T="12" /><S X="569" m="" o="324650" L="37" Y="313" H="10" P="0,0,0.3,0.2,-28,0,0,0" T="12" /><S X="600" m="" o="324650" L="37" Y="307" H="10" P="0,0,0.3,0.2,6,0,0,0" T="12" /><S X="624" m="" o="324650" L="17" Y="312" H="10" P="0,0,0.3,0.2,29,0,0,0" T="12" /><S X="637" m="" o="324650" L="20" Y="318" H="10" P="0,0,0.3,0.2,17,0,0,0" T="12" /><S X="678" m="" o="324650" L="20" Y="330" H="10" P="0,0,0.3,0.2,165,0,0,0" T="12" /><S X="695" m="" o="324650" L="20" Y="328" H="10" P="0,0,0.3,0.2,3,0,0,0" T="12" /><S X="709" m="" o="324650" L="20" Y="328" H="10" P="0,0,0.3,0.2,-8,0,0,0" T="12" /><S X="753" m="" o="324650" L="98" Y="399" H="64" P="0,0,0.3,0.2,-5,0,0,0" T="12" /><S X="496" m="" L="606" Y="401" H="39" P="0,0,,,,0,0,0" T="9" /><S X="496" m="" o="324650" L="607" Y="412" H="10" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="244" m="" o="324650" L="20" Y="407" H="64" P="0,0,0.3,0.2,-5,0,0,0" T="12" /><S X="739" m="" o="324650" L="20" Y="367" H="10" P="0,0,0.3,0.2,-42,0,0,0" T="12" /><S X="750" m="" o="324650" L="20" Y="367" H="10" P="0,0,0.3,0.2,32,0,0,0" T="12" /><S X="772" m="" o="324650" L="20" Y="367" H="10" P="0,0,0.3,0.2,-61,0,0,0" T="12" /><S X="777" m="" o="324650" L="20" Y="355" H="10" P="0,0,0.3,0.2,-79,0,0,0" T="12" /><S X="783" m="" o="324650" L="20" Y="349" H="10" P="0,0,0.3,0.2,22,0,0,0" T="12" /><S X="793" m="" o="324650" L="20" Y="351" H="10" P="0,0,0.3,0.2,-39,0,0,0" T="12" /><S X="805" m="" L="607" Y="303" H="10" P="0,0,0,0,90,0,0,0" T="1" /><S X="-4" m="" L="607" Y="303" H="10" P="0,0,0,0,90,0,0,0" T="1" /><S X="243" m="" o="324650" L="20" Y="364" H="27" P="0,0,0.3,0.2,8,0,0,0" T="12" /><S X="251" m="" o="324650" L="20" Y="339" H="27" P="0,0,0.3,0.2,28,0,0,0" T="12" /><S X="248" m="" o="324650" L="20" Y="324" H="27" P="0,0,0.3,0.2,-37,0,0,0" T="12" /><S X="245" m="" o="324650" L="20" Y="312" H="27" P="0,0,0.3,0.2,-1,0,0,0" T="12" /><S X="653" m="" o="324650" L="20" Y="325" H="10" P="0,0,0.3,0.2,30,0,0,0" T="12" /><S X="668" m="" o="324650" L="20" Y="331" H="10" P="0,0,0.3,0.2,1,0,0,0" T="12" /></S><D><DS X="200" Y="283" /></D><O /></Z></C>'
	tfm.exec.newGame(map)
end

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end


function eventNewPlayer(n)
	ui.setMapName(getMsg("mapName"), n)
	tfm.exec.addImage("vday_trevi_map_latest.15511", "?0", 0, 0, n)
	tfm.exec.addImage("vday_trevi_map_foreground.15503", "!1", 0, 0, n)

	system.addBot(-9826, "Ariana", 3051, "39;0,16_DFDFDF+EAACF8,0,0,26_DFDFDF+D77FEC,20_DFDFDF+EAACF8,0,0,0,3,0", 340, 380, n)

	players[n]={
		heart = false,
		dance = false,
		clap = false,
		laugh = false,
		kiss = false,
		coins = math.random(1,3),
		bitti = false,
		ctBasla = false,
		countdown = 11,
		tamam = false,
		seri = "undefined"
	}

	if players[n].coins == 1 then
		players[n].seri = seriler.bir[math.random(#seriler.bir)]
	elseif players[n].coins == 2 then
		players[n].seri = seriler.iki[math.random(#seriler.iki)]
	elseif players[n].coins == 3 then
		players[n].seri = seriler.uc[math.random(#seriler.uc)]
	end

	tfm.exec.chatMessage(getMsg("welcome"), n)
end

function eventNewGame()
	for n,p in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end
end

function eventEmotePlayed(n, id)
	if not players[n].bitti then
		if id == 3 and players[n].heart == false then
			tfm.exec.chatMessage("<vp>1/5", n)
			players[n].heart = true 
		elseif id == 0 and players[n].heart == true and players[n].dance == false then
			tfm.exec.chatMessage("<vp>2/5", n)
			players[n].dance = true
		elseif id == 5 and players[n].dance == true and players[n].clap == false then
			tfm.exec.chatMessage("<vp>3/5", n)
			players[n].clap = true
		elseif id == 1 and players[n].clap == true and players[n].laugh == false then
			tfm.exec.chatMessage("<vp>4/5", n)
			players[n].laugh = true
		elseif id == 23 and players[n].laugh == true and players[n].kiss == false then
			tfm.exec.chatMessage("<vp>5/5", n)
			--system.giveConsumables(n, math.random(2139, 2144), 1)
			tfm.exec.chatMessage(param(getMsg("yhcoins"), {para=players[n].coins}), n)
			tfm.exec.chatMessage(getMsg("toss"), n)
			players[n].kiss = true 
			players[n].ctBasla = true
		end

		if id == 9 then
			if players[n].ctBasla and players[n].countdown ~= 1 then
				thrownCoins = thrownCoins + players[n].coins 
				if players[n].coins == 1 then
					if players[n].seri == "A" then
						system.giveConsumables(n, math.random(2111, 2116), 1)
					elseif players[n].seri == "D" then
						system.giveConsumables(n, math.random(2111, 2116), 1)
					end

				elseif players[n].coins == 2 then
					if players[n].seri == "A" then
						system.giveConsumables(n, math.random(2111, 2116), 1)
					elseif players[n].seri == "D" then
						system.giveConsumables(n, math.random(2111, 2116), 1)
					end

				elseif players[n].coins == 3 then
					if players[n].seri == "A" then
						system.giveConsumables(n, math.random(2118, 2123), 1)
					elseif players[n].seri == "B" then
						system.giveConsumables(n, math.random(2118, 2123), 1)
					elseif players[n].seri == "D" then
						system.giveConsumables(n, math.random(2118, 2123), 1)
					end
				end
				
				players[n].coins = 0
				players[n].bitti = true
				players[n].tamam = true
				players[n].ctBasla = false



				tfm.exec.chatMessage(getMsg("completed"), n)

			end
		end
	end
end

function eventLoop()
	for n,p in pairs(tfm.get.room.playerList) do
		if not tfm.get.room.playerList[n].isDead then
			if players[n].ctBasla then
				if players[n].countdown ~= 1 then
					players[n].countdown = players[n].countdown - 1

					tfm.exec.chatMessage(param(getMsg("countdown"), {sure=players[n].countdown}), n)
				elseif players[n].countdown == 1 and not players[n].tamam then
					system.newTimer(function() tfm.exec.chatMessage(getMsg("fail"), n) players[n].tamam = true end,1000,false)
				end
			end
		end
	end
end

main()