package veritabani;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.luaj.vm2.lib.L_Room;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by sevendr on 17.05.2017.
 */
public class LuaData {
    public static void savePlayerData(String playerName, String data, String mg) {

        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO minigames (minioyun,isim,veri) VALUES(?,?,?) ON DUPLICATE KEY UPDATE veri=?")) {

            statement.setString(1, mg);
            statement.setString(2, playerName);
            statement.setString(3, data);
            statement.setString(4, data);
            statement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }// eventPlayerDataLoaded ( playerName, playerData )

    public static String getDeveloperName(L_Room l_room) {
        if (l_room.debug) {
            PlayerConnection pc = l_room.room.clients.get(l_room.developer);
            if (pc != null) {
                if (pc.kullanici.yetki == 10 || pc.kullanici.isLuacrew)
                    return "#" + pc.kullanici.isim;
            }
        }
        if (l_room.room.event) {
            return "_"+l_room.eventAd;
        }
        if (l_room.room.LUAFile == null) return "NONE";
        return l_room.room.LUAFile;
    }

    public static void loadPlayerData(String playerName, L_Room l_room) {

        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection
                     .prepareStatement("select veri from minigames where minioyun=? and isim=?")) {

            statement.setString(1, getDeveloperName(l_room));
            statement.setString(2, playerName);
            ResultSet rs = statement.executeQuery();
            String veri = "";
            if (rs.next()) {
                veri = rs.getString(1);
            }
            l_room.eventPlayerDataLoaded(playerName, veri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }// eventPlayerDataLoaded ( playerName, playerData )
}
