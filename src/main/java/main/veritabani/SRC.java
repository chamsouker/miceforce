package veritabani;

import mfserver.main.MFServer;
import mfserver.util.Yasaklama;

import java.util.function.Consumer;

/**
 * @author sevendr
 */

public interface SRC<T> {
    /**
     * Called when the operation completes.
     *
     * @param result the result, which may be null. Always null if e is not null.
     */
    void onResult(T result);
    public static Consumer lambdaWrapper(Consumer consumer) {
        return i -> {
            if (i instanceof Exception){
                MFServer.log.warning(MFServer.stackTraceToString((Exception)i));
            }
            try {
                consumer.accept(i);
            } catch (Exception e) {
                System.err.println(
                        "Exception occured : " + e.getMessage());
            }
        };
    }
}
