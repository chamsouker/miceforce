package veritabani;

import mfserver.main.MFServer;
import mfserver.util.VanillaMap;
import mfserver.util.map;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author sevendr
 */
public class Harita {

    public static void haritalar(MFServer server) {
        MFServer.executor.execute(() -> {

            try (Connection connection = MFServer.DS.getConnection();
                 PreparedStatement statement = connection.prepareStatement("select id,perm from maps"); PreparedStatement statement1 = connection.prepareStatement("set sql_mode=\"STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\"")) {

                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    int _id = rs.getInt(1);
                    int perm = rs.getInt(2);
                    ArrayList<Integer> l = server.perms.computeIfAbsent(perm, k -> new ArrayList<>());
                    l.add(_id);
                }
                statement1.execute();

                System.out.println("MAPS LOADED");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public static map al(Integer mapCode) {
        if(MFServer.DS == null){
            map m = new map();
            m.perm=22;
            m.id=570531;
            m.name="Soulzone";
            m.xml="<C><P Ca=\"\" /><Z><S><S X=\"394\" L=\"90\" Y=\"140\" H=\"20\" P=\"0,0,0,0.2,-45,0,0,0\" T=\"1\" /><S X=\"321\" L=\"169\" Y=\"346\" H=\"20\" P=\"0,0,0,0.2,-45,0,0,0\" T=\"1\" /><S X=\"593\" L=\"100\" Y=\"110\" H=\"20\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"0\" /><S X=\"469\" L=\"169\" Y=\"323\" H=\"20\" P=\"0,0,0,0.2,-45,0,0,0\" T=\"1\" /><S X=\"260\" L=\"10\" Y=\"325\" H=\"69\" P=\"0,0,20,0.2,,0,0,0\" T=\"4\" /><S X=\"469\" L=\"100\" Y=\"111\" H=\"20\" P=\"0,0,0,0.2,,0,0,0\" T=\"1\" /><S X=\"86\" L=\"113\" Y=\"107\" H=\"21\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"0\" /><S X=\"321\" L=\"130\" Y=\"241\" H=\"20\" P=\"0,0,0,0.2,45,0,0,0\" T=\"1\" /><S X=\"445\" L=\"180\" Y=\"176\" H=\"20\" P=\"0,0,0,0.2,,0,0,0\" T=\"1\" /><S X=\"260\" L=\"10\" Y=\"163\" H=\"250\" P=\"0,0,0,0.2,,0,0,0\" T=\"1\" /><S X=\"767\" L=\"27\" Y=\"228\" H=\"200\" P=\"0,0,20,0.2,,0,0,0\" T=\"4\" /><S X=\"269\" L=\"25\" Y=\"207\" H=\"340\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"0\" N=\"\" /><S X=\"400\" L=\"800\" Y=\"23\" H=\"27\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"5\" /><S X=\"400\" L=\"800\" Y=\"389\" H=\"27\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"5\" /><S X=\"780\" L=\"40\" Y=\"200\" H=\"400\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"6\" /><S X=\"178\" L=\"47\" Y=\"288\" H=\"52\" P=\"0,0,0,0.2,,0,0,0\" T=\"1\" /><S X=\"531\" L=\"25\" Y=\"207\" H=\"340\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"0\" N=\"\" /><S X=\"102\" L=\"202\" Y=\"253\" H=\"26\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"0\" /><S X=\"20\" L=\"40\" Y=\"200\" H=\"400\" P=\"0,0,0.3,0.2,,0,0,0\" T=\"6\" /></S><D><T X=\"69\" Y=\"96\" /><DS X=\"83\" Y=\"361\" /><F X=\"658\" D=\"\" Y=\"370\" /></D><O /></Z></C>";
            return  m;
        }
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * FROM maps where id =?")) {
            statement.setInt(1, mapCode);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return new map().load(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return VanillaMap.get(0);
    }
    public static map alRacing(Integer mapCode,int perm) {
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection.prepareStatement("select * FROM maps where id =?")) {
            statement.setInt(1, mapCode);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                map Map=new map().load(rs);
                Map.perm=perm;
                Map.name=MFServer.getPerms(new int[]{perm}).name;
                Map.id=MFServer.getPerm(perm).id;
                return Map;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return VanillaMap.get(0);
    }


}
