package veritabani;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sevendr on 5.04.2017.
 */
public class Kayit {
    public final int id;
    public final String ad;
    public final String yetkili;
    public final String sebep;
    public final String oda;
    public final String durum;
    public final String ip;
    public final int surev;
    public int sure = -1;

    public Kayit(ResultSet rs) throws SQLException {
        id = rs.getInt(1);
        ad = rs.getString(2);
        yetkili = rs.getString(3);
        sebep = rs.getString(4);
        oda = rs.getString(5);
        sure = rs.getInt("surev");
        durum = rs.getString(7);
        ip = rs.getString(8);
        surev = rs.getInt("time");

    }
}
