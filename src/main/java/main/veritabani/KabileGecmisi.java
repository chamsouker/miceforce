package veritabani;

import mfserver.main.MFServer;
import org.bson.Document;

public class KabileGecmisi {
    public int tribeCode;
    public int tip;
    public Document bilgi;
    public int tarih;
    public KabileGecmisi(){

    }
    public KabileGecmisi(int tribeCode,int tip,Document bilgi){
        this.tribeCode=tribeCode;
        this.tip=tip;
        this.bilgi=bilgi;
        this.tarih= MFServer.timestamp2();
    }
}
