package mfserver.util;

import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;

/**
 * @author sevendr
 */

public class Event {
    public LuaValue[] degerler;
    public String fonksiyon;
    public boolean loop;
    public long deadline;
    public int time;
    public LuaFunction fonk;
    public int evId;

    public Event(String f, LuaValue[] vals) {
        fonksiyon = f;
        degerler = vals;
    }

    public Event(String f) {
        fonksiyon = f;
        degerler = new LuaValue[0];
    }
}
