package mfserver.util;

import java.util.concurrent.atomic.AtomicInteger;

public class Mob {
    public Mob(AtomicInteger can) {
        this.can = can;
    }

    public AtomicInteger can;
    public long lastMoveTime;
    public int lastDirection;
    public  int x;
    public  int y;
    public  int id;
    public String name;
}
