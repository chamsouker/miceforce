package mfserver.util;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * Created by sevendr on 4.05.2017.
 */
public interface OrnekTribulle {
    void tribulle(int ID, ByteBufInputStream pbuf) throws IOException;

    void arkadasiYolla() throws IOException;

    void kabileyiYolla(PlayerConnection client) throws IOException;

    void kabiledenAtildi(String atan, String atilan, int atilan_id) throws IOException;

    void rutbeDegistir(String degistiren, String degistirilen, String rutbe, int degistirilen_id, int rank_id)
            throws IOException;

    void receivePM(String sender, String message, int senderLang) throws IOException;
}
