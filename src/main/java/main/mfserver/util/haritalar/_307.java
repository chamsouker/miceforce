package mfserver.util.haritalar;

import mfserver.util.map;

public class _307 extends map {
    public _307() {
        this.perm = 2;
        this.name = "Rosenmontag";
        this.id = 307;
        this.xml = "<C><P F=\"1\" /><Z><S><S X=\"475\" H=\"300\" o=\"324650\" L=\"100\" Y=\"431\" c=\"1\" P=\"0,0,0.3,0.2,-45,0,0,0\" T=\"12\" /><S X=\"360\" H=\"90\" o=\"324650\" L=\"300\" Y=\"329\" c=\"1\" P=\"0,0,0.3,0.2,90,0,0,0\" T=\"12\" /><S H=\"300\" o=\"324650\" L=\"100\" Y=\"321\" c=\"1\" X=\"476\" P=\"0,0,0.3,0.2,-45,0,0,0\" T=\"12\" /><S X=\"200\" L=\"400\" Y=\"360\" H=\"150\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"6\" /><S H=\"60\" L=\"400\" Y=\"369\" X=\"600\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"6\" /><S H=\"40\" L=\"120\" Y=\"200\" X=\"740\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"6\" /></S><D><DS X=\"20\" Y=\"270\" /><P P=\"0,0\" X=\"595\" T=\"0\" Y=\"340\" /><P P=\"0,0\" X=\"772\" T=\"4\" Y=\"339\" /><P P=\"0,0\" X=\"693\" T=\"3\" Y=\"183\" /><P P=\"0,1\" X=\"260\" T=\"11\" Y=\"285\" /><P P=\"0,0\" X=\"113\" T=\"1\" Y=\"291\" /><T X=\"20\" Y=\"285\" /><F X=\"740\" Y=\"175\" /><P P=\"0,0\" X=\"782\" T=\"2\" Y=\"178\" /><P P=\"0,1\" X=\"702\" T=\"2\" Y=\"338\" /></D><O /></Z></C>";
    }
}