package mfserver.util;

import org.msgpack.annotation.Message;

@Message
public class DataHolder {
    public int playerId;
    public long millis;
    public byte[] data;

    public DataHolder(int playerId, byte[] data) {
        this.playerId = playerId;
        this.data = data;
        this.millis=System.currentTimeMillis();
    }

    public DataHolder() {
    }
}
