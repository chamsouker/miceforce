package mfserver.util;

import mfserver.main.MFServer;
import mfserver.main.Room;

/**
 * Created by sevendr on 22.05.2017.
 */
public class OdaMod {
    public String ad;
    public int sayi = 0;
    public boolean funcorp = false;
    public int tip = 0;
    public int dil = 0;
    public int sinir = 100;//Sınır

    public OdaMod(Room room) {
        ad = room.outname;
        if (room.lang.equals("XX")) ad = "*" + ad;
        sayi = room.playerCount();
        funcorp = room.isFuncorp;
        sinir = room.maxPlayers;
        dil = MFServer.tersLangs.getOrDefault(room.lang, 0);
    }

    public OdaMod() {

    }
}
