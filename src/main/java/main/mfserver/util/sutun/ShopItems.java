package mfserver.util.sutun;

import java.util.ArrayList;
import java.util.HashMap;

public class ShopItems {
    public HashMap<Integer, ShopItem> items=new HashMap<Integer, ShopItem>();
    public class ShopItem{
        public int cat;
        public boolean used;
        public ArrayList<Integer> renkler;
    }
}
