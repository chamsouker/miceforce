package mfserver.util;

import java.util.ArrayList;

public class Sistem {
    public ArrayList<byte[]> arrayList;
    public Sistem(){
        arrayList=new ArrayList<>();
        arrayList.add(new byte[25]);
    }

    public static void writeShort(int v,int index,byte[] writeBuffer) {
        writeBuffer[index] = (byte)((v >>> 8) & 0xFF);
        writeBuffer[index+1] = (byte)((v) & 0xFF);
    }

    public static void writeBoolean(boolean v,int index,byte[] writeBuffer) {
        writeBuffer[index] = (byte)(v ? 1 : 0);
    }
    public static void writeByte(int v,int index,byte[] writeBuffer) {
        writeBuffer[index] = (byte)v;
    }

    public  static void writeInt(int v,int index,byte[] writeBuffer) {
        writeBuffer[index] = (byte)((v >>> 24) & 0xFF);
        writeBuffer[index+1] = (byte)((v >>> 16) & 0xFF);
        writeBuffer[index+2] = (byte)((v >>> 8) & 0xFF);
        writeBuffer[index+3] = (byte)(v);
    }

    public  static void writeLong(long v,int index,byte[] writeBuffer) {
        writeBuffer[index] = (byte)(v >>> 56);
        writeBuffer[index+1] = (byte)(v >>> 48);
        writeBuffer[index+2] = (byte)(v >>> 40);
        writeBuffer[index+3] = (byte)(v >>> 32);
        writeBuffer[index+4] = (byte)(v >>> 24);
        writeBuffer[index+5] = (byte)(v >>> 16);
        writeBuffer[index+6] = (byte)(v >>>  8);
        writeBuffer[index+7] = (byte)(v);
    }
    public static long readLong (int index,byte[] buf)
 {
        return (((long)(buf [index] & 0xff) << 56) |
                     ((long)(buf [index+1] & 0xff) << 48) |
                      ((long)(buf [index+2] & 0xff) << 40) |
                      ((long)(buf [index+3] & 0xff) << 32) |
                     ((long)(buf [index+4] & 0xff) << 24) |
                      ((long)(buf [index+5] & 0xff) << 16) |
                      ((long)(buf [index+6] & 0xff) <<  8) |
                       ((long)(buf [index+7] & 0xff)));
           }
}
