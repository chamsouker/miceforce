/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class O_Death extends base_old {

    public O_Death(PlayerConnection client) throws IOException {
        super((byte) 8, (byte) 5, client);
        this.objects = new Object[]{client.kullanici.code, client.room.death_alive()[1], client.score};
    }
    public O_Death(int playerCode, int dv) throws IOException {
        super((byte) 8, (byte) 5);
        this.objects = new Object[]{playerCode, dv, 0};
    }
}
