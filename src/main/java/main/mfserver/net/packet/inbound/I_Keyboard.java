/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class I_Keyboard extends Packet {

    public boolean down;
    public short key;
    public short x;
    public short y;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {

        key = buf.readShort();
        down = buf.readBoolean();
        x = buf.readShort();
        y = buf.readShort();

    }

}
