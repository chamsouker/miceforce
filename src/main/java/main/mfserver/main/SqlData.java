package mfserver.main;

/**
 * Created by sevendr on 20.03.2017.
 */
public class SqlData {
    public int typ;
    public int code;
    public String name;
    public String room;
    public int time_stamp;
    public String isp;
    public String dil;
    public int type;
    public String ip;
    public String sent_to;
    public String message;
    public long Pey;
    public double Hole;
    public int map;
    public int flag;
    public long millis;
    public byte[] data;
    public String country;
    public String city;
    public String tz;

    public SqlData() {
        time_stamp = MFServer.timestamp();
    }
}
