/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import com.google.common.collect.Lists;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.data_out.*;
import mfserver.net.PlayerConnection;
import mfserver.util.*;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.L_Room;
import org.msgpack.MessagePack;
import veritabani.*;

import javax.print.Doc;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;
import static mfserver.main.MFServer.colRandomUUID;

/**
 * @author sevendr
 */
public class Room {

    public static final Pattern Official = Pattern.compile(
            "^(village|racing|survivor|defilante|music|vanilla|fastracing|kutier|bootcamp|\\#parkour|\\#|)(\\d{3}|\\d{2}|\\d{1}|)$");
    public static final char tire = "-".charAt(0);
    public static final String s_tribe = "*" + (char) 3;
    public static final String char_3 = "" + (char) 3;
    public static final String s_tutorial = (char) 3 + "[tutorial]";
    public static final String s_totem = (char) 3 + "[totem]";
    public static final String s_editeur = (char) 3 + "[editeur]";
    public static final int[] P_Norm = new int[]{0, 1, 4, 9, 5, 8, 14, 6, 7};
    public static final int[] P_Music = new int[]{19};
    public static final int[] P_Defilante = new int[]{18};
    public static final int[] P_Bootcamp = new int[]{13, 3};
    public static final int[] P_Racing = new int[]{17, 17, 7, 17, 17};
    public static final int[] P_Survivor = new int[]{10, 10, 10, 10, 10, 10, 10, 11, 10, 10, 10, 10, 24, 10, 24, 10};
    public static final int[] P_Kutier = new int[]{10, 10, 10, 10, 10, 10, 11, 10, 10, 10, 10};
    public static final int[] MAP_LIST = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46,
            47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
            74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
            101, 102, 103, 104, 105, 106, 107, 110, 111, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125,
            126, 127, 128, 129, 130, 131, 132, 133, 134, 136, 137, 138, 139, 140, 141, 142, 143, 200, 201, 202, 203,
            204, 205, 206, 207, 208, 209, 210, 301, 302, 303, 304, 305, 306, 307, 308, 309};
    public static final int[] LUA_ROTATION = new int[]{570531, 570542, 570572, 570599, 570630, 570631, 570637, 570864};


    public HashMap<String, Integer> NickColors = new HashMap<>();
    public static final HashSet<Integer> M_noshaman = new HashSet<>();
    public static final HashSet<Integer> M_transform = new HashSet<>();

    public static final HashSet<Integer> M_catchcheese = new HashSet<>();
    public static final HashSet<Integer> M_catchcheese_nosham = new HashSet<>();
    private static final String TENGRI_DATA = StringUtils.join(new Object[]{7, 1, 0, 777, 0, 0, 0, "1;0,0,0,0,0,0,0", 0, "ffffff", "95d9d6", 0, ""}, "#");

    public ScheduledFuture snowTimer;
    public static HashMap<Integer, int[][]> halTuzaklar = new HashMap<>();
    public static HashMap<Integer, int[]> EventMaps = new HashMap<>();

    static {
        EventMaps.put(0, new int[]{120, 120});
        EventMaps.put(1, new int[]{130, 160});
        EventMaps.put(2, new int[]{335, 90});
        EventMaps.put(3, new int[]{720, 200});
        EventMaps.put(4, new int[]{540, 170});
        EventMaps.put(5, new int[]{680, 100});
        EventMaps.put(6, new int[]{400, 290});
        EventMaps.put(7, new int[]{660, 250});
        EventMaps.put(8, new int[]{40, 140});
        EventMaps.put(9, new int[]{700, 100});
        EventMaps.put(10, new int[]{400, 290});
        EventMaps.put(11, new int[]{300, 100});
        EventMaps.put(12, new int[]{660, 180});
        EventMaps.put(13, new int[]{400, 110});
        EventMaps.put(14, new int[]{280, 100});
        EventMaps.put(15, new int[]{280, 260});
        EventMaps.put(16, new int[]{100, 80});
        EventMaps.put(17, new int[]{400, 180});
        EventMaps.put(18, new int[]{215, 100});
        EventMaps.put(19, new int[]{620, 140});
        EventMaps.put(20, new int[]{250, 220});
        EventMaps.put(21, new int[]{375, 220});
        EventMaps.put(22, new int[]{355, 375});
        EventMaps.put(23, new int[]{555, 375});
        EventMaps.put(24, new int[]{335, 280});
        EventMaps.put(25, new int[]{400, 280});
        EventMaps.put(26, new int[]{400, 170});
        EventMaps.put(27, new int[]{323, 200});
        EventMaps.put(28, new int[]{260, 140});
        EventMaps.put(29, new int[]{290, 160});
        EventMaps.put(30, new int[]{410, 120});
        EventMaps.put(31, new int[]{400, 300});
        EventMaps.put(32, new int[]{280, 220});
        EventMaps.put(33, new int[]{95, 120});
        EventMaps.put(34, new int[]{265, 315});
        EventMaps.put(35, new int[]{150, 60});
        EventMaps.put(36, new int[]{550, 260});
        EventMaps.put(37, new int[]{400, 50});
        EventMaps.put(38, new int[]{400, 180});
        EventMaps.put(39, new int[]{180, 260});
        EventMaps.put(40, new int[]{400, 300});
        EventMaps.put(41, new int[]{420, 300});
        EventMaps.put(42, new int[]{1180, 260});
        EventMaps.put(43, new int[]{400, 100});
        EventMaps.put(44, new int[]{400, 240});
        EventMaps.put(46, new int[]{400, 280});
        EventMaps.put(47, new int[]{400, 220});
        EventMaps.put(48, new int[]{400, 60});
        EventMaps.put(49, new int[]{200, 200});
        EventMaps.put(50, new int[]{400, 140});
        EventMaps.put(51, new int[]{400, 230});
        EventMaps.put(52, new int[]{260, 120});
        EventMaps.put(53, new int[]{400, 160});
        EventMaps.put(54, new int[]{80, 100});
        EventMaps.put(56, new int[]{400, 360});
        EventMaps.put(57, new int[]{620, 140});
        EventMaps.put(58, new int[]{600, 200});
        EventMaps.put(59, new int[]{400, 60});
        EventMaps.put(60, new int[]{600, 140});
        EventMaps.put(61, new int[]{640, 230});
        EventMaps.put(62, new int[]{180, 100});
        EventMaps.put(63, new int[]{100, 260});
        EventMaps.put(64, new int[]{600, 200});
        EventMaps.put(65, new int[]{600, 200});
        EventMaps.put(66, new int[]{580, 300});
        EventMaps.put(67, new int[]{430, 280});
        EventMaps.put(68, new int[]{400, 140});
        EventMaps.put(69, new int[]{560, 280});
        EventMaps.put(70, new int[]{460, 80});
        EventMaps.put(71, new int[]{425, 260});
        EventMaps.put(72, new int[]{455, 260});
        EventMaps.put(73, new int[]{400, 300});
        EventMaps.put(74, new int[]{400, 80});
        EventMaps.put(75, new int[]{740, 220});
        EventMaps.put(76, new int[]{400, 380});
        EventMaps.put(77, new int[]{60, 320});
        EventMaps.put(78, new int[]{150, 60});
        EventMaps.put(79, new int[]{400, 160});
        EventMaps.put(80, new int[]{420, 200});
        EventMaps.put(81, new int[]{400, 80});
        EventMaps.put(82, new int[]{180, 200});
        EventMaps.put(83, new int[]{400, 240});
        EventMaps.put(84, new int[]{400, 140});
        EventMaps.put(85, new int[]{400, 180});
        EventMaps.put(86, new int[]{390, 220});
        EventMaps.put(87, new int[]{400, 220});
        EventMaps.put(88, new int[]{400, 260});
        EventMaps.put(89, new int[]{400, 200});
        EventMaps.put(90, new int[]{620, 140});
        EventMaps.put(91, new int[]{660, 240});
        EventMaps.put(92, new int[]{520, 200});
        EventMaps.put(93, new int[]{620, 260});
        EventMaps.put(94, new int[]{430, 220});
        EventMaps.put(95, new int[]{400, 220});
        EventMaps.put(96, new int[]{220, 260});
        EventMaps.put(97, new int[]{80, 230});
        EventMaps.put(98, new int[]{80, 140});
        EventMaps.put(100, new int[]{400, 210});
        EventMaps.put(101, new int[]{740, 200});
        EventMaps.put(102, new int[]{100, 160});
        EventMaps.put(103, new int[]{400, 360});
        EventMaps.put(104, new int[]{400, 200});
        EventMaps.put(105, new int[]{340, 60});
        EventMaps.put(106, new int[]{400, 360});
        EventMaps.put(107, new int[]{60, 320});
        EventMaps.put(114, new int[]{520, 140});
        EventMaps.put(115, new int[]{120, 240});
        EventMaps.put(116, new int[]{400, 100});
        EventMaps.put(117, new int[]{770, 80});
        EventMaps.put(118, new int[]{400, 80});
        EventMaps.put(119, new int[]{560, 320});
        EventMaps.put(120, new int[]{520, 200});
        EventMaps.put(121, new int[]{410, 160});
        EventMaps.put(122, new int[]{130, 160});
        EventMaps.put(123, new int[]{280, 260});
        EventMaps.put(124, new int[]{400, 60});
        EventMaps.put(125, new int[]{400, 60});
        EventMaps.put(126, new int[]{400, 60});
        EventMaps.put(127, new int[]{410, 220});
        EventMaps.put(128, new int[]{400, 340});
        EventMaps.put(129, new int[]{700, 160});
        EventMaps.put(130, new int[]{360, 340});
        EventMaps.put(131, new int[]{130, 320});
        EventMaps.put(132, new int[]{550, 260});
        EventMaps.put(133, new int[]{520, 140});
        EventMaps.put(134, new int[]{680, 200});
        EventMaps.put(136, new int[]{440, 240});
        EventMaps.put(137, new int[]{240, 160});
        EventMaps.put(138, new int[]{400, 300});
        EventMaps.put(139, new int[]{400, 80});
        EventMaps.put(141, new int[]{400, 280});
        EventMaps.put(142, new int[]{400, 160});
        EventMaps.put(143, new int[]{400, 260});
        EventMaps.put(200, new int[]{400, 300});
        EventMaps.put(201, new int[]{400, 120});
        EventMaps.put(202, new int[]{280, 180});
        EventMaps.put(203, new int[]{400, 260});
        EventMaps.put(204, new int[]{60, 240});
        EventMaps.put(205, new int[]{400, 280});
        EventMaps.put(206, new int[]{180, 180});
        EventMaps.put(207, new int[]{260, 60});
        EventMaps.put(208, new int[]{400, 280});
        EventMaps.put(209, new int[]{570, 60});
        EventMaps.put(210, new int[]{620, 220});
        EventMaps.put(114098, new int[]{400, 80});
        EventMaps.put(123076, new int[]{80, 80});
        EventMaps.put(106100, new int[]{180, 140});
        EventMaps.put(115778, new int[]{400, 140});
        EventMaps.put(116247, new int[]{560, 300});
        EventMaps.put(128907, new int[]{680, 140});
        EventMaps.put(124786, new int[]{400, 340});
        EventMaps.put(104162, new int[]{40, 240});
        EventMaps.put(122034, new int[]{400, 160});
        EventMaps.put(125297, new int[]{80, 100});
        EventMaps.put(152536, new int[]{300, 60});
        EventMaps.put(105970, new int[]{400, 340});
        EventMaps.put(122301, new int[]{400, 140});
        EventMaps.put(126243, new int[]{720, 120});
        EventMaps.put(384713, new int[]{400, 360});
        EventMaps.put(126048, new int[]{330, 100});
        EventMaps.put(117067, new int[]{100, 225});
        EventMaps.put(272395, new int[]{160, 180});
        EventMaps.put(109401, new int[]{240, 80});
        EventMaps.put(152216, new int[]{120, 300});
        EventMaps.put(400092, new int[]{400, 180});
        EventMaps.put(315827, new int[]{400, 250});
        EventMaps.put(120031, new int[]{720, 260});
        EventMaps.put(114425, new int[]{680, 80});
        EventMaps.put(133524, new int[]{360, 340});
        EventMaps.put(122056, new int[]{400, 360});
        EventMaps.put(106308, new int[]{520, 80});
        EventMaps.put(133793, new int[]{60, 100});
        EventMaps.put(123084, new int[]{120, 160});
        EventMaps.put(329625, new int[]{520, 80});
        EventMaps.put(330633, new int[]{80, 100});
        EventMaps.put(123700, new int[]{680, 180});
        EventMaps.put(129447, new int[]{640, 280});
        EventMaps.put(107808, new int[]{580, 120});
        EventMaps.put(367449, new int[]{60, 280});
        EventMaps.put(110932, new int[]{400, 340});
        EventMaps.put(111865, new int[]{320, 300});
        EventMaps.put(112969, new int[]{700, 200});
        EventMaps.put(128157, new int[]{85, 300});
        EventMaps.put(114021, new int[]{560, 100});
        EventMaps.put(113371, new int[]{400, 100});
        EventMaps.put(339884, new int[]{80, 80});
        EventMaps.put(120774, new int[]{400, 100});
        EventMaps.put(133411, new int[]{400, 180});
        EventMaps.put(134984, new int[]{220, 320});
        EventMaps.put(108303, new int[]{540, 200});
        EventMaps.put(121887, new int[]{390, 160});
        EventMaps.put(124149, new int[]{180, 280});
        EventMaps.put(129165, new int[]{620, 340});
        EventMaps.put(126617, new int[]{400, 60});
        EventMaps.put(116984, new int[]{400, 160});
        EventMaps.put(315670, new int[]{70, 240});
        EventMaps.put(115291, new int[]{750, 160});
        EventMaps.put(126322, new int[]{400, 340});
        EventMaps.put(104825, new int[]{385, 100});
        EventMaps.put(115509, new int[]{360, 80});
        EventMaps.put(112964, new int[]{590, 80});
        EventMaps.put(119388, new int[]{700, 240});
        EventMaps.put(129847, new int[]{400, 350});
        EventMaps.put(137254, new int[]{400, 80});
        EventMaps.put(115286, new int[]{640, 280});
        EventMaps.put(102061, new int[]{400, 100});
        EventMaps.put(122093, new int[]{560, 100});
        EventMaps.put(239068, new int[]{680, 120});
        EventMaps.put(114656, new int[]{400, 340});
        EventMaps.put(112553, new int[]{400, 160});
        EventMaps.put(133034, new int[]{400, 60});
        EventMaps.put(129991, new int[]{400, 340});
        EventMaps.put(124257, new int[]{400, 100});
        EventMaps.put(115902, new int[]{400, 60});
        EventMaps.put(109813, new int[]{400, 80});
        EventMaps.put(130163, new int[]{680, 200});
        EventMaps.put(101872, new int[]{520, 200});
        EventMaps.put(102523, new int[]{290, 180});
        EventMaps.put(126446, new int[]{380, 155});
        EventMaps.put(117718, new int[]{400, 160});
        EventMaps.put(104777, new int[]{640, 100});
        EventMaps.put(126596, new int[]{560, 320});
        EventMaps.put(112213, new int[]{320, 320});
        EventMaps.put(121709, new int[]{400, 260});
        EventMaps.put(129000, new int[]{400, 340});
        EventMaps.put(121924, new int[]{50, 200});
        EventMaps.put(129545, new int[]{180, 240});
        EventMaps.put(133362, new int[]{520, 180});
        EventMaps.put(378860, new int[]{400, 240});
        EventMaps.put(120704, new int[]{65, 320});
        EventMaps.put(100562, new int[]{400, 310});
        EventMaps.put(129088, new int[]{330, 340});
        EventMaps.put(114571, new int[]{400, 240});
        EventMaps.put(107721, new int[]{400, 180});
        EventMaps.put(125088, new int[]{440, 100});
        EventMaps.put(107226, new int[]{400, 280});
        EventMaps.put(129669, new int[]{670, 280});
        EventMaps.put(110277, new int[]{400, 300});
        EventMaps.put(112010, new int[]{400, 300});
        EventMaps.put(108522, new int[]{100, 240});
        EventMaps.put(103112, new int[]{400, 360});
        EventMaps.put(100603, new int[]{315, 120});
        EventMaps.put(291337, new int[]{420, 280});
        EventMaps.put(111668, new int[]{400, 140});
        EventMaps.put(212262, new int[]{400, 320});
        EventMaps.put(109049, new int[]{380, 80});
        EventMaps.put(112590, new int[]{400, 50});
        EventMaps.put(100221, new int[]{437, 85});
        EventMaps.put(309049, new int[]{380, 80});
        EventMaps.put(319257, new int[]{300, 60});
        EventMaps.put(110960, new int[]{400, 160});
        EventMaps.put(122591, new int[]{400, 240});
        EventMaps.put(127868, new int[]{240, 180});
        EventMaps.put(145240, new int[]{700, 60});
        EventMaps.put(121448, new int[]{560, 240});
        EventMaps.put(123991, new int[]{600, 60});
        EventMaps.put(126213, new int[]{100, 140});
        EventMaps.put(112466, new int[]{400, 370});
        EventMaps.put(122645, new int[]{380, 80});
        EventMaps.put(414571, new int[]{560, 80});
        EventMaps.put(133856, new int[]{520, 100});
        EventMaps.put(122029, new int[]{560, 205});
        EventMaps.put(284340, new int[]{80, 200});
        EventMaps.put(123093, new int[]{100, 120});
        EventMaps.put(150683, new int[]{420, 140});
        EventMaps.put(212260, new int[]{60, 270});
        EventMaps.put(212293, new int[]{400, 160});
        EventMaps.put(107044, new int[]{420, 100});
        EventMaps.put(119321, new int[]{265, 80});
        EventMaps.put(152311, new int[]{500, 180});
        EventMaps.put(307585, new int[]{520, 60});
        EventMaps.put(116184, new int[]{120, 140});
        EventMaps.put(106872, new int[]{400, 150});
        EventMaps.put(332334, new int[]{660, 120});
        EventMaps.put(113770, new int[]{80, 100});
        EventMaps.put(108694, new int[]{120, 60});
        EventMaps.put(239069, new int[]{80, 320});
        EventMaps.put(115906, new int[]{680, 80});
        EventMaps.put(122477, new int[]{60, 160});
        EventMaps.put(330625, new int[]{310, 340});
        EventMaps.put(324488, new int[]{720, 220});
        EventMaps.put(323523, new int[]{320, 240});
        EventMaps.put(322934, new int[]{620, 340});
        EventMaps.put(319582, new int[]{720, 240});
        EventMaps.put(316513, new int[]{720, 80});
        EventMaps.put(304656, new int[]{420, 80});
        EventMaps.put(300791, new int[]{520, 100});
        EventMaps.put(239054, new int[]{400, 80});
        EventMaps.put(212292, new int[]{320, 340});
        EventMaps.put(212273, new int[]{680, 180});
        EventMaps.put(152257, new int[]{400, 60});
        EventMaps.put(149958, new int[]{340, 120});
        EventMaps.put(142486, new int[]{140, 140});
        EventMaps.put(133794, new int[]{180, 250});
        EventMaps.put(133658, new int[]{400, 120});
        EventMaps.put(132846, new int[]{400, 50});
        EventMaps.put(130202, new int[]{400, 140});
        EventMaps.put(128237, new int[]{630, 140});
        EventMaps.put(128052, new int[]{700, 80});
        EventMaps.put(127257, new int[]{400, 150});
        EventMaps.put(127213, new int[]{400, 60});
        EventMaps.put(126559, new int[]{90, 350});
        EventMaps.put(123759, new int[]{100, 300});
        EventMaps.put(121987, new int[]{440, 100});
        EventMaps.put(121945, new int[]{380, 120});
        EventMaps.put(121699, new int[]{400, 50});
        EventMaps.put(121664, new int[]{400, 320});
        EventMaps.put(121657, new int[]{60, 200});
        EventMaps.put(121644, new int[]{400, 260});
        EventMaps.put(121210, new int[]{80, 260});
        EventMaps.put(121049, new int[]{680, 80});
        EventMaps.put(120869, new int[]{100, 140});
        EventMaps.put(120804, new int[]{400, 50});
        EventMaps.put(120733, new int[]{120, 220});
        EventMaps.put(119513, new int[]{580, 100});
        EventMaps.put(118227, new int[]{400, 200});
        EventMaps.put(117500, new int[]{600, 120});
        EventMaps.put(116451, new int[]{200, 80});
        EventMaps.put(116299, new int[]{160, 80});
        EventMaps.put(116056, new int[]{290, 325});
        EventMaps.put(115450, new int[]{400, 80});
        EventMaps.put(115329, new int[]{400, 340});
        EventMaps.put(115096, new int[]{265, 160});
        EventMaps.put(112117, new int[]{400, 100});
        EventMaps.put(111550, new int[]{80, 100});
        EventMaps.put(110896, new int[]{480, 100});
        EventMaps.put(110615, new int[]{220, 100});
        EventMaps.put(110613, new int[]{400, 200});
        EventMaps.put(110519, new int[]{545, 145});
        EventMaps.put(109532, new int[]{400, 60});
        EventMaps.put(109288, new int[]{400, 280});
        EventMaps.put(107882, new int[]{300, 60});
        EventMaps.put(107761, new int[]{400, 80});
        EventMaps.put(107264, new int[]{400, 360});
        EventMaps.put(104836, new int[]{505, 330});
        EventMaps.put(104831, new int[]{400, 160});
        EventMaps.put(104720, new int[]{400, 180});
        EventMaps.put(103714, new int[]{680, 160});
        EventMaps.put(103558, new int[]{240, 80});
        EventMaps.put(102189, new int[]{460, 180});
        EventMaps.put(101749, new int[]{250, 80});
        EventMaps.put(101617, new int[]{400, 180});
        EventMaps.put(101577, new int[]{400, 220});
        EventMaps.put(101328, new int[]{680, 280});
        EventMaps.put(101065, new int[]{380, 80});
        EventMaps.put(100124, new int[]{400, 80});
        EventMaps.put(110090, new int[]{20, 145});
        EventMaps.put(142023, new int[]{700, 200});
        EventMaps.put(246942, new int[]{350, 220});
        EventMaps.put(343978, new int[]{400, 60});
        EventMaps.put(223000, new int[]{400, 360});
        EventMaps.put(400972, new int[]{200, 340});
        EventMaps.put(391591, new int[]{280, 80});
        EventMaps.put(387737, new int[]{400, 120});
        EventMaps.put(384408, new int[]{520, 160});
        EventMaps.put(384407, new int[]{400, 200});
        EventMaps.put(384406, new int[]{70, 320});
        EventMaps.put(384401, new int[]{400, 240});
        EventMaps.put(377577, new int[]{100, 90});
        EventMaps.put(375316, new int[]{400, 340});
        EventMaps.put(371293, new int[]{400, 100});
        EventMaps.put(365999, new int[]{400, 310});
        EventMaps.put(365998, new int[]{400, 180});
        EventMaps.put(364632, new int[]{660, 240});
        EventMaps.put(362705, new int[]{760, 90});
        EventMaps.put(362668, new int[]{400, 60});
        EventMaps.put(362557, new int[]{340, 180});
        EventMaps.put(362146, new int[]{1105, 320});
        EventMaps.put(356120, new int[]{50, 180});
        EventMaps.put(355569, new int[]{475, 110});
        EventMaps.put(344173, new int[]{260, 160});
        EventMaps.put(344171, new int[]{400, 180});
        EventMaps.put(344170, new int[]{640, 340});
        EventMaps.put(344168, new int[]{340, 100});
        EventMaps.put(344167, new int[]{400, 150});
        EventMaps.put(344166, new int[]{400, 255});
        EventMaps.put(344165, new int[]{400, 50});
        EventMaps.put(344158, new int[]{770, 90});
        EventMaps.put(344157, new int[]{400, 260});
        EventMaps.put(344156, new int[]{590, 240});
        EventMaps.put(344155, new int[]{400, 80});
        EventMaps.put(344146, new int[]{400, 100});
        EventMaps.put(344145, new int[]{400, 80});
        EventMaps.put(344143, new int[]{225, 340});
        EventMaps.put(344142, new int[]{400, 180});
        EventMaps.put(344140, new int[]{400, 120});
        EventMaps.put(344139, new int[]{400, 200});
        EventMaps.put(344138, new int[]{400, 200});
        EventMaps.put(344137, new int[]{400, 320});
        EventMaps.put(344135, new int[]{400, 220});
        EventMaps.put(344134, new int[]{400, 180});
        EventMaps.put(344132, new int[]{400, 50});
        EventMaps.put(344130, new int[]{400, 60});
        EventMaps.put(344129, new int[]{400, 345});
        EventMaps.put(344128, new int[]{400, 60});
        EventMaps.put(344127, new int[]{760, 250});
        EventMaps.put(344124, new int[]{400, 160});
        EventMaps.put(344123, new int[]{400, 75});
        EventMaps.put(344120, new int[]{400, 75});
        EventMaps.put(344119, new int[]{645, 100});
        EventMaps.put(344118, new int[]{75, 160});
        EventMaps.put(344117, new int[]{400, 50});
        EventMaps.put(344116, new int[]{400, 220});
        EventMaps.put(344115, new int[]{400, 250});
        EventMaps.put(344114, new int[]{630, 200});
        EventMaps.put(344113, new int[]{400, 200});
        EventMaps.put(344110, new int[]{365, 40});
        EventMaps.put(344109, new int[]{400, 320});
        EventMaps.put(344108, new int[]{700, 340});
        EventMaps.put(344107, new int[]{400, 240});
        EventMaps.put(344106, new int[]{400, 130});
        EventMaps.put(344105, new int[]{530, 120});
        EventMaps.put(344104, new int[]{400, 240});
        EventMaps.put(344103, new int[]{280, 240});
        EventMaps.put(344102, new int[]{600, 210});
        EventMaps.put(344101, new int[]{400, 120});
        EventMaps.put(344100, new int[]{360, 140});
        EventMaps.put(344099, new int[]{580, 80});
        EventMaps.put(344098, new int[]{400, 210});
        EventMaps.put(344097, new int[]{400, 60});
        EventMaps.put(344096, new int[]{400, 280});
        EventMaps.put(344095, new int[]{400, 280});
        EventMaps.put(344094, new int[]{400, 120});
        EventMaps.put(344093, new int[]{770, 150});
        EventMaps.put(344092, new int[]{400, 170});
        EventMaps.put(344091, new int[]{450, 220});
        EventMaps.put(344090, new int[]{680, 200});
        EventMaps.put(344089, new int[]{340, 80});
        EventMaps.put(344088, new int[]{140, 260});
        EventMaps.put(344087, new int[]{650, 210});
        EventMaps.put(344085, new int[]{420, 140});
        EventMaps.put(344084, new int[]{180, 210});
        EventMaps.put(344083, new int[]{480, 350});
        EventMaps.put(344082, new int[]{400, 210});
        EventMaps.put(344081, new int[]{500, 330});
        EventMaps.put(344080, new int[]{400, 280});
        EventMaps.put(344079, new int[]{470, 140});
        EventMaps.put(344078, new int[]{400, 160});
        EventMaps.put(344077, new int[]{480, 80});
        EventMaps.put(344076, new int[]{640, 360});
        EventMaps.put(344075, new int[]{160, 60});
        EventMaps.put(341446, new int[]{400, 180});
        EventMaps.put(344073, new int[]{400, 370});
        EventMaps.put(344072, new int[]{575, 110});
        EventMaps.put(344071, new int[]{400, 190});
        EventMaps.put(344070, new int[]{235, 130});
        EventMaps.put(344069, new int[]{520, 240});
        EventMaps.put(344068, new int[]{400, 160});
        EventMaps.put(344067, new int[]{600, 120});
        EventMaps.put(344066, new int[]{620, 105});
        EventMaps.put(344064, new int[]{400, 230});
        EventMaps.put(344063, new int[]{400, 315});
        EventMaps.put(344062, new int[]{400, 120});
        EventMaps.put(344061, new int[]{710, 200});
        EventMaps.put(344059, new int[]{720, 120});
        EventMaps.put(344058, new int[]{220, 210});
        EventMaps.put(344057, new int[]{400, 40});
        EventMaps.put(344056, new int[]{75, 100});
        EventMaps.put(344055, new int[]{410, 120});
        EventMaps.put(344054, new int[]{400, 120});
        EventMaps.put(344053, new int[]{60, 255});
        EventMaps.put(344052, new int[]{500, 220});
        EventMaps.put(344051, new int[]{400, 210});
        EventMaps.put(344050, new int[]{50, 70});
        EventMaps.put(344049, new int[]{400, 140});
        EventMaps.put(344048, new int[]{480, 80});
        EventMaps.put(344047, new int[]{400, 190});
        EventMaps.put(344046, new int[]{1155, 200});
        EventMaps.put(344045, new int[]{600, 190});
        EventMaps.put(344044, new int[]{400, 280});
        EventMaps.put(344043, new int[]{150, 140});
        EventMaps.put(344042, new int[]{400, 100});
        EventMaps.put(344041, new int[]{490, 145});
        EventMaps.put(344040, new int[]{400, 300});
        EventMaps.put(344039, new int[]{400, 45});
        EventMaps.put(344038, new int[]{370, 155});
        EventMaps.put(344037, new int[]{380, 60});
        EventMaps.put(344035, new int[]{400, 120});
        EventMaps.put(344034, new int[]{400, 220});
        EventMaps.put(344033, new int[]{400, 240});
        EventMaps.put(344031, new int[]{680, 330});
        EventMaps.put(344030, new int[]{400, 120});
        EventMaps.put(344029, new int[]{400, 300});
        EventMaps.put(344027, new int[]{350, 70});
        EventMaps.put(344026, new int[]{400, 80});
        EventMaps.put(344025, new int[]{300, 100});
        EventMaps.put(344024, new int[]{500, 260});
        EventMaps.put(344023, new int[]{500, 320});
        EventMaps.put(344022, new int[]{315, 190});
        EventMaps.put(344021, new int[]{400, 125});
        EventMaps.put(344019, new int[]{590, 110});
        EventMaps.put(344018, new int[]{580, 130});
        EventMaps.put(344017, new int[]{400, 220});
        EventMaps.put(344016, new int[]{320, 105});
        EventMaps.put(344015, new int[]{400, 100});
        EventMaps.put(344014, new int[]{360, 190});
        EventMaps.put(344013, new int[]{25, 230});
        EventMaps.put(344013, new int[]{25, 230});
        EventMaps.put(344011, new int[]{500, 260});
        EventMaps.put(344010, new int[]{400, 190});
        EventMaps.put(344007, new int[]{420, 180});
        EventMaps.put(344006, new int[]{610, 240});
        EventMaps.put(344005, new int[]{400, 200});
        EventMaps.put(344004, new int[]{670, 200});
        EventMaps.put(344003, new int[]{400, 240});
        EventMaps.put(344001, new int[]{40, 160});
        EventMaps.put(344000, new int[]{400, 125});
        EventMaps.put(343999, new int[]{540, 80});
        EventMaps.put(343998, new int[]{400, 200});
        EventMaps.put(343996, new int[]{220, 300});
        EventMaps.put(343995, new int[]{400, 120});
        EventMaps.put(343994, new int[]{400, 220});
        EventMaps.put(343993, new int[]{400, 320});
        EventMaps.put(343992, new int[]{140, 200});
        EventMaps.put(343991, new int[]{290, 210});
        EventMaps.put(343990, new int[]{700, 135});
        EventMaps.put(343989, new int[]{400, 230});
        EventMaps.put(343988, new int[]{460, 75});
        EventMaps.put(343986, new int[]{200, 155});
        EventMaps.put(343985, new int[]{200, 85});
        EventMaps.put(343984, new int[]{400, 150});
        EventMaps.put(343983, new int[]{200, 200});
        EventMaps.put(343981, new int[]{400, 160});
        EventMaps.put(343980, new int[]{500, 175});
        EventMaps.put(343979, new int[]{620, 120});
        EventMaps.put(343977, new int[]{275, 90});
        EventMaps.put(343976, new int[]{400, 50});
        EventMaps.put(343975, new int[]{320, 75});
        EventMaps.put(343974, new int[]{400, 120});
        EventMaps.put(343973, new int[]{320, 280});
        EventMaps.put(343971, new int[]{550, 180});
        EventMaps.put(343970, new int[]{80, 140});
        EventMaps.put(343969, new int[]{400, 180});
        EventMaps.put(343968, new int[]{580, 80});
        EventMaps.put(343967, new int[]{400, 200});
        EventMaps.put(343966, new int[]{680, 220});
        EventMaps.put(343964, new int[]{280, 180});
        EventMaps.put(343962, new int[]{280, 180});
        EventMaps.put(343960, new int[]{700, 200});
        EventMaps.put(343958, new int[]{500, 140});
        EventMaps.put(343956, new int[]{740, 360});
        EventMaps.put(343955, new int[]{510, 80});
        EventMaps.put(343953, new int[]{620, 235});
        EventMaps.put(343951, new int[]{400, 235});
        EventMaps.put(343949, new int[]{400, 260});
        EventMaps.put(343947, new int[]{85, 310});
        EventMaps.put(343945, new int[]{400, 340});
        EventMaps.put(343941, new int[]{560, 255});
        EventMaps.put(343939, new int[]{400, 200});
        EventMaps.put(343937, new int[]{65, 260});
        EventMaps.put(343936, new int[]{400, 200});
        EventMaps.put(343934, new int[]{400, 570});
        EventMaps.put(343933, new int[]{545, 180});
        EventMaps.put(343932, new int[]{625, 190});
        EventMaps.put(343931, new int[]{300, 170});
        EventMaps.put(343930, new int[]{580, 300});
        EventMaps.put(343929, new int[]{380, 280});
        EventMaps.put(343928, new int[]{180, 220});
        EventMaps.put(343927, new int[]{500, 160});
        EventMaps.put(343925, new int[]{400, 340});
        EventMaps.put(343924, new int[]{400, 187});
        EventMaps.put(343921, new int[]{450, 300});
        EventMaps.put(343920, new int[]{240, 200});
        EventMaps.put(343919, new int[]{580, 110});
        EventMaps.put(343918, new int[]{200, 220});
        EventMaps.put(343917, new int[]{120, 120});
        EventMaps.put(343916, new int[]{280, 130});
        EventMaps.put(343915, new int[]{400, 500});
        EventMaps.put(343914, new int[]{400, 290});
        EventMaps.put(343913, new int[]{465, 265});
        EventMaps.put(343912, new int[]{400, 260});
        EventMaps.put(343911, new int[]{595, 180});
        EventMaps.put(343910, new int[]{340, 120});
        EventMaps.put(343908, new int[]{400, 210});
        EventMaps.put(343907, new int[]{400, 80});
        EventMaps.put(343905, new int[]{400, 130});
        EventMaps.put(343903, new int[]{280, 180});
        EventMaps.put(343901, new int[]{400, 190});
        EventMaps.put(343900, new int[]{615, 240});
        EventMaps.put(343899, new int[]{550, 315});
        EventMaps.put(343898, new int[]{615, 300});
        EventMaps.put(343897, new int[]{590, 95});
        EventMaps.put(343896, new int[]{320, 355});
        EventMaps.put(343895, new int[]{280, 50});
        EventMaps.put(343893, new int[]{400, 150});
        EventMaps.put(343892, new int[]{400, 120});
        EventMaps.put(343891, new int[]{400, 50});
        EventMaps.put(343890, new int[]{80, 345});
        EventMaps.put(343889, new int[]{460, 240});
        EventMaps.put(343888, new int[]{400, 155});
        EventMaps.put(343887, new int[]{310, 140});
        EventMaps.put(343886, new int[]{400, 210});
        EventMaps.put(343885, new int[]{400, 320});
        EventMaps.put(343884, new int[]{350, 75});
        EventMaps.put(343883, new int[]{400, 200});
        EventMaps.put(343882, new int[]{400, 45});
        EventMaps.put(343881, new int[]{400, 180});
        EventMaps.put(343880, new int[]{560, 205});
        EventMaps.put(343878, new int[]{175, 355});
        EventMaps.put(343877, new int[]{460, 190});
        EventMaps.put(343876, new int[]{150, 70});
        EventMaps.put(343875, new int[]{400, 340});
        EventMaps.put(343874, new int[]{605, 550});
        EventMaps.put(343873, new int[]{400, 185});
        EventMaps.put(343872, new int[]{600, 300});
        EventMaps.put(343871, new int[]{270, 220});
        EventMaps.put(343869, new int[]{100, 200});
        EventMaps.put(343868, new int[]{410, 210});
        EventMaps.put(343865, new int[]{400, 80});
        EventMaps.put(343864, new int[]{545, 345});
        EventMaps.put(343863, new int[]{400, 80});
        EventMaps.put(343862, new int[]{590, 115});
        EventMaps.put(343861, new int[]{400, 120});
        EventMaps.put(343860, new int[]{375, 80});
        EventMaps.put(343859, new int[]{660, 160});
        EventMaps.put(343858, new int[]{400, 370});
        EventMaps.put(343857, new int[]{610, 130});
        EventMaps.put(343856, new int[]{700, 170});
        EventMaps.put(343855, new int[]{400, 355});
        EventMaps.put(343853, new int[]{600, 245});
        EventMaps.put(343852, new int[]{380, 365});
        EventMaps.put(343851, new int[]{620, 75});
        EventMaps.put(343850, new int[]{400, 325});
        EventMaps.put(342702, new int[]{400, 345});
        EventMaps.put(342696, new int[]{340, 125});
        EventMaps.put(342655, new int[]{245, 190});
        EventMaps.put(342629, new int[]{400, 320});
        EventMaps.put(342601, new int[]{400, 60});
        EventMaps.put(342590, new int[]{370, 240});
        EventMaps.put(342576, new int[]{400, 165});
        EventMaps.put(342572, new int[]{400, 180});
        EventMaps.put(342564, new int[]{355, 210});
        EventMaps.put(342554, new int[]{190, 345});
        EventMaps.put(342549, new int[]{500, 135});
        EventMaps.put(342540, new int[]{500, 115});
        EventMaps.put(342501, new int[]{230, 215});
        EventMaps.put(342500, new int[]{680, 345});
        EventMaps.put(342476, new int[]{200, 235});
        EventMaps.put(342461, new int[]{60, 80});
        EventMaps.put(342447, new int[]{400, 300});
        EventMaps.put(342430, new int[]{490, 365});
        EventMaps.put(342425, new int[]{450, 315});
        EventMaps.put(342420, new int[]{370, 590});
        EventMaps.put(342376, new int[]{550, 235});
        EventMaps.put(342367, new int[]{510, 335});
        EventMaps.put(342363, new int[]{130, 135});
        EventMaps.put(335650, new int[]{400, 55});
        EventMaps.put(331834, new int[]{380, 90});
        EventMaps.put(330179, new int[]{840, 210});
        EventMaps.put(330170, new int[]{400, 280});
        EventMaps.put(328356, new int[]{450, 300});
        EventMaps.put(328000, new int[]{450, 95});
        EventMaps.put(327990, new int[]{400, 305});
        EventMaps.put(325431, new int[]{160, 340});
        EventMaps.put(325415, new int[]{250, 235});
        EventMaps.put(324773, new int[]{280, 75});
        EventMaps.put(324493, new int[]{250, 355});
        EventMaps.put(324484, new int[]{180, 260});
        EventMaps.put(324300, new int[]{1020, 100});
        EventMaps.put(323528, new int[]{360, 275});
        EventMaps.put(322900, new int[]{165, 190});
        EventMaps.put(321006, new int[]{240, 110});
        EventMaps.put(246883, new int[]{620, 160});
        EventMaps.put(100159, new int[]{570, 50});
        EventMaps.put(121524, new int[]{400, 100});
        EventMaps.put(105992, new int[]{400, 80});
        EventMaps.put(104243, new int[]{640, 350});
        EventMaps.put(343983, new int[]{400, 320});
        EventMaps.put(106616, new int[]{20, 100});
        EventMaps.put(300501, new int[]{240, 300});
        EventMaps.put(130682, new int[]{610, 60});
        EventMaps.put(246906, new int[]{320, 90});
        EventMaps.put(100913, new int[]{120, 130});
        EventMaps.put(138845, new int[]{50, 70});
        EventMaps.put(101705, new int[]{400, 220});
        EventMaps.put(246914, new int[]{400, 220});
        EventMaps.put(315704, new int[]{500, 185});
        EventMaps.put(118986, new int[]{400, 70});
        EventMaps.put(109406, new int[]{375, 100});
        EventMaps.put(133709, new int[]{480, 60});
        EventMaps.put(128470, new int[]{400, 260});
        EventMaps.put(128730, new int[]{160, 200});
        EventMaps.put(319172, new int[]{350, 220});
        EventMaps.put(433431, new int[]{100, 140});
        EventMaps.put(433321, new int[]{300, 340});
        EventMaps.put(433391, new int[]{420, 180});
        EventMaps.put(429590, new int[]{180, 110});
        EventMaps.put(429585, new int[]{400, 300});
        EventMaps.put(325056, new int[]{520, 80});
        EventMaps.put(342726, new int[]{400, 240});
        EventMaps.put(342412, new int[]{440, 85});
        EventMaps.put(386223, new int[]{580, 85});
        EventMaps.put(342654, new int[]{300, 185});
        EventMaps.put(121871, new int[]{310, 335});
        EventMaps.put(342741, new int[]{400, 200});
        EventMaps.put(389061, new int[]{510, 255});
        EventMaps.put(342377, new int[]{320, 50});
        EventMaps.put(342408, new int[]{400, 270});
        EventMaps.put(342488, new int[]{320, 175});
        EventMaps.put(369339, new int[]{180, 40});
        EventMaps.put(117200, new int[]{400, 120});
        EventMaps.put(137366, new int[]{30, 55});
        EventMaps.put(401405, new int[]{550, 170});
        EventMaps.put(385015, new int[]{400, 90});
        EventMaps.put(342688, new int[]{400, 225});
        EventMaps.put(342462, new int[]{470, 75});
        EventMaps.put(342413, new int[]{400, 70});
        EventMaps.put(372178, new int[]{110, 135});
        EventMaps.put(342392, new int[]{365, 350});
        EventMaps.put(411142, new int[]{550, 65});
        EventMaps.put(342468, new int[]{65, 350});
        EventMaps.put(101306, new int[]{400, 50});
        EventMaps.put(390325, new int[]{630, 150});
        EventMaps.put(369552, new int[]{700, 165});
        EventMaps.put(328357, new int[]{200, 60});
        EventMaps.put(313927, new int[]{457, 100});
        EventMaps.put(407385, new int[]{585, 285});
        EventMaps.put(368319, new int[]{485, 85});
        EventMaps.put(342384, new int[]{395, 155});
        EventMaps.put(127580, new int[]{600, 60});
        EventMaps.put(297196, new int[]{675, 60});
        EventMaps.put(394484, new int[]{630, 80});
        EventMaps.put(305760, new int[]{350, 70});
        EventMaps.put(342397, new int[]{500, 240});
        EventMaps.put(362666, new int[]{355, 280});
        EventMaps.put(102593, new int[]{400, 145});
        EventMaps.put(362677, new int[]{220, 155});
        EventMaps.put(325440, new int[]{400, 280});
        EventMaps.put(342409, new int[]{260, 145});
        EventMaps.put(151494, new int[]{660, 330});
        EventMaps.put(304937, new int[]{400, 205});
        EventMaps.put(401413, new int[]{320, 255});
        EventMaps.put(150686, new int[]{390, 75});
        EventMaps.put(122061, new int[]{380, 165});
        EventMaps.put(309612, new int[]{400, 80});
        EventMaps.put(342639, new int[]{360, 240});
        EventMaps.put(342689, new int[]{400, 65});
        EventMaps.put(100605, new int[]{470, 310});
        EventMaps.put(342698, new int[]{340, 70});
        EventMaps.put(121335, new int[]{400, 120});
        EventMaps.put(342715, new int[]{240, 185});
        EventMaps.put(321661, new int[]{230, 65});
        EventMaps.put(342618, new int[]{260, 240});
        EventMaps.put(125848, new int[]{430, 330});
        EventMaps.put(342385, new int[]{300, 95});
        EventMaps.put(296281, new int[]{460, 90});
        EventMaps.put(342479, new int[]{350, 170});
        EventMaps.put(322924, new int[]{540, 90});
        EventMaps.put(105820, new int[]{400, 110});
        EventMaps.put(129437, new int[]{400, 315});
        EventMaps.put(386963, new int[]{180, 140});
        EventMaps.put(111913, new int[]{785, 70});
        EventMaps.put(121197, new int[]{645, 145});
        EventMaps.put(342530, new int[]{370, 120});
        EventMaps.put(141039, new int[]{100, 220});
        EventMaps.put(394806, new int[]{460, 160});
        EventMaps.put(391566, new int[]{540, 200});
        EventMaps.put(342378, new int[]{340, 105});
        EventMaps.put(342491, new int[]{400, 360});
        EventMaps.put(322030, new int[]{400, 100});
        EventMaps.put(342389, new int[]{355, 200});
        EventMaps.put(312097, new int[]{675, 260});
        EventMaps.put(342395, new int[]{195, 120});
        EventMaps.put(406787, new int[]{675, 50});
        EventMaps.put(342649, new int[]{550, 320});
        EventMaps.put(152824, new int[]{100, 280});
        EventMaps.put(126930, new int[]{400, 220});
        EventMaps.put(102332, new int[]{660, 150});
        EventMaps.put(310583, new int[]{400, 160});
        EventMaps.put(342414, new int[]{400, 160});
        EventMaps.put(371285, new int[]{680, 350});
        EventMaps.put(383660, new int[]{450, 155});
        EventMaps.put(117505, new int[]{525, 350});
        EventMaps.put(342710, new int[]{330, 140});
        EventMaps.put(306420, new int[]{750, 200});
        EventMaps.put(109790, new int[]{175, 225});
        EventMaps.put(104089, new int[]{525, 120});
        EventMaps.put(387721, new int[]{505, 115});
        EventMaps.put(375855, new int[]{715, 60});
        EventMaps.put(222371, new int[]{400, 240});
        EventMaps.put(105861, new int[]{420, 300});
        EventMaps.put(342642, new int[]{555, 60});
        EventMaps.put(128558, new int[]{725, 315});
        EventMaps.put(103745, new int[]{520, 250});
        EventMaps.put(342403, new int[]{400, 255});
        EventMaps.put(219208, new int[]{775, 275});
        EventMaps.put(342678, new int[]{400, 60});
        EventMaps.put(342559, new int[]{260, 215});
        EventMaps.put(342536, new int[]{155, 100});
        EventMaps.put(132624, new int[]{590, 300});
        EventMaps.put(111459, new int[]{400, 100});
        EventMaps.put(342405, new int[]{768, 120});
        EventMaps.put(342498, new int[]{640, 70});
        EventMaps.put(342714, new int[]{420, 240});
        EventMaps.put(342681, new int[]{400, 260});
        EventMaps.put(389628, new int[]{580, 295});
        EventMaps.put(342520, new int[]{340, 75});
        EventMaps.put(269236, new int[]{500, 140});
        EventMaps.put(362707, new int[]{500, 80});
        EventMaps.put(385483, new int[]{450, 280});
        EventMaps.put(342400, new int[]{400, 180});
        EventMaps.put(329488, new int[]{520, 135});
        EventMaps.put(129337, new int[]{440, 160});
        EventMaps.put(121050, new int[]{600, 300});
        EventMaps.put(148211, new int[]{400, 215});
        EventMaps.put(315771, new int[]{240, 200});
        EventMaps.put(133043, new int[]{770, 125});
        EventMaps.put(133264, new int[]{400, 240});
        EventMaps.put(342496, new int[]{260, 120});
        EventMaps.put(108669, new int[]{400, 260});
        EventMaps.put(102455, new int[]{400, 350});
        EventMaps.put(101178, new int[]{360, 320});
        EventMaps.put(342372, new int[]{400, 100});
        EventMaps.put(342515, new int[]{370, 330});
        EventMaps.put(325281, new int[]{680, 80});
        EventMaps.put(370885, new int[]{430, 200});
        EventMaps.put(344337, new int[]{80, 240});
        EventMaps.put(255172, new int[]{400, 160});
        EventMaps.put(344224, new int[]{620, 340});
        EventMaps.put(344502, new int[]{710, 315});
        EventMaps.put(324777, new int[]{670, 190});
        EventMaps.put(344444, new int[]{400, 80});
        EventMaps.put(117868, new int[]{120, 100});
        EventMaps.put(104545, new int[]{120, 145});
        EventMaps.put(103962, new int[]{400, 80});
        EventMaps.put(132956, new int[]{400, 80});
        EventMaps.put(108800, new int[]{720, 320});
        EventMaps.put(344462, new int[]{200, 60});
        EventMaps.put(344597, new int[]{460, 80});
        EventMaps.put(344406, new int[]{720, 80});
        EventMaps.put(344396, new int[]{470, 80});
        EventMaps.put(344522, new int[]{400, 200});
        EventMaps.put(344358, new int[]{400, 190});
        EventMaps.put(344339, new int[]{680, 260});
        EventMaps.put(344408, new int[]{660, 100});
        EventMaps.put(344368, new int[]{80, 160});
        EventMaps.put(344563, new int[]{480, 340});
        EventMaps.put(344432, new int[]{180, 120});
        EventMaps.put(344347, new int[]{400, 155});
        EventMaps.put(344445, new int[]{140, 280});
        EventMaps.put(344242, new int[]{680, 250});
        EventMaps.put(344603, new int[]{200, 100});
        EventMaps.put(103830, new int[]{540, 140});
        EventMaps.put(344426, new int[]{400, 80});
        EventMaps.put(116110, new int[]{640, 220});
        EventMaps.put(344344, new int[]{400, 130});
        EventMaps.put(344537, new int[]{80, 120});
        EventMaps.put(343208, new int[]{560, 140});
        EventMaps.put(342964, new int[]{440, 320});
        EventMaps.put(304848, new int[]{740, 120});
        EventMaps.put(343021, new int[]{680, 60});
        EventMaps.put(342991, new int[]{320, 80});


    }

    static {


        int[][] map_0 = {
                {2, 540, 200, 50, 50, 0},
                {0, 700, 50, 100, 100, 0},};
        halTuzaklar.put(0, map_0);
        int[][] map_1 = {
                {21, 635, 400, 100, 100, 0},};
        halTuzaklar.put(1, map_1);
        int[][] map_2 = {
                {22, 240, 330, 80, 80, 0},
                {3, 698, 200, 50, 50, 0},};
        halTuzaklar.put(2, map_2);
        int[][] map_5 = {
                {21, 530, 400, 150, 200, 0},
                {21, 165, 400, 100, 60, 0},};
        halTuzaklar.put(5, map_5);
        int[][] map_8 = {
                {22, 585, 165, 80, 80, -27},
                {22, 350, 290, 80, 80, -27},};
        halTuzaklar.put(8, map_8);
        int[][] map_9 = {
                {1, 300, 180, 50, 50, 0},};
        halTuzaklar.put(9, map_9);
        int[][] map_19 = {
                {22, 590, 325, 100, 100, 0},};
        halTuzaklar.put(19, map_19);
        int[][] map_22 = {
                {21, 155, 390, 50, 200, 0},
                {21, 255, 390, 50, 200, 0},
                {21, 350, 390, 50, 200, 0},
                {21, 450, 390, 50, 200, 0},
                {21, 550, 390, 50, 200, 0},
                {21, 650, 390, 50, 200, 0},};
        halTuzaklar.put(22, map_22);
        int[][] map_23 = {
                {21, 450, 390, 50, 200, 0},
                {21, 555, 390, 50, 200, 0},
                {21, 660, 390, 50, 200, 0},
                {21, 345, 390, 50, 200, 0},};
        halTuzaklar.put(23, map_23);
        int[][] map_24 = {
                {3, 660, 50, 100, 100, 0},};
        halTuzaklar.put(24, map_24);
        int[][] map_43 = {
                {21, 335, 395, 90, 50, 0},
                {21, 465, 395, 90, 50, 0},};
        halTuzaklar.put(43, map_43);
        int[][] map_54 = {
                {22, 500, 285, 100, 100, 0},};
        halTuzaklar.put(54, map_54);
        int[][] map_61 = {
                {0, 175, 50, 100, 100, 0},
                {22, 450, 330, 80, 80, 0},};
        halTuzaklar.put(61, map_61);
        int[][] map_67 = {
                {21, 165, 390, 50, 200, 0},
                {21, 290, 390, 50, 200, 0},
                {21, 425, 390, 50, 200, 0},
                {21, 560, 390, 50, 200, 0},
                {21, 675, 390, 50, 200, 0},};
        halTuzaklar.put(67, map_67);
        int[][] map_69 = {
                {22, 300, 280, 70, 70, 40},
                {22, 585, 360, 80, 80, 0},};
        halTuzaklar.put(69, map_69);
        int[][] map_71 = {
                {21, 165, 390, 50, 200, 0},
                {21, 290, 390, 50, 200, 0},
                {21, 425, 390, 50, 200, 0},
                {21, 560, 390, 50, 200, 0},
                {21, 675, 390, 50, 200, 0},};
        halTuzaklar.put(71, map_71);
        int[][] map_72 = {
                {21, 350, 390, 50, 200, 0},
                {21, 455, 390, 50, 200, 0},};
        halTuzaklar.put(72, map_72);
        int[][] map_73 = {
                {3, 175, 190, 70, 70, 0},
                {3, 320, 190, 70, 70, 0},
                {3, 475, 190, 70, 70, 0},
                {3, 625, 190, 70, 70, 0},};
        halTuzaklar.put(73, map_73);
        int[][] map_74 = {
                {22, 200, 325, 100, 100, 0},
                {22, 600, 325, 100, 100, 0},};
        halTuzaklar.put(74, map_74);
        int[][] map_79 = {
                {2, 390, 100, 100, 100, 0},};
        halTuzaklar.put(79, map_79);
        int[][] map_80 = {
                {2, 420, 200, 50, 50, 0},};
        halTuzaklar.put(80, map_80);
        int[][] map_82 = {
                {21, 420, 400, 120, 500, 0},};
        halTuzaklar.put(82, map_82);
        int[][] map_83 = {
                {20, 250, 420, 100, 100, 0},
                {20, 550, 420, 100, 100, 0},};
        halTuzaklar.put(83, map_83);
        int[][] map_84 = {
                {21, 400, 900, 400, 1000, 0},};
        halTuzaklar.put(84, map_84);
        int[][] map_86 = {
                {21, 130, 390, 50, 200, 0},
                {21, 255, 390, 50, 200, 0},
                {21, 390, 390, 50, 200, 0},
                {21, 520, 390, 50, 200, 0},
                {21, 635, 390, 50, 200, 0},};
        halTuzaklar.put(86, map_86);
        int[][] map_90 = {
                {0, 250, 50, 100, 100, 0},
                {0, 570, 50, 100, 100, 0},};
        halTuzaklar.put(90, map_90);
        int[][] map_93 = {
                {22, 290, 250, 100, 100, 31},};
        halTuzaklar.put(93, map_93);
        int[][] map_98 = {
                {3, 380, -47, 80, 80, 0},
                {21, 625, 400, 80, 500, 0},};
        halTuzaklar.put(98, map_98);
        int[][] map_101 = {
                {1, 400, 50, 70, 70, 0},};
        halTuzaklar.put(101, map_101);
        int[][] map_102 = {
                {21, 215, 400, 70, 600, 0},
                {21, 580, 400, 70, 600, 0},
                {22, 300, 330, 80, 80, 0},
                {22, 500, 330, 80, 80, 0},};
        halTuzaklar.put(102, map_102);
        int[][] map_103 = {
                {20, 400, 120, 80, 80, 0},};
        halTuzaklar.put(103, map_103);
        int[][] map_104 = {
                {20, 400, 400, 80, 80, 0},};
        halTuzaklar.put(104, map_104);
        int[][] map_105 = {
                {20, 400, 430, 80, 80, 0},};
        halTuzaklar.put(105, map_105);
        int[][] map_106 = {
                {20, 400, 163, 80, 80, 0},};
        halTuzaklar.put(106, map_106);
        int[][] map_107 = {
                {20, 730, 360, 80, 80, 0},};
        halTuzaklar.put(107, map_107);
        int[][] map_108 = {
                {22, 155, 180, 80, 80, 0},
                {22, 395, 280, 80, 80, 0},
                {22, 440, 360, 80, 80, 0},
                {22, 610, 90, 80, 80, 0},};
        halTuzaklar.put(108, map_108);
        int[][] map_109 = {
                {20, 735, 379, 50, 50, 0},
                {20, 70, 110, 50, 50, 0},};
        halTuzaklar.put(109, map_109);
        int[][] map_111 = {
                {3, 350, 50, 100, 100, 0},
                {20, 50, 350, 50, 50, 0},};
        halTuzaklar.put(111, map_111);
        int[][] map_112 = {
                {21, 300, 400, 70, 800, 0},};
        halTuzaklar.put(112, map_112);
        int[][] map_113 = {
                {21, 170, 400, 50, 600, 0},
                {21, 295, 400, 50, 600, 0},
                {21, 425, 400, 50, 600, 0},
                {21, 560, 400, 50, 600, 0},};
        halTuzaklar.put(113, map_113);
        int[][] map_118 = {
                {21, 240, 400, 50, 600, 0},
                {21, 550, 400, 50, 600, 0},};
        halTuzaklar.put(118, map_118);
        int[][] map_128 = {
                {3, 400, 100, 100, 100, 0},};
        halTuzaklar.put(128, map_128);
        int[][] map_129 = {
                {3, 400, 100, 100, 100, 0},};
        halTuzaklar.put(129, map_129);
        int[][] map_130 = {
                {20, 695, 140, 50, 50, 0},
                {3, 400, 100, 100, 100, 0},};
        halTuzaklar.put(130, map_130);
        int[][] map_131 = {
                {3, 400, 100, 100, 100, 0},};
        halTuzaklar.put(131, map_131);
        int[][] map_134 = {
                {3, 300, 100, 100, 100, 0},};
        halTuzaklar.put(134, map_134);
        int[][] map_141 = {
                {21, 190, 400, 85, 800, 0},
                {21, 600, 400, 85, 800, 0},};
        halTuzaklar.put(141, map_141);
        int[][] map_143 = {
                {20, 240, 200, 140, 140, 180},
                {20, 560, 200, 140, 140, 180},};
        halTuzaklar.put(143, map_143);
        int[][] map_200 = {
                {3, 410, 100, 100, 100, 0},};
        halTuzaklar.put(200, map_200);
        int[][] map_206 = {
                {20, 482, 305, 140, 140, 90},};
        halTuzaklar.put(206, map_206);
        int[][] map_209 = {
                {22, 500, 330, 80, 80, 0},
                {20, 380, 175, 80, 80, -90},};
        halTuzaklar.put(209, map_209);
        int[][] map_301 = {
                {22, 430, 188, 70, 70, 10},
                {22, 102, 170, 70, 70, 20},
                {20, 756, 190, 70, 70, -80},};
        halTuzaklar.put(301, map_301);
        int[][] map_307 = {
                {22, 360, 165, 70, 70, 0},};
        halTuzaklar.put(307, map_307);
    }

    static {
        M_noshaman.add(7);
        M_noshaman.add(8);
        M_noshaman.add(14);
        M_noshaman.add(22);
        M_noshaman.add(23);
        M_noshaman.add(28);
        M_noshaman.add(29);
        M_noshaman.add(54);
        M_noshaman.add(57);
        M_noshaman.add(58);
        M_noshaman.add(59);
        M_noshaman.add(60);
        M_noshaman.add(61);
        M_noshaman.add(70);
        M_noshaman.add(77);
        M_noshaman.add(78);
        M_noshaman.add(87);
        M_noshaman.add(88);
        M_noshaman.add(89);
        M_noshaman.add(92);
        M_noshaman.add(122);
        M_noshaman.add(123);
        M_noshaman.add(124);
        M_noshaman.add(125);
        M_noshaman.add(126);
        M_noshaman.add(1007);
        M_noshaman.add(888);
        M_noshaman.add(560);
        M_noshaman.add(200);
        M_noshaman.add(201);
        M_noshaman.add(202);
        M_noshaman.add(203);
        M_noshaman.add(204);
        M_noshaman.add(205);
        M_noshaman.add(206);
        M_noshaman.add(207);
        M_noshaman.add(208);
        M_noshaman.add(209);
        M_noshaman.add(210);
    }

    static {
        M_transform.add(200);
        M_transform.add(201);
        M_transform.add(202);
        M_transform.add(203);
        M_transform.add(204);
        M_transform.add(205);
        M_transform.add(206);
        M_transform.add(207);
        M_transform.add(208);
        M_transform.add(209);
        M_transform.add(210);
    }

    static {
        M_catchcheese.add(110);
        M_catchcheese.add(111);
        M_catchcheese.add(112);
        M_catchcheese.add(113);
    }

    static {
        M_catchcheese_nosham.add(108);
        M_catchcheese_nosham.add(109);
        M_catchcheese_nosham.add(301);
        M_catchcheese_nosham.add(302);
        M_catchcheese_nosham.add(303);
    }

    private final String lowout;
    public boolean isAllShaman = false;
    public boolean isLama = false;
    public boolean isQuiz = false;
    public boolean isEliteRacing;
    public boolean isTengri;
    public boolean isBot;
    public String LuaKey;
    public Map<String, PlayerConnection> clients = new ConcurrentHashMap<>();
    public Map<Integer, PlayerConnection> clients_p = new ConcurrentHashMap<>();
    public String name;
    public String outname;
    public String lang;
    public MFServer server;
    public long Started = -1;
    public int Round = 99;
    public ReentrantLock lock4;
    public boolean isNormal = false;
    public boolean isMinigame;
    public boolean countStats = true;
    public boolean isTutorial = false;
    public boolean never20secTimer = false;
    public boolean noShaman = false;
    public boolean isEditeur = false;
    public boolean isTotemEditeur = false;
    public int roundTime = 120;
    public int orjroundTime = 120;
    public boolean isMusic = false;
    public boolean isVanilla = false;
    public boolean isBootcamp = false;
    public boolean autoRespawn = false;
    public boolean isSurvivor = false;
    public AtomicInteger mapisReady = new AtomicInteger();
    public boolean isDefilante = false;
    public boolean isRacing = false;
    public boolean isFastRacing = false;
    public boolean is801Room = false;
    public boolean isKutier = false;
    public boolean isMulodrome = false;
    public boolean isTribeHouse = false;
    public boolean isDoubleMap = false;
    public boolean T_20sec;
    public int VampireCode = 0;
    public ScheduledFuture T_newRound;
    public ScheduledFuture T_zamanAt;
    public ScheduledFuture T_Respawn;
    public int currentShamanCode;
    public int forceNextShaman;
    public int forcePerm = -1;
    public map Map;
    public String editeurXML = "";
    public Integer loadedCode = 0;
    public int currentShamanCode2;
    public PlayerConnection[] codes = new PlayerConnection[2];
    public String password = "";
    public map NextMap;
    public Integer NextPerm = null;
    public AtomicInteger numCompleted = new AtomicInteger();
    public int[] Saves;
    public AtomicInteger ObjectID = new AtomicInteger();
    public AtomicInteger TimerID = new AtomicInteger();
    public AtomicInteger ImageID = new AtomicInteger();
    public L_Room l_room;
    public LuaValue lua;
    public boolean isFuncorp = false;
    public boolean autoNewGame = true;
    public boolean autoShaman = true;
    public String LUAFile;
    public HashMap<Integer, ScheduledFuture> runningTimers = new HashMap<>();
    public boolean autoKillAfk = true;
    public boolean AutoTimeLeft = true;
    public boolean ShamanSkills = true;
    public boolean autoScore = true;
    public ArrayList<String> anchors = new ArrayList<>();
    public String community;
    public int maxPlayers = 100;
    public boolean isofficial = false;
    public int Syncer = -1;
    public ArrayList<ArrayList<Integer>> holeCoordinate = new ArrayList<>();
    public ArrayList<ArrayList<Integer>> cheeseCoordinate = new ArrayList<>();
    public HashMap<String, Integer> allShamanSkills = new HashMap<>();
    public int iceCubeCount = 0;
    public ArrayList<ByteBuf> shamanSkillsData;
    public ConcurrentHashMap<Integer, LuaTable> objectList = new ConcurrentHashMap<>();
    public Integer lastHandyMouseID;
    public Integer lastHandyMouseByte;
    public int cloudId = -1;
    public Tribe tribe;
    public int editeurID = -1;
    public boolean physicalConsumables = false;
    public int editeurPerm = 100;
    public String editeurName = "-";
    public String boss = "";
    public long lastMapTime;
    public boolean event = false;
    public List<MusicVideo> musics = Collections.synchronizedList(new ArrayList());
    public MusicVideo currentVideo;
    public int L_music = 0;
    public HashSet<String> videoVotes = new HashSet<>();
    public boolean isLeveTest;
    public boolean isAgil;
    public boolean isHiddenWater;
    public ConcurrentHashMap<Integer, MulodromePlayer> mulodrome = null;
    public int[] mulodromeStats;
    public boolean mulodromeStart;
    public AtomicInteger mulodromeLock = new AtomicInteger();
    public FuncorpSettings funcorpSettings;
    public int leveTestTime = 0;
    public ArrayList<String> arrComp = new ArrayList<>();
    public ArrayList<An> anlar = new ArrayList<>();
    public boolean isBar = false;
    public HashMap<String, String> miceNames = new HashMap<>();
    public int odaBaslangic = 0;
    public boolean ayna = false;
    public HashSet<String> blockedPlayers = new HashSet<>();
    public int forceAyna = 0;
    public boolean TagsEnabled = true;
    public boolean workEditeur;
    public boolean localSync = false;
    public long lastSkip;
    public ConcurrentHashMap<String, Long> drawCache = new ConcurrentHashMap<>();
    public boolean mapstroll;
    public boolean parkourMode;
    public boolean onlyme = false;
    public boolean isIsimRenkDisabled = false;
    private HashSet<Integer> currentJoints = new HashSet<>();
    private HashSet<Integer> currentPhysicObjects = new HashSet<>();
    private ScheduledFuture startMapTimer;
    private ScheduledFuture T_Vampire;
    private ScheduledFuture T_Afk;
    private AtomicInteger lock2 = new AtomicInteger();
    private AtomicInteger lock = new AtomicInteger();
    private PlayerConnection sonKral;
    private ScheduledFuture T_Music;
    private long changedTime;
    private static final int[] keys = new int[]{32, 74, 75, 76, 67, 0, 1, 2, 3, 67};
    public ArrayDeque<DelikVerisi> delikVerileri = new ArrayDeque<>();
    public Long mevcutHid;
    public DelikVerisi mevcutVeri;
    public int sil = 0;
    public long sonEntropi = 0;
    public int entropiSayisi = 0;
    private int mr;
    private int luaInc;
    public ConcurrentHashMap<String, Integer> votes = new ConcurrentHashMap<>();
    public String Quiz_word = "";
    private String[] Quiz_words = new String[]{"pineapple", "cup", "tree", "cube", "mouse", "pencil", "umbrella"};
    public boolean forceEvent;
    public ArrayList<DelikVerisi> veriler = new ArrayList<>();
    public ConcurrentHashMap<Integer, Mob> Mobs = new ConcurrentHashMap<>();
    private int eventIndex = 0;
    public AtomicBoolean parkourWinner = new AtomicBoolean();
    private boolean luaRotation;
    public boolean randomAyna;

    public Room(MFServer server, String name) {
        this.Started = System.currentTimeMillis();
        this.mapisReady.set(-1);

        this.name = name.trim().replace("<", "").replace(">", "");

        this.server = server;
        if (this.name.startsWith("*")) {
            this.lang = "XX";
            this.community = "INT";
            this.outname = this.name.substring(1);
        } else {
            String[] parts = this.name.split("-", 2);
            this.lang = parts[0].toUpperCase();// ""
            this.community = this.lang;
            this.outname = parts[1];
        }
       /* if (this.lang.equals("E2")) {
            this.lang = "XX";
            this.community = "INT";
            this.name="*"+this.outname;
        }*/
        if (Official.matcher(outname).matches()) {
            this.isofficial = true;
        }
        this.lowout = this.outname.toLowerCase();
        this.lock4 = new ReentrantLock();
        if (this.outname.startsWith("#")) {
            String mgname = this.outname.substring(1);
            for (Map.Entry<String, MiniOyun> entry : MFServer.MINIGAMES.entrySet()) {
                String key = entry.getKey();
                if (key.equals("hal_event")) continue;
                MiniOyun value = entry.getValue();
                if (mgname.startsWith(key)) {
                    this.LuaKey = key;
                    this.LUAFile = value.path;
                    this.isMinigame = true;
                    this.countStats = false;
                    if (key.equals("amongus")){
                        password=MFServer.getRandom(12);
                    }
                    break;
                }

            }
        } else if (this.name.startsWith(s_tribe)) {
            this.countStats = false;
            this.isTribeHouse = true;
            this.noShaman = true;
            this.autoRespawn = true;
            this.roundTime = 10000;
            this.never20secTimer = true;

        } else if (this.lowout.startsWith(s_tutorial)) {
            this.countStats = false;
            this.noShaman = true;
            this.never20secTimer = true;
            this.isTutorial = true;

        } else if (this.lowout.startsWith(s_editeur)) {
            this.countStats = false;
            this.isEditeur = true;
            this.never20secTimer = true;

        } else if (this.lowout.startsWith(s_totem)) {
            this.countStats = false;
            this.isTotemEditeur = true;
            this.roundTime = 360;
            this.never20secTimer = true;

        } else if (this.lowout.contains("music")) {
            this.isMusic = true;

        } else if (this.lowout.contains("vanilla")) {
            this.isNormal = true;
            this.isVanilla = true;

        } else if (this.lowout.contains("bootcamp")) {
            this.isBootcamp = true;
            // this.countStats = false;
            this.roundTime = 360;
            this.never20secTimer = true;
            this.autoRespawn = true;
            this.noShaman = true;

        } else if (this.lowout.startsWith("tengri")) {
            this.isTengri = true;
            this.countStats = false;
            this.roundTime = 120;
            this.never20secTimer = true;
            this.noShaman = true;
            this.autoKillAfk = false;

        } else if (this.lowout.contains("survivor")) {
            this.isSurvivor = true;

        } else if (this.lowout.contains("kutier")) {
            this.ShamanSkills = false;
            this.isKutier = true;

        } else if (this.lowout.contains("defilante")) {
            this.isDefilante = true;
            this.noShaman = true;

        } else if (this.name.equals("*eliteracing")) {
            this.isRacing = true;
            this.isEliteRacing = true;
            this.noShaman = true;
            this.never20secTimer = true;
            this.roundTime = 60;

        } else if (this.lowout.contains("fastracing")) {
            this.isRacing = true;
            this.isFastRacing = true;
            this.noShaman = true;
            this.never20secTimer = true;
            this.roundTime = 60;
            this.localSync = true;

        } else if (this.lowout.contains("racing")) {
            this.isRacing = true;
            this.noShaman = true;
            this.never20secTimer = true;
            this.roundTime = 60;
            this.localSync = true;

        } /*else if (this.lowout.equals("lama")) {
            this.isLama = true;
            this.noShaman = true;

        } */ else if (this.lowout.equals("quiz")) {
            this.ShamanSkills = false;
            this.isQuiz = true;
            this.roundTime = 30;
        } else if (this.lowout.contains("village")) {
            this.is801Room = true;
            this.LUAFile = "./minigames/Village.lua";
            this.isMinigame = true;
            this.countStats = false;
            this.autoRespawn = true;
            this.roundTime = 9000;

        } else if (this.lowout.startsWith("shaman")) {
            this.ShamanSkills = false;
            this.isAllShaman = true;
            this.roundTime = 60;
            this.maxPlayers = 15;
        } else {
            this.isNormal = true;
        }
        if ((this.isRacing) && this.lowout.startsWith("bot")) {
            this.isBot = true;
        }
        this.orjroundTime = this.roundTime;

    }

    public static void iptal(ScheduledFuture t) {
        if (t != null) {
            t.cancel(false);
        }
    }

    public ByteBuf mulodromeRoundData() {
        ByteBuf pack = MFServer.getBuf();
        return pack.writeByte(30).writeByte(4).writeByte(this.Round).writeShort(this.mulodromeStats[0]).writeShort(this.mulodromeStats[1]);
    }

    public ByteBuf mulodromeIptal() {
        return MFServer.getBuf().writeByte(30).writeByte(13);
    }

    public void mulodromeAddPoint(Integer pcode, Integer pos) throws IOException {
        int point = 1;
        if (pos.equals(1)) point = 5;
        MulodromePlayer mulodromePlayer = this.mulodrome.get(pcode);
        if (mulodromePlayer != null) {
            this.mulodromeStats[mulodromePlayer.team] += point;

            this.sendAll(mulodromeRoundData());
        }
    }

    public void mulodromeJoin(Integer pcode) {
        MulodromePlayer found = this.mulodrome.get(pcode);
        if (found != null) {
            MFServer.executor.execute(() -> {
                ByteBufOutputStream bs = MFServer.getStream();
                try {
                    bs.writeByte(30);
                    bs.writeByte(15);
                    bs.writeByte(found.team);
                    bs.writeByte(found.pos);
                    bs.writeInt(MFServer.get_avatar(found.code));
                    bs.writeUTF(found.name);
                    bs.writeUTF(found.tribeName);
                    this.sendAll(bs.buffer());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void mulodromeLeft(Integer pcode) throws IOException {
        MulodromePlayer found = this.mulodrome.remove(pcode);
        if (found != null) {
            this.sendAll(MFServer.getBuf().writeByte(30).writeByte(16).writeByte(found.team).writeByte(found.pos));
        }
    }

    public void playNext() {
        videoVotes.clear();
        this.currentVideo = null;
        if (this.musics.size() > 0) {
            MusicVideo musicVideo = this.musics.remove(0);
            iptal(this.T_Music);
            this.currentVideo = musicVideo;
            try {
                this.sendVideo(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.L_music = MFServer.timestamp();

            this.T_Music = MFServer.executor.schedule(this::playNext, this.currentVideo.sure, TimeUnit.SECONDS);

        }
    }


    public void playNextMp3() {
        videoVotes.clear();
        this.currentVideo = null;
        if (this.musics.size() > 0) {
            MusicVideo musicVideo = this.musics.remove(0);
            iptal(this.T_Music);
            this.currentVideo = musicVideo;
            try {
                sendAllOld(26, 12, new Object[]{"http://mp3.miceforce.com/" + currentVideo.vid});
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.L_music = MFServer.timestamp();

            this.T_Music = MFServer.executor.schedule(this::playNextMp3, this.currentVideo.sure, TimeUnit.SECONDS);

        }
    }

    public void sendVideo(PlayerConnection target) throws IOException {
        if (this.currentVideo == null) return;
        int sec = 0;
        if (target != null) {
            sec = MFServer.timestamp() - this.L_music;
        }
        ByteBufOutputStream bs = MFServer.getStream();
        bs.writeByte(5);
        bs.writeByte(72);
        bs.writeUTF(this.currentVideo.vid);
        bs.writeUTF(this.currentVideo.title);
        bs.writeShort(sec);
        bs.writeUTF(this.currentVideo.kullanici);
        if (target != null) {
            target.sendPacket(bs.buffer());
        } else {
            this.sendAll(bs.buffer());
        }

    }

    public void create_lua(SRC cb) throws IOException {
        if (this.l_room == null) {
            this.l_room = new L_Room(this, false);
        }
        LUA_THREAD t = new LUA_THREAD(l_room, cb);
        this.server.threads.add(t);
        this.l_room.t = t;
        t.start();
        for (PlayerConnection pc : this.clients.values()) {
            pc.LuaGame();
        }
    }

    public PlayerConnection getFirstShaman() {
        return this.codes[0];
    }

    public PlayerConnection getSecondShaman() {
        return this.codes[1];
    }

    public int get_highest() {

        ArrayList<Integer> scores = new ArrayList<>();
        for (PlayerConnection client : this.clients.values()) {
            if (client.hidden) continue;
            scores.add(client.score);
        }
        if (scores.size() == 0) return 0;
        int maxScore = Collections.max(scores);
        for (PlayerConnection client : this.clients.values()) {
            if (client.score == maxScore && client.kullanici != null) {
                return client.kullanici.code;
            }
        }
        return 0;
    }

    public PlayerConnection[] getHighestScore() {
        ArrayList<Integer> scores = new ArrayList<>();
        PlayerConnection player = null;
        PlayerConnection player2 = null;
        PlayerConnection[] _codes = new PlayerConnection[2];
        for (PlayerConnection client : this.clients.values()) {
            if (client.hidden) continue;
            scores.add(client.score);
        }
        if (scores.size() == 0) return _codes;

        Integer maxScore = Collections.max(scores);
        if (maxScore == 0) {
            ArrayList<PlayerConnection> cls = new ArrayList<>(this.clients.values());
            if (cls.size() == 0) return _codes;
            Collections.shuffle(cls);
            player = cls.get(0);
            if (this.isDoubleMap && this.clients.size() > 1) {
                player2 = cls.get(1);
            }
        }
        for (PlayerConnection client : this.clients.values()) {
            if (client.score == maxScore) {
                player = client;
                break;
            }
        }
        if (this.isDoubleMap && this.clients.size() > 1) {
            scores.remove(maxScore);

            maxScore = Collections.max(scores);
            for (PlayerConnection client : this.clients.values()) {
                if (client.score == maxScore) {
                    player2 = client;

                }
            }
        }
        _codes[0] = player;
        _codes[1] = player2;
        // allShamanSkills.clear();
        if (player != null && player.kullanici != null) {
            if (player.kullanici.beceriler.get("4") != null)
                player.shamanRespawn = true;
            this.currentShamanCode = player.kullanici.code;
            player.isShaman = true;
            if (ShamanSkills && !isKutier) {
                allShamanSkills = new HashMap<>(player.kullanici.beceriler);
            }
            player.score = 0;
        }
        if (player2 != null && player2.kullanici != null) {
            if (player2.kullanici.beceriler.get("4") != null)
                player2.shamanRespawn = true;
            this.currentShamanCode2 = player2.kullanici.code;
            player2.isShaman = true;
            if (ShamanSkills && !isKutier) {
                for (String skillCode : player2.kullanici.beceriler.keySet()) {
                    if (this.allShamanSkills.containsKey(skillCode)) {
                        if (skillCode.equals("34") || skillCode.equals("23") || skillCode.equals("100")) {
                            continue;
                        }
                        this.allShamanSkills.replace(skillCode,
                                player2.kullanici.beceriler.get(skillCode) + this.allShamanSkills.get(skillCode));
                    } else {
                        this.allShamanSkills.put(skillCode, player2.kullanici.beceriler.get(skillCode));
                    }
                }
            }
            player2.score = 0;
        }

        // this.shamanSkillsData = this.sendShamanSkills();
        return _codes;
    }

    public synchronized void HazirlaSaman() {
        if (this.event) return;
        if (this.currentShamanCode == 0) {
            if (this.forceNextShaman != 0) {

                this.codes = new PlayerConnection[]{this.clients_p.get(this.forceNextShaman), null};
                this.forceNextShaman = 0;
            } else if (this.Map.perm == 0 && Room.M_noshaman.contains(this.Map.id) || this.noShaman || this.Map.perm == 11 || this.Map.perm == 7 || this.Map.perm == 42 || this.Map.perm == 14) {

            } else {
                this.codes = this.getHighestScore();
            }

        }

    }

    public void chech_shaman_feather(PlayerConnection client, int saves) throws IOException {

        if (client.kullanici.samanTur > 2)
            client.kullanici.samanTur = 0;

        if (!this.isSurvivor && !this.isKutier && !client.hidden)
            this.sendAllOld(8, 17, new Object[]{client.kullanici.isim, saves});
        client.score += saves;

        if (saves >= 10 && !client.kullanici.unvanlar.contains(3558)) {
            client.kullanici.unvanlar.add(3558);
            ByteBuf packet = MFServer.pack_old(8, 14, new Object[]{client.kullanici.code, 3558});
            sendAll(packet);
        }
        int min;
        int div;
        client.gorevIlerleme(6, saves * MFServer.genel_carpan);
        int op = 1;
        if (client.kullanici.samanTur == 0) {

            client.kullanici.kurtarma += saves * MFServer.genel_carpan;
            client.giveExp(saves * 10 * MFServer.carpan * MFServer.genel_carpan, true, saves);
            min = 3;
            div = 6;

        } else if (client.kullanici.samanTur == 1) {

            client.gorevIlerleme(8, saves * MFServer.genel_carpan);
            client.kullanici.hardKurtarma += saves * MFServer.genel_carpan;
            client.kullanici.kurtarma += saves * MFServer.genel_carpan;
            client.giveExp(saves * 20 * MFServer.carpan * MFServer.genel_carpan, true, saves);
            min = 4;
            div = 5;
            op = 2;
        } else {

            client.gorevIlerleme(7, saves * MFServer.genel_carpan);
            client.kullanici.divineKurtarma += saves * MFServer.genel_carpan;
            client.kullanici.kurtarma += saves * MFServer.genel_carpan;
            client.giveExp(saves * 30 * MFServer.carpan * MFServer.genel_carpan, true, saves);
            min = 5;
            div = 4;
            op = 3;
        }
        int e = Math.min(min, saves / div);
        if (e > 0) {
            client.addInventoryItem(2253, e);
        }
        client.checkAndRebuildTitleList("hard");
        client.checkAndRebuildTitleList("save");
        client.checkAndRebuildTitleList("divine");

        MFServer.updateToRanking(client.kullanici.code, "saves", saves * op, 4 - op, saves);


    }


    public synchronized void addClient(PlayerConnection client) throws IOException, InterruptedException {
        Kullanici klu = client.kullanici;
        if (klu == null) return;
        if (this.mapisReady.get() == 0) {
            return;
        }
        try {
            this.clients.put(klu.isim, client);
            this.clients_p.put(klu.code, client);
            client.room = this;

            int size = this.clients.size();
            client.sendEnterRoom();
            client.score = 0;
            client.resetPlay();

            client.isDead = true;
            client.sendAnchors.addAll(this.anchors);
                /*for (Map.Entry<String, PlayerConnection> entry : clients.entrySet()) {
                    if (entry.getValue().kullanici == null) clients.remove(entry.getKey());
                }*/
            if (!client.hidden) {
                ByteBufOutputStream bfs = MFServer.getStream();
                bfs.writeShort(36866);

                client.writePlayerData(bfs);
                bfs.writeByte(0);
                bfs.writeByte(1);
                //dat = new PlayerList(this).data();
                this.sendAllOthers(bfs.buffer(), klu.code);
                //  this.sendAllOthers(new NewPlayer(client, client.getPlayerData()).data(), client.kullanici.code);
            }
            if ((size == 1 || (size == 2 && !this.isBootcamp && !this.is801Room && !client.hidden)) && this.autoNewGame
                    && !(this.isMinigame && this.l_room == null)) {

                this.NewRound();


            } else if (size != 1) {
                client.startPlay(true);

                if (this.isBootcamp) {
                    if (this.Map != null) {
                        Integer id = this.Map.id;
                        Integer isLev = MFServer.leveMaps2.get(id);
                        if (isLev != null) {
                            id = isLev;
                        }
                        MFServer.rekorAl(id, (Rekor rekor) -> {
                            try {
                                if (rekor != null) {
                                    client.iletiYolla("record", rekor.ad, rekor.sure);
                                }
                                this.tacYolla();
                            } catch (Exception e) {

                                MFServer.log.warning(MFServer.stackTraceToString(e));
                            }
                        });
                    }
                }
            }
            if (this.l_room != null) {
                this.l_room.eventNewPlayer(klu.isim);
                try {
                    for (Mob mob : Mobs.values()) {
                        ByteBufOutputStream p = MFServer.getStream();
                        p.writeByte(26);
                        p.writeByte(6);
                        p.writeInt(mob.id);
                        p.writeInt(mob.x);
                        p.writeInt(mob.y);
                        p.writeUTF(mob.name);
                        client.sendPacket(p.buffer());
                    }
                } catch (Exception e) {

                }

            }
            if (size == 1 && this.isMinigame) {
                String name = klu.isim;
                this.create_lua((z) -> {
                    this.l_room.eventNewPlayer(name);
                });

            }
            if (!klu.disableSettings) {
                client.LuaGame();
                client.sendPacket(L_Room.packImage("settings_icon.18301", -67, 7, -997, 760, 30));
                client.sendPacket(L_Room.packAddTextArea(-7003, "<a href=\"event:g_settings_ac\">\n\n</a>", 762, 35, 30, 20, 0, 1, 0, false));

            }

            if (this.isLama) {
                client.lamaCount = 0;
                client.sendMessage("<J>Welcome :)");
                client.sendLama(0);
            }
            if (this.isBot) {
                client.iletiYolla("bot_warning");
            }

        } catch (Exception e) {

            MFServer.log.warning(MFServer.stackTraceToString(e));
        } finally {
        }


    }

    public ArrayList<PlayerConnection> findModsInRoom() {
        ArrayList<PlayerConnection> arr = new ArrayList<>();
        for (PlayerConnection pc : this.clients.values()) {
            if (pc.kullanici != null && pc.kullanici.yetki >= 2) {
                arr.add(pc);
            }
        }
        return arr;
    }

    public void removeClient(PlayerConnection client) throws Exception {
        Kullanici kullanici = client.kullanici;
        if (kullanici == null) {
            for (java.util.Map.Entry<String, PlayerConnection> playerConnection : clients.entrySet()) {
                if (playerConnection.getValue() == client) {
                    System.out.println("rr" + playerConnection.getKey());
                    //clients.remove(playerConnection.getKey());
                }
            }
            return;
        }
        this.clients.remove(kullanici.isim);
        this.clients_p.remove(kullanici.code);
        this.votes.remove(client.ip);
        if (client.ip.isEmpty()) {
            System.out.println("ip is empty " + kullanici.isim + " " + kullanici.code);
        } else {
            votes.remove(client.ip);
        }
        if (isQuiz) {
            client.sendPacket(new login_1(client).data());
        }
        if ((this.isLeveTest || this.isAgil || this.isHiddenWater) && kullanici.yetki >= 4) {
            boolean exist = false;
            for (PlayerConnection pc : this.clients.values()) {
                if (pc.kullanici.yetki >= 4) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                this.isLeveTest = false;
                this.isAgil = false;
                this.isHiddenWater = false;
            }
        }
        if (this.clients.isEmpty()) {
            iptal(this.T_Music);
            this.server.closeRoom(this.name, this);
            this.codes = new PlayerConnection[2];
            if (this.l_room != null) {
                this.l_room.set_interrupted(true);
                // this.l_room=null;
            }
            return;
        }
        this.sendAllOld(8, 7, new Object[]{kullanici.code, kullanici.isim});

        if (client.isSyncer) {
            this.setSyncer();
            this.sendSync();
        }
        if (VampireCode == kullanici.code) {
            VampireCode = this.get_highest();

        }

        if (this.l_room != null) {
            try {

                this.l_room.eventPlayerLeft(kullanici.isim);
            } catch (Exception e) {
            }
            ;
        }
        if (client.lamaExec != null) {
            client.lamaExec.cancel(false);
        }
        // this.sendAll(new PlayerLeft(client, client.kullanici.code,
        // client.kullanici.isim).data());
    }

    public void sendMessageStaff(String message) throws IOException {
        this.sendAllStaff(MFServer.pack_message(message));
    }

    public void sendMessage(String message) throws IOException {
        this.sendAll(MFServer.pack_message(message));
    }

    public void checkShouldChangeCarte() throws IOException {
        if (this.isBootcamp || this.autoRespawn || (this.isTribeHouse) || !this.AutoTimeLeft) {

        } else {
            boolean allDead = true;
            for (PlayerConnection client : this.clients.values()) {
                if (!client.isDead) {
                    allDead = false;
                    break;
                }
            }

            if (allDead) {

                this.NewRound();
                return;
            }

            this.check20sec();
        }
    }

    public void check20sec() throws IOException {
        if (!this.never20secTimer && !this.T_20sec) {
            int total = this.playerCount();
            int alive = this.death_alive()[1];
            byte shadead = 0;
            byte shac = 0;
            if (this.getFirstShaman() != null) {
                shac++;
                if (this.getFirstShaman().isDead) {
                    shadead++;
                }
            }
            if (this.getSecondShaman() != null) {
                shac++;
                if (this.getSecondShaman().isDead) {
                    shadead++;
                }
            }
            if (this.T_newRound != null && this.T_newRound.getDelay(TimeUnit.SECONDS) < 20) return;
            if ((total >= 2 && alive <= 1) || (shadead == shac && shac != 0)) {
                this.T_20sec = true;
                this.set_time(20);
                this.sendAll(new O_20Sec(this).data());

            }

        }
    }

    public void set_time(int time) throws IOException {
        iptal(T_zamanAt);
        iptal(T_newRound);
        if (time != 0) {
            this.sendAll(new RoundTime(this, time).data());
        }
        this.roundTime = time;
        this.changedTime = System.currentTimeMillis();
        if (this.autoNewGame) {
            this.T_newRound = MFServer.RoomExecutor.schedule(() -> {
                try {
                    this.NewRound();
                } catch (Exception e) {
                    MFServer.log.warning(MFServer.stackTraceToString(e));
                }
            }, time, TimeUnit.SECONDS);
        }
    }

    public void sendDeath(PlayerConnection cl) throws IOException {
        if (cl != null && cl.kullanici != null) {
            ByteBuf data = new O_Death(cl).data();

            //  addtoBuffer(data,cl.kullanici.code);
            this.sendAll(data);
            cl.isDead = true;
            if (this.l_room != null) {
                this.l_room.eventPlayerDied(cl.kullanici.isim);
            }
            if (cl.checkPoint) {

                long SUREC = (System.currentTimeMillis() - Started);
                if (cl.playerStartTime != 0 && !cl.revived) {

                    SUREC = (System.currentTimeMillis() - cl.playerStartTime);
                }
                cl.checkPointTime += SUREC;
            }
        }

    }

    public void kill_all() throws IOException {
        for (PlayerConnection pc : this.clients.values()) {
            sendDeath(pc);
        }
    }

    public int[] death_alive() {
        int[] a = new int[2];
        for (PlayerConnection client : this.clients.values()) {
            if (client.isDead) {
                a[0]++;
            } else {
                a[1]++;
            }

        }
        return a;
    }

    public ByteBuf getPlayerBuf() throws IOException {

        Collection<PlayerConnection> pcs = this.clients.values().stream().filter(kul -> !kul.hidden).collect(Collectors.toList());
        ByteBufOutputStream bfs = MFServer.getStream();
        bfs.writeShort(36865);
        int size = pcs.size();
        if (isTengri) size += 1;
        size += veriler.size();
        bfs.writeShort(size);
        pcs.forEach((p) -> {
            try {
                p.writePlayerData(bfs);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        for (DelikVerisi delikVerisi : veriler) {
            String _look = "1;0,0,0,0,0,0,0";
            String name = delikVerisi.ad;
            bfs.writeUTF(name);//İSİM
            bfs.writeInt(delikVerisi.code);//PID
            bfs.writeBoolean(false);
            bfs.writeByte(0);
            bfs.writeShort(0);
            bfs.writeBoolean(false);
            bfs.writeShort(0);//ÜNVAN
            bfs.writeByte(1);
            bfs.writeByte(2);
            bfs.writeUTF("0");
            bfs.writeUTF(_look);
            bfs.writeBoolean(true);
            bfs.writeInt(Integer.parseInt("78583a", 16));
            bfs.writeInt(Integer.parseInt("95d9d6", 16));
            bfs.writeInt(0);
            bfs.writeInt(-1);//İsim renk 3906460
        }
        if (isTengri) {

            String _look = "1;0,0,0,0,0,0,0";
            String name = "Tengri";
            if (this.mevcutVeri != null && this.mevcutHid != -1 && this.mevcutVeri.harita == this.Map.id)
                name = this.mevcutVeri.ad;
            bfs.writeUTF(name);//İSİM
            bfs.writeInt(7);//PID
            bfs.writeBoolean(false);
            bfs.writeByte(0);
            bfs.writeShort(0);
            bfs.writeBoolean(false);
            bfs.writeShort(0);//ÜNVAN
            bfs.writeByte(1);
            bfs.writeByte(2);
            bfs.writeUTF("0");
            bfs.writeUTF(_look);
            bfs.writeBoolean(true);
            bfs.writeInt(Integer.parseInt("78583a", 16));
            bfs.writeInt(Integer.parseInt("95d9d6", 16));
            bfs.writeInt(0);
            bfs.writeInt(-1);//İsim renk 3906460

        }
        return bfs.buffer();
    }

    public int playerCount() {
        int i = 0;
        for (PlayerConnection client : this.clients.values()) {
            if (!client.hidden) i++;
        }
        return i;
    }

    public int unique_playerCount() {
        HashSet<String> a = new HashSet<>();
        for (PlayerConnection client : this.clients.values()) {
            if (!client.hidden && !client.vpn) a.add(client.ip);
        }
        return a.size();
    }

    public LuaValue[] getRoundTime2() {
        int time = this.roundTime;
        long long1 = (System.currentTimeMillis() - this.Started);
        long long2 = time * 1000 - (System.currentTimeMillis() - this.changedTime);
        if (this.T_newRound != null && !this.T_newRound.isDone()) {
            long2 = this.T_newRound.getDelay(TimeUnit.MILLISECONDS);
        }
        return new LuaValue[]{LuaInteger.valueOf(long1), LuaInteger.valueOf(long2)};
    }

    public int getRoundTime() {
        int time = this.roundTime;

        return time;
    }

    public void runLuaRotation(String luaCode, String eventAd) throws IOException {
        if (this.l_room != null) {
            this.closeLua();
        }
        this.l_room = new L_Room(this, false);
        this.l_room.developer = "lu";
        this.isMinigame = true;
        this.l_room.eventAd = eventAd;
        LUA_THREAD t = new LUA_THREAD2(this.l_room, luaCode);
        t.setPriority(3);
        t.start();
        this.server.threads.add(t);
        for (PlayerConnection pc : this.clients.values()) {
            pc.LuaGame();
        }
        this.l_room.t = t;

    }

    public void runEvent(String luaCode, String eventAd) throws IOException {
        if (this.l_room != null) {
            this.closeLua();
        }
        this.l_room = new L_Room(this, false);
        this.l_room.developer = "an";
        this.isMinigame = true;
        this.l_room.eventAd = eventAd;
        LUA_THREAD t = new LUA_THREAD2(this.l_room, luaCode);
        t.setPriority(3);
        t.start();
        this.server.threads.add(t);
        for (PlayerConnection pc : this.clients.values()) {
            pc.LuaGame();
        }
        this.l_room.t = t;

    }

    public void closeLua() {

        L_Room lr = this.l_room;
        if (this.playerCount() > 0 & lr != null) {
            List<Integer> array = new ArrayList<>(lr.img_list);
            for (Integer img : array) {
                lr.removeImage(img, "");
            }

            array = new ArrayList<>(lr.text_list);
            for (Integer text : array) {
                lr.removeTextArea(text, "");
            }
        }

        if (lr != null) {
            iptal(lr.snowTimer);
            lr.set_interrupted(true);
            if (lr.t != null)
                lr.t.events.clear();
        }
        runningTimers.forEach((k, v) -> v.cancel(true));
        runningTimers.clear();
        try {
            lr.t.l_room = null;
            lr.t = null;
        } catch (Exception e) {
        }
        /*
         * if (this.l_room.t instanceof LUA_THREAD2) { try { this.NewRound(); }
         * catch (IOException ex) {
         * Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex); }
         * }
         */
        this.autoShaman = true;
        this.autoNewGame = true;
        this.ShamanSkills = true;
        this.AutoTimeLeft = true;
        this.autoKillAfk = true;
        this.localSync = false;
        try {
            lr.room = null;
        } catch (Exception e) {
        }
        this.l_room = null;

    }

    public void sendRemoveShamanSkillCode(int skillCode) {
        ByteBuf packet = MFServer.getBuf().writeByte(5).writeByte(40).writeByte(skillCode).writeByte(1);
        for (PlayerConnection client : this.clients.values()) {
            if (client.isShaman) {
                client.sendPacket(packet.retainedSlice());
            }
        }
        packet.release();
    }

    public void resetRoom() {
        this.arrComp.clear();
        this.isDoubleMap = false;
        this.T_20sec = false;
        this.ObjectID.set(0);
        this.anchors.clear();
        iptal(T_newRound);
        iptal(T_Respawn);
        iptal(T_zamanAt);
        iptal(T_Afk);
        iptal(startMapTimer);
        iptal(T_Vampire);
        this.currentShamanCode = 0;
        this.currentShamanCode2 = 0;
        currentJoints.clear();
        currentPhysicObjects.clear();
        objectList.clear();
        iceCubeCount = 0;
        PlayerConnection s1 = getFirstShaman();
        PlayerConnection s2 = getSecondShaman();
        if (s1 != null)
            s1.score = this.Saves[1];
        if (s2 != null)
            s2.score = this.Saves[2];
        this.Saves = new int[3];
        sonKral = null;
        allShamanSkills.clear();
        Mobs.clear();
        ;
        /*
         * if (shamanSkillsData != null) { for (ByteBuf buf : shamanSkillsData)
         * { if (buf.refCnt() != 0) { buf.release(); } } shamanSkillsData =
         * null; }
         */

        codes = new PlayerConnection[2];
        this.numCompleted.set(0);
        this.holeCoordinate.clear();
        this.cheeseCoordinate.clear();
        this.lastHandyMouseID = null;
        this.lastHandyMouseByte = null;
        this.cloudId = -1;
        this.mapisReady.set(0);
        parkourWinner.set(false);
        parkourMode = false;
    }

    public map chooseMap() {
        luaRotation = false;
        if (this.mapstroll) {
            if (this.isVanilla) {
                int[] arr = new int[]{449323, 449324, 449325, 449328, 449335, 449336, 449339, 449340, 449355, 449356, 449357, 449358, 449359, 449360, 449362, 449369, 449371, 449374, 449375, 319364, 325564, 303902, 303904, 303906, 303908, 303911, 303898, 303897, 303900, 303894, 303890, 303896, 303891, 303889, 303887, 319364, 325564, 508633, 506723, 446008, 446013, 446015, 446022, 446024, 446026, 314109, 314112, 314113, 314119, 314120, 314121, 314122, 314126, 314135, 314136, 329912, 329914, 329916, 329918};
                int Rmap = arr[MFServer.random.nextInt(arr.length)];
                return Harita.alRacing(Rmap, 0);
            } else if (this.isRacing) {
                int[] arr = new int[]{449318, 449319, 449321, 449322, 449326, 449327, 449329, 449331, 449332, 449333, 449334, 449335, 449337, 449339, 449356, 449359, 449361, 449364, 449366, 449367, 449368, 449369, 449370, 449413, 449376, 449377, 449379, 449380, 449382, 538620, 548886, 539662, 548883, 538465, 540511, 541520, 545899, 314108, 314110, 314111, 314114, 314115, 314116, 314118, 314123, 314124, 314127, 314130, 314131, 314132, 314133, 314134, 314137, 314138, 314147, 507431, 506676, 538620, 548886, 539662, 548883, 538465, 540511, 541520, 545899
                };
                int Rmap = arr[MFServer.random.nextInt(arr.length)];
                return Harita.alRacing(Rmap, 17);
            } else {
                int[] arr = new int[]{449370, 449374, 449375};
                int Rmap = arr[MFServer.random.nextInt(arr.length)];
                return Harita.alRacing(Rmap, 17);
            }
        }
        int uniq = this.unique_playerCount();
        if (this.NextPerm != null) {
            int perm = this.NextPerm;
            this.NextPerm = null;
            return this.server.getPerm(perm);
        }
        if (this.NextMap != null) {
            map nm = this.NextMap;
            this.NextMap = null;
            return nm;
        }
        if (this.isVanilla && !this.isAgil) {
            if (false) {
                if (MFServer.random.nextBoolean()) {
                    int size = halTuzaklar.keySet().size();
                    int item = MFServer.random.nextInt(size); // In real life, the Random object should be rather more shared than this
                    int i = 0;
                    int map = 0;
                    for (Integer obj : halTuzaklar.keySet()) {
                        if (i == item) {
                            map = obj;
                            break;
                        }
                        i++;
                    }
                    return VanillaMap.get(map);
                } else {
                    return VanillaMap.get(MAP_LIST[MFServer.random.nextInt(MAP_LIST.length)]);
                }
            } else {
                return VanillaMap.get(MAP_LIST[MFServer.random.nextInt(MAP_LIST.length)]);
            }
        } else if (this.isEditeur) {
            map m = new map();
            m.xml = editeurXML;
            m.id = editeurID;
            m.perm = editeurPerm;
            m.name = editeurName;
            return m;
        } else if (this.isTribeHouse) {
            try {
                if (tribe != null && tribe.kabile != null) {
                    int tmap = tribe.kabile.harita;
                    return Harita.al(tmap);
                }

            } catch (Exception e) {
                e.printStackTrace();
                //MFServer.log.warning(MFServer.stackTraceToString(e));

            }
            return VanillaMap.get(0);
        } else if (this.isTutorial) {

            return VanillaMap.get(900);
        } else if (this.isQuiz) {

            return Harita.al(547991);
        } else if (this.isTotemEditeur) {
            return VanillaMap.get(444);
        } else if (this.isNormal && this.Round % 4 == 0) {
           /* if(MFServer.enableEvent){
                if(MFServer.random.nextBoolean()){
                    int size = halTuzaklar.keySet().size();
                    int item = MFServer.random.nextInt(size); // In real life, the Random object should be rather more shared than this
                    int i = 0;
                    int map=0;
                    for(Integer obj : halTuzaklar.keySet())
                    {
                        if (i == item){
                            map=obj;
                            break;
                        }
                        i++;
                    }
                    return VanillaMap.get(map);
                }
                else{
                    return VanillaMap.get(MAP_LIST[MFServer.random.nextInt(MAP_LIST.length)]);
                }
            }
            else{*/
            return VanillaMap.get(MAP_LIST[MFServer.random.nextInt(MAP_LIST.length)]);
            //}
        }/* else if (this.isNormal && this.isofficial && this.Round % 7 == 0 && MFServer.enableEvent && unique_playerCount() > 3 && MFServer.random.nextBoolean()) {
            luaRotation = true;
            return Harita.al(LUA_ROTATION[MFServer.random.nextInt(LUA_ROTATION.length)]);
        }*/ else {
            int[] permlist = new int[]{1};
            if (this.isBootcamp) {
                permlist = P_Bootcamp;
            } else if (this.isDefilante) {
                permlist = P_Defilante;
            } else if (this.isMusic) {
                permlist = P_Music;
            } else if (this.isLama) {
                permlist = P_Music;
            } else if (this.isRacing) {
                permlist = P_Racing;
            } else if (this.isSurvivor) {
                permlist = P_Survivor;
            } else if (this.isKutier) {
                permlist = P_Kutier;
            } else if (this.isAllShaman) {
                permlist = new int[]{32};
            } else {
                permlist = P_Norm;
            }
            if (this.isMulodrome && this.mulodromeStart) permlist = P_Racing;
            // int perm = permlist[this.server.random.nextInt(permlist.length)];
            if (this.isRacing && this.isLeveTest) {
                if (MFServer.timestamp() - this.leveTestTime > 600) {
                    this.isLeveTest = false;
                    ByteBuf data = null;
                    try {
                        data = MFServer.pack_message("<R>Leve test has been disabled.");

                        for (PlayerConnection pc : this.clients.values()) {
                            if (pc.kullanici.yetki >= 5) {
                                pc.sendPacket(data.retainedSlice());
                            }
                        }
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }
                    MFServer.bitti(data);
                }
                ArrayList<Integer> arrayList = new ArrayList(MFServer.leveMaps.values());
                int Rmap = arrayList.get(MFServer.random.nextInt(arrayList.size()));

                try {
                    ByteBuf data = MFServer.pack_message("<VP>LEVE TEST");
                    for (PlayerConnection pc : this.clients.values()) {
                        if (pc.kullanici.yetki >= 5) {
                            pc.sendPacket(data.retainedSlice());
                        }
                    }
                    data.release();
                } catch (Exception ex) {
                    MFServer.log.warning(MFServer.stackTraceToString(ex));

                }
                return Harita.al(Rmap);

            } else if (this.isAgil && (this.isRacing || this.isVanilla || this.isSurvivor || this.isKutier || this.isBootcamp || this.isNormal)) {
                if (MFServer.timestamp() - this.leveTestTime > 600) {
                    this.isAgil = false;
                }
                if (this.isRacing) {
                    int[] arr = new int[]{460120, 460126, 460128, 460130, 460246, 460265, 460253, 460255, 460258, 460458, 461100, 461110, 461111, 461114, 461119, 461124, 461128, 461139, 461140, 461142, 461157, 461159, 450363};
                    int Rmap = arr[MFServer.random.nextInt(arr.length)];
                    return Harita.alRacing(Rmap, 17);
                } else if (this.isVanilla) {

                    int[] arr = new int[]{462990, 462991, 465195};
                    int Rmap = arr[MFServer.random.nextInt(arr.length)];
                    return Harita.alRacing(Rmap, 0);
                } else if (this.isNormal) {

                    int[] arr = new int[]{465910, 465929, 465930, 465931, 465932};
                    int Rmap = arr[MFServer.random.nextInt(arr.length)];
                    return Harita.alRacing(Rmap, 1);
                } else if (this.isKutier || this.isSurvivor) {

                    int[] arr = new int[]{492943, 492948, 492952, 492955, 492964, 492965, 492966, 492967};
                    int Rmap = arr[MFServer.random.nextInt(arr.length)];
                    return Harita.alRacing(Rmap, 10);
                } else if (this.isBootcamp) {

                    int[] arr = new int[]{506312, 506316, 506318, 506321, 506322, 506323, 506327, 506329, 506330, 552824, 552825, 552827, 552828, 552922};
                    int Rmap = arr[MFServer.random.nextInt(arr.length)];
                    return Harita.alRacing(Rmap, 13);
                } else {

                    return MFServer.getPerms(permlist);
                }
            } else if (this.isRacing && this.isHiddenWater) {

                if (MFServer.timestamp() - this.leveTestTime > 600) {
                    this.isHiddenWater = false;
                }
                int Rmap = MFServer.hiddenWaterMaps.get(this.server.random.nextInt(MFServer.hiddenWaterMaps.size()));
                return Harita.alRacing(Rmap, 17);
            }
            return MFServer.getPerms(permlist);

        }
    }

    public void addConjuration(int x, int y, int time) throws IOException {
        this.sendAllOld(4, 14, new Object[]{x, y});
        MFServer.executor.schedule(() -> {
            try {
                this.removeConjuration(this.Round, x, y);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, time, TimeUnit.SECONDS);

    }

    public ArrayList<ByteBuf> sendShamanSkills() {
        ArrayList<ByteBuf> byteBuf = new ArrayList<>();
        if (this.allShamanSkills != null && !this.allShamanSkills.isEmpty()) {

            for (java.util.Map.Entry<String, Integer> entry : this.allShamanSkills.entrySet()) {
                Integer skillCode = Integer.valueOf(entry.getKey());
                Integer skillCount = entry.getValue();
                if (this.isSurvivor & MFServer.forbidden_survivor.contains(skillCode)) {
                    continue;
                }
                if (MFServer.snormal.contains(skillCode)) {
                    if (MFServer.smult.contains(skillCode)) {
                        byteBuf.add(this.sendShamanSkillCode(skillCode, 1));
                    } else if (skillCode.equals(81)) {
                        byteBuf.add(this.sendShamanSkillCode(skillCode, skillCount));
                    } else {

                        byteBuf.add(this.sendShamanSkillCode(skillCode, skillCount));
                    }
                } else if (MFServer.sfull.contains(skillCode)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100));
                } else if (skillCode.equals(1) || skillCode.equals(45)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100 + skillCount * 10));
                } else if (skillCode.equals(2)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100 + skillCount * 4));
                } else if (skillCode.equals(22) || skillCode.equals(72)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 20 + skillCount * 5));
                } else if (skillCode.equals(10) || skillCode.equals(13)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 3));
                } else if (skillCode.equals(12)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, skillCount * 4));
                } else if (skillCode.equals(20)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 110 + skillCount * 4));
                } else if (skillCode.equals(23)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100 - skillCount * 10));
                } else if (skillCode.equals(28) || skillCode.equals(65) || skillCode.equals(74)
                        || skillCode.equals(85)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, skillCount * 2));
                } else if (skillCode.equals(32)) {
                    this.iceCubeCount += skillCount;
                } else if (skillCode.equals(40)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100 + skillCount * 8));
                } else if (skillCode.equals(42) || skillCode.equals(43)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 250 - skillCount * 10));
                } else if (skillCode.equals(49)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 96 - skillCount * 6));
                } else if (skillCode.equals(54)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 130));
                } else if (skillCode.equals(68)) {
                    byteBuf.add(this.sendShamanSkillCode(skillCode, 100 - skillCount * 4));
                } else {
                    if (!skillCode.equals(89)) {
                        continue;
                    }
                    byteBuf.add(this.sendShamanSkillCode(54, 100 - skillCount * 4));
                    byteBuf.add(this.sendShamanSkillCode(49, 100 - skillCount * 4));
                }
            }
        }
        return byteBuf;
    }

    public ByteBuf sendShamanSkillCode(int skillCode, int skillId) {

        return MFServer.getBuf().writeByte(8).writeByte(10).writeByte(skillCode).writeByte(skillId);
    }

    public void removeConjuration(int round, int x, int y) throws IOException {
        if (round == this.Round) {
            this.sendAllOld(4, 15, new Object[]{x, y});
        }

    }

    public ArrayList<PlayerConnection> findNearMices(int pcode, int x, int y) {
        ArrayList<PlayerConnection> list = new ArrayList<>();
        for (PlayerConnection client : this.clients.values()) {
            if (x - 65 <= client.x && client.x <= x + 65 && client.y >= y - 65 && client.y <= y + 65 && !client.isDead
                    && client.kullanici != null && client.kullanici.code != pcode) {
                list.add(client);
            }
        }
        return list;
    }

    public void NewRound() {
        // this.lock.isLocked();
        Long tt = System.currentTimeMillis();
        ScheduledExecutorService[] exc = new ScheduledExecutorService[]{MFServer.RoomExecutor};
        if (this.isofficial) exc[0] = MFServer.RoomExecutorOfficial;
        if (this.lock4.getQueueLength() > 3) return;
        exc[0].execute(() -> {
           /* if(isEditeur){

                try {
                    sendAll(MFServer.pack_old(14, 14, new Object[]{""}));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
            if (this.isEditeur && !this.workEditeur) return;
            this.lock4.lock();

            //<C><P /><Z><S><S X="493" L="163" Y="383" H="10" P="0,0,0.3,0.2,0,0,0,0" T="0" /></S><D><DS X="484" Y="361" /></D><O /></Z></C>
            // if (this.isEditeur && !this.workEditeur) return;
            try {
                if ((this.Round++) % 10 == 0) {
                    if (this.isMulodrome) {
                        this.isMulodrome = false;
                        this.mulodrome.clear();
                        this.mulodromeStart = false;
                        this.sendAll(this.mulodromeIptal());
                    }
                }
                if (this.Round == 100) {
                    this.Round = PlayerConnection.betweenRandom(1, 7) * 10;
                }
                //this.Started = -1;
                int count = this.unique_playerCount();
                this.odaBaslangic = count;
               /* this.motor=true;
                this.daires =new HashSet<>();
                this.daires.add(new Daire(1, 232, 338,15,"78f4622cedf011e7b5f688d7f643024a.png"));*/
                if (count == 0)
                    return;

                try {
                    if (this.event) {
                        this.event = false;
                        try {
                            this.closeLua();
                        } catch (Exception e) {

                        }
                        this.isMinigame = false;
                    }
                    //     Date date = new Date();
                    //  LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    //  int ay = localDate.getMonth().getValue();

                    if (this.l_room != null && this.l_room.developer.equals("an")) this.event = true;
                    else if ((MFServer.enableEvent && ((this.Round % 13 == 0 && this.isofficial && (this.isNormal || this.isVanilla) && count >= 7)) || forceEvent)) {
                        forceEvent = false;
                        //Date date = new Date();
                        // LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        //int gun = localDate.getDayOfMonth();
                        String[] lns = new String[]{"vday_event"};
                        String ln = lns[eventIndex++ % lns.length];
                        try {
                            String code = Files.readString(Paths.get("minigames/" + ln + ".lua"));
                            if (!code.isEmpty()) {
                                this.runEvent(code, "mg17");
                                return;
                            }
                        } catch (Exception e) {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if ((this.isRacing || this.isBootcamp || randomAyna) && this.forceAyna == 0) {
                    if (MFServer.random.nextInt(4) == 2) {
                        ayna = MFServer.random.nextBoolean();
                    }
                }
                if (this.forceAyna != 0) {
                    this.ayna = this.forceAyna == 1;
                    this.forceAyna = 0;
                }
                if (count > 3 && !isAllShaman) {
                    for (java.util.Map.Entry<String, PlayerConnection> us : clients.entrySet()) {
                        PlayerConnection client = us.getValue();
                        Kullanici kullanici = client.kullanici;
                        if (kullanici == null) {
                            //  clients.remove(us.getKey());
                            continue;
                        }
                        if (kullanici.code == this.currentShamanCode) {
                            chech_shaman_feather(client, this.Saves[1]);
                        }
                        if (kullanici.code == this.currentShamanCode2) {
                            chech_shaman_feather(client, this.Saves[2]);
                        }
                        kullanici.tur += 1;
                        if (!this.isFuncorp && !this.isMinigame && !this.isTribeHouse && !this.isEditeur && !this.isTotemEditeur && !this.isTutorial) {
                            client.gorevIlerleme(5, this.orjroundTime);
                        }
                    }
                }
                int alive = this.death_alive()[1];
                if ((this.isSurvivor || this.isRacing || this.isAllShaman || this.isKutier)) {
                    for (java.util.Map.Entry<String, PlayerConnection> us : clients.entrySet()) {
                        PlayerConnection client = us.getValue();
                        Kullanici kullanici = client.kullanici;
                        if (kullanici == null) {
                            //clients.remove(us.getKey());
                            continue;
                        }
                        if (!client.isNew & !client.isAfk) {
                            if (!client.isDead && !client.isShaman && !client.isVampire) {
                                client.score += 10;
                            }
                            if (count > 3 && countStats) {

                                try {
                                    if (this.isRacing) {
                                        if (client.isComp) {

                                            kullanici.extraStats.racing_comp_rounds += 1 * MFServer.genel_carpan;
                                            client.addInventoryItem(2254, 1 * MFServer.genel_carpan);
                                        }

                                        kullanici.extraStats.racing_rounds += 1 * MFServer.genel_carpan;
                                        client.checkAndRebuildTitleList("racing");
                                    } else if (this.isAllShaman && !client.isDead && alive < 4) {
                                        client.score += 20;
                                        kullanici.extraStats.allSham += 1 * MFServer.genel_carpan;
                                        client.checkAndRebuildTitleList("shaman");
                                    } else {

                                        kullanici.extraStats.survivor_rounds += 1 * MFServer.genel_carpan;
                                        if (client.isShaman) {
                                            kullanici.extraStats.survivor_shaman += 1 * MFServer.genel_carpan;
                                            int killed = 0;
                                            for (PlayerConnection cl : clients.values()) {
                                                if (cl.isDead & !cl.isAfk & !cl.isNew & !cl.isShaman)
                                                    killed++;
                                            }
                                            killed = killed * MFServer.genel_carpan;
                                            if (killed > 0) {
                                                client.addInventoryItem(2260, Math.min(killed, 10));
                                                if (this.isKutier) {
                                                    client.kullanici.extraStats.kutier_killed += killed;
                                                    client.gorevIlerleme(10, killed);
                                                }
                                                client.gorevIlerleme(11, killed);
                                                kullanici.extraStats.survivor_killed += killed;
                                                client.giveExp(killed * 2 * MFServer.carpan, false, 0);
                                            }

                                        }
                                        if (!client.isDead && !client.isShaman && !client.isVampire) {
                                            if (this.isKutier) {
                                                kullanici.extraStats.kutier_survivor += 1 * MFServer.genel_carpan;
                                                client.gorevIlerleme(4, 1 * MFServer.genel_carpan);
                                                //client.log.put("kutier_survivor", client.log.get("kutier_survivor") + 1);
                                            } else {
                                                client.gorevIlerleme(3, 1 * MFServer.genel_carpan);
                                            }
                                            client.giveExp(5 * MFServer.carpan * MFServer.genel_carpan, false, 0);
                                            kullanici.extraStats.survivor += 1 * MFServer.genel_carpan;
                                            //  client.log.put("survivor", client.log.get("survivor") + 1);

                                            kullanici.peynir += 272 * MFServer.genel_carpan;
                                        }

                                        client.checkAndRebuildTitleList("survivor");
                                        client.checkAndRebuildTitleList("kutier");
                                    }
                                } catch (Exception e) {
                                    MFServer.log.warning(MFServer.stackTraceToString(e));
                                }

                            }
                        } else {
                            client.isNew = false;
                        }

                    }
                }
                boolean oldParkour = this.parkourMode;
                try {
                    this.resetRoom();


                } catch (Exception e) {
                    MFServer.log.warning(MFServer.stackTraceToString(e));
                }
                try {
                    this.setSyncer();
                    for (PlayerConnection client : clients.values()) {
                        client.resetPlay();
                    }
                    if (luaRotation) {
                        closeLua();
                        isMinigame = false;
                    }

                } catch (Exception e) {
                    MFServer.log.warning(MFServer.stackTraceToString(e));
                }


                this.roundTime = this.orjroundTime;
                int time = this.roundTime;
                // for(StackTraceElement c :

                map m = this.chooseMap();

                if (luaRotation) {
                    try {
                        String code = Files.readString(Paths.get("minigames/@" + m.id + ".lua"));
                        if (!code.isEmpty()) {
                            this.runLuaRotation(code, String.valueOf(m.id));
                        }
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));

                    }
                }
                if (forcePerm != -1) {
                    m.perm = forcePerm;
                    forcePerm = -1;
                }
                this.Map = m;
                if (this.Map.perm == 8 || MFServer.doubleMaps.contains(this.Map.id) || this.Map.perm == 32)
                    this.isDoubleMap = true;
                try {
                    if (this.autoShaman && !this.luaRotation) {
                        this.HazirlaSaman();
                    }
                } catch (Exception e) {
                    MFServer.log.warning(MFServer.stackTraceToString(e));
                }                // this.isBot=(this.isRacing || isFastRacing) && clients.size()<5 && this.isofficial;
                int roundId = Round;

                if ((MFServer.enableEvent2 && (count > 4 && this.isofficial) && !isMinigame) && false) {
                    if (EventMaps.containsKey(this.Map.id)) {
                        int[] event_coords = EventMaps.get(this.Map.id);
                        for (PlayerConnection pc : clients.values()) {
                            pc.event_coords = event_coords;
                            pc.sendPacket(L_Room.packImage("empty-png.24486", -98750, 7, -1, event_coords[0], event_coords[1]));
                            pc.bindKeyboard(32, true, true);
                        }
                    }
                }
              /*  if(this.Map instanceof VanillaMap && halTuzaklar.containsKey(Map.id)){
                    int[][] ints =halTuzaklar.get(Map.id);
                    for(int[] vObj: ints){
                        for (int kObj: new int[]{20,60,80}){
                            MFServer.executor.schedule(()->{
                                if(roundId!=Round)return;
                                try {
                                    ByteBufOutputStream p = MFServer.getStream();
                                    p.writeByte(5);
                                    p.writeByte(47);
                                    p.writeShort(vObj[0]);
                                    p.writeShort(vObj[1]);
                                    p.writeShort(vObj[2]);
                                    p.writeShort(vObj[3]);
                                    p.writeShort(vObj[4]);
                                    p.writeShort(vObj[5]);
                                    this.sendAll(p.buffer());
                                } catch (IOException ex) {
                                    Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            },kObj,TimeUnit.SECONDS);
                        }
                    }
                }*/
                try {
                    if (this.isBot) {

                        veriler.clear();
                        ArrayList<Document> delikler = new ArrayList<>();
                        MFServer.colHoleLog.aggregate(Lists.newArrayList(new Document("$match", new Document("map", this.Map.id)), new Document("$sample", new Document("size", 10)))).into(delikler);
                        //MFServer.colHoleLog.find(eq("map", this.Map.id)).sort(new Document("_id", -1)).limit(500).into(delikler);
                        Collections.shuffle(delikler);
                        int i = 0;
                        for (Document delik : delikler) {

                            String ad = "";
                            Kullanici av = MFServer.colKullanici.find(eq("_id", delik.getInteger("code"))).projection(include("isim")).first();
                            if (av != null) ad = av.isim;
                            ad = "*" + ad;
                            DelikVerisi delikVerisi = new DelikVerisi(delik.getInteger("map"), 0L, ad);
                            delikVerisi.parse(delik.get("data", org.bson.types.Binary.class).getData());
                            delikVerisi.code = delik.getInteger("code") + 100_000_000;
                            if (delikVerisi.ayna == ayna && delikVerisi.delik > 5_000 && !clients_p.containsKey(delikVerisi.code)) {
                                veriler.add(delikVerisi);
                                i++;
                            }
                            if (i == 3) break;
                        }
                        for (DelikVerisi delikVerisi : veriler) {
                            int r = this.Round;
                            int ttt = (int) (delikVerisi.delik / 10);
                            MFServer.TengriExecutor.schedule(() -> {
                                if (r != this.Round) return;
                                ByteBuf bf = MFServer.getBuf();
                                try {
                                    bf.writeByte(8).writeByte(6).writeByte(0).writeInt(delikVerisi.code).writeShort(0).writeByte(1).writeShort(ttt);
                                    this.sendAll(bf);

                                } catch (Exception e) {
                                    MFServer.log.warning(MFServer.stackTraceToString(e));
                                }
                            }, delikVerisi.delik, TimeUnit.MILLISECONDS);
                            MFServer.TengriExecutor.schedule(() -> {
                                //   System.out.println(r+" "+this.Round+" R");
                                if (r != this.Round) return;
                                try {
                                    this.sendAll(new GetCheese(delikVerisi.code).data());

                                } catch (Exception e) {

                                    MFServer.log.warning(MFServer.stackTraceToString(e));
                                }
                            }, delikVerisi.pey, TimeUnit.MILLISECONDS);

                            int i2 = 0;
                            for (byte[] bytes : delikVerisi.arrs) {

                                MFServer.TengriExecutor.schedule(() -> {
                                    //  System.out.println(r+" "+this.Round+" R");
                                    if (r != this.Round) return;
                                    ByteBuf bf = MFServer.getBuf();
                                    try {
                                        bf.writeByte(4);
                                        bf.writeByte(4);

                                        bf.writeInt(delikVerisi.code);
                                        bf.writeInt(r);
                                        bf.writeBytes(bytes);
                                        //bf.writeShort(2000);
                                        //     bf.writeBytes(bytes,0,11);

                                        this.sendAll(bf);


                                    } catch (Exception e) {
                                        MFServer.log.warning(MFServer.stackTraceToString(e));
                                    }
                                }, delikVerisi.vakitler.get(i2), TimeUnit.MILLISECONDS);
                                i2++;
                            }
                        }

                    }
                } catch (Exception e) {
                    MFServer.log.warning(e.getMessage());
                    MFServer.log.warning(MFServer.stackTraceToString(e));

                }

                // this.sendAll(new MapData(this).data());
                // this.sendAll(new MapData(this, Map).data());
                ByteBuf dat = new MapData(this, Map).data();
                this.sendAll(dat);
                T_zamanAt = exc[0].schedule(() -> {
                    try {
                        ByteBuf dat2 = new RoundTime(this, time).data();

                        this.sendAll(dat2);
                        for (PlayerConnection pc : codes) {
                            if (pc != null && pc.kullanici != null && pc.kullanici.beceriler.containsKey("67")) {
                                pc.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(48).writeByte(0).writeInt(pc.kullanici.code));
                            }
                        }
                    } catch (Exception e) {

                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }
                }, 700, TimeUnit.MILLISECONDS);
                //  this.sendAll(MFServer.getBuf().writeBytes(this.server.hexStringToByteArray("050200000330001d5c000001b77801ad96c14e834010865f65b36729ccee4221b25cda430f1a9be841bd34354513d3d2261aad6fef0c210d2cb366631bda4226e5e3ff676667296755b914b3b59552ccefadfcb9d699c24b2b8fabfaab6eea5ddd7c7ec4c7d5ebbed92cd6dbedfebbae9bdbf5414dde0f6f52c455f95c95f7f8114f56ea2291e2c14a40c4d2cae40a8f89c6afa22b3aa4b8b1324ff0bca7bfbd6c0a23c58e9efe68a5a1f8c24a95b6e016a928c623a3a20f6d6f66a01174503ca15a2fb4551986cc39a4069fcebeca0c553022d5b4354edc9346e5050e556a1e99e1ed98cb4b2221d50c136321f5f1c8549ab37e1e536becbf91778c9da15317f9c599a6606a14d647295ff414c6d9840263bc731dd09a261b1b074ac6ff913ae190d93948a0f6724a0e05ce115e6514e23cc7d134427a8d072115b32affa84f10d3d0a07075026ae7adf74b0ebe36a22674902aac3173be3181e6bc4ba4040f452a54e76c166078e25477c36838e1bcabdc0c8cd34dcc1c9e9ace781fda6669a893dbd5c0cbecd6a4c2679ea67b58363ddb85ce70b138d904f0767bdfb9a6cd9571ae0cc61d6434aa0fe73bf714c8d9cd637c499893ff3bfa89f1ad219e55bf43d8b95c000e5f4261722048616c6c6f7765656e5700")));

                /*
                 * if (shamanSkillsData != null) { for (ByteBuf buf :
                 * this.shamanSkillsData) {
                 *
                 * this.sendAll(buf.retainedSlice()); } }
                 */

                //this.sendAll(MFServer.getBuf().writeByte(100).writeByte(6).writeShort(0));
                // this.sendAll(MFServer.getBuf().writeByte(60).writeByte(4).writeShort(1));
                for (PlayerConnection client : this.clients.values()) {

                    if (this.isBootcamp || this.isDefilante || this.playerCount() < 2) {
                        client.sendStartMap(false);
                    } else {
                        client.sendStartMap(true);
                    }
                    if (client.isShaman && !this.isEditeur && this.ShamanSkills && !isKutier) {
                        try {

                            Beceriler.Yolla(client, false);
                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }
                    if (client.hidden) client.isDead = true;
                    try {

                        if (this.isMulodrome && this.mulodromeStart && !this.mulodrome.containsKey(client.kullanici.code))
                            client.isDead = true;
                        if (this.isQuiz && this.codes.length != 0) {
                            if (this.codes[0].kullanici.code != client.kullanici.code) client.isDead = true;
                            else {
                                this.Quiz_word = Quiz_words[MFServer.random.nextInt(Quiz_words.length)];
                                this.sendAllOld(25, 3, new Object[]{});
                                client.sendMessage("Tell others by drawing: " + Quiz_word);
                                client.sendMessage("<VP>Press o to activate drawing");
                            }

                            client.sendPacket(new login_1(client).data());
                        }
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }
                }
                ByteBuf bff = getPlayerBuf();
                for (PlayerConnection pc : clients.values()) {
                    if ((pc.onlyme || onlyme) && !pc.isDead && (isBootcamp || isRacing || isDefilante || parkourMode || oldParkour || onlyme)) {
                        ByteBufOutputStream bfs = MFServer.getStream();
                        bfs.writeShort(36865);
                        bfs.writeShort(1);
                        pc.writePlayerData(bfs);
                        pc.sendPacket(bfs);

                    } else {

                        pc.sendPacket(bff.retainedSlice());
                    }
                }
                bff.release();

                if (!this.isBootcamp && !this.isDefilante && this.clients.size() >= 2 & !this.isEditeur) {
                    this.startMapTimer = exc[0].schedule(() -> {
                        for (PlayerConnection client : this.clients.values()) {
                            client.sendStartMap(false);
                        }
                    }, 3, TimeUnit.SECONDS);
                }
                if (this.isAllShaman) {
                    for (PlayerConnection pc : clients.values()) {

                        ByteBuf data = MFServer.getBuf().writeByte(8).writeByte(12).writeInt(pc.kullanici.code).writeByte(pc.kullanici.samanTur).writeShort(pc.kullanici.seviye.seviye).writeShort(pc.kullanici.ikon);
                        pc.sendPacket(data);
                        pc.sendPacket(MFServer.getBuf().writeByte(8).writeByte(21).writeInt(pc.kullanici.code).writeInt(0));
                        pc.isShaman = true;
                    }
                } else {
                    try {
                        this.sendAll(new Shaman(this).data());

                        this.sendSync();

                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }
                }

                this.Started = System.currentTimeMillis();
                this.changedTime = System.currentTimeMillis();
                if (this.clients.size() == 0) return;
                if (this.autoNewGame) {

                    this.T_newRound = exc[0].schedule(() -> {
                        try {
                            this.NewRound();
                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }, time, TimeUnit.SECONDS);
                    if (this.autoRespawn) {
                        this.T_Respawn = exc[0].schedule(() -> {
                            try {
                                this.respawn_players();
                            } catch (Exception e) {
                                MFServer.log.warning(MFServer.stackTraceToString(e));
                            }
                        }, 2, TimeUnit.SECONDS);
                    }
                }
                if (this.autoKillAfk) {
                    this.T_Afk = exc[0].schedule(() -> {
                        try {
                            this.kill_afk_players();
                        } catch (Exception e) {

                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }, 30, TimeUnit.SECONDS);
                }
                if (this.isRacing) {
                    ByteBuf val = MFServer.getBuf().writeByte(5).writeByte(1).writeByte(this.Round % 10 - 1)
                            .writeInt(this.get_highest());
                    this.sendAll(val);
                }
                if (this.Map.perm == 11) {

                    T_Vampire = exc[0].schedule(() -> {

                        PlayerConnection pc = new ArrayList<>(this.clients.values()).get(MFServer.random.nextInt(this.clients.size()));

                        if (pc != null) {
                            try {
                                VampireCode = pc.kullanici.code;
                                this.setVampirePlayer(pc.kullanici.isim);
                            } catch (Exception e) {

                                MFServer.log.warning(MFServer.stackTraceToString(e));
                            }
                        }
                    }, 10, TimeUnit.SECONDS);

                }

                boolean transformed = false;

                boolean catchMap = false;
                if (this.Map.perm.equals(0) || this.Map.perm.equals(2)) {
                    if (M_catchcheese.contains(this.Map.id)) {
                        this.sendAllOld(8, 23, new Object[]{this.currentShamanCode});
                        this.sendAllOld(5, 19, new Object[]{this.currentShamanCode});
                        catchMap = true;
                    } else if (M_catchcheese_nosham.contains(this.Map.id)) {
                        this.sendAllOld(8, 23, new Object[]{this.currentShamanCode});
                        this.sendAllOld(5, 19, new Object[]{this.currentShamanCode});
                        this.sendAllOld(8, 20, new Object[]{this.currentShamanCode});
                        catchMap = true;

                    } else if (this.Map.id >= 200 && this.Map.id <= 211) {
                        transformed = true;
                    }
                }
                if (catchMap) {
                    for (PlayerConnection pc : clients.values()) pc.shamanRespawn = false;
                }
                if (this.Map.perm == 14) {
                    transformed = true;
                }
                for (PlayerConnection pc : this.clients.values()) {
                    try {

                        pc.send_isim_renk();
                        pc.startPlay(false);
                        if (transformed)
                            pc.giveTransformice();
                    } catch (Exception e) {
                        MFServer.log.warning(MFServer.stackTraceToString(e));
                    }


                }
                if (!this.isMusic) videoVotes.clear();

                // System.out.println((this.mevcutVeri.harita == this.Map.id)+" "+this.mevcutVeri.harita+" "+this.Map.id);
                if (this.isTengri && this.mevcutVeri != null && this.mevcutHid != -1 && this.mevcutVeri.harita == this.Map.id) {
                    //  ByteBuf data = new NewPlayer("(Tengri)" + this.mevcutVeri.ad + "#" + TENGRI_DATA).data();

                    //   this.sendAll(data);
                    int r = this.Round;
                    int ttt = (int) (this.mevcutVeri.delik / 10);
                    int i = 0;
                    MFServer.TengriExecutor.schedule(() -> {
                        //   System.out.println(r+" "+this.Round+" R");
                        if (r != this.Round) return;
                        ByteBuf bf = MFServer.getBuf();
                        try {
                            bf.writeByte(8).writeByte(6).writeByte(0).writeInt(7).writeShort(777).writeByte(1).writeShort(ttt);
                            this.sendAllStaff(bf);

                        } catch (Exception e) {
                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }, this.mevcutVeri.delik, TimeUnit.MILLISECONDS);
                    MFServer.TengriExecutor.schedule(() -> {
                        //   System.out.println(r+" "+this.Round+" R");
                        if (r != this.Round) return;
                        try {
                            this.sendAllStaff(new GetCheese(7).data());

                        } catch (Exception e) {

                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    }, this.mevcutVeri.pey, TimeUnit.MILLISECONDS);
                    long Tping = 0;
                    for (byte[] bytes : this.mevcutVeri.arrs) {
                        Tping += this.mevcutVeri.pingler.get(i);
                        int I = i;
                        long tping = Tping;
                        if (i % 5 == 0) {
                            Tping = 0;
                        }
                        MFServer.TengriExecutor.schedule(() -> {
                            //   System.out.println(r+" "+this.Round+" R");
                            if (r != this.Round) return;
                            ByteBuf bf = MFServer.getBuf();
                            try {
                                bf.writeByte(4);
                                bf.writeByte(4);

                                bf.writeInt(7);
                                bf.writeInt(r);
                                bf.writeBytes(bytes);
                                //     bf.writeBytes(bytes,0,11);

                                this.sendAllStaff(bf);
                                if (I % 5 == 0 && tping != 0) {
                                    sendMessageStaff("Average ping is " + (tping / 5) + "ms");

                                }

                            } catch (Exception e) {
                                MFServer.log.warning(MFServer.stackTraceToString(e));
                            }
                        }, this.mevcutVeri.vakitler.get(i), TimeUnit.MILLISECONDS);
                        i++;
                    }

                    this.mevcutVeri = null;
                    this.mevcutHid = 0L;
                }
                if (this.isTotemEditeur) {
                    for (PlayerConnection cl : this.clients.values()) {
                        if (cl.kullanici != null) cl.sendTotem(400, 200);
                    }
                }
                if (this.isBootcamp) {
                    for (PlayerConnection cl : this.clients.values()) {
                        cl.LuaGame();
                        cl.bindKeyboard(88, true, true);
                        cl.bindKeyboard(120, true, true);
                    }
                }
                if ((this.isRacing || this.isBootcamp || this.isDefilante || oldParkour) && !this.isFuncorp && this.password.isEmpty()) {

                    Integer id = this.Map.id;
                    Integer isLev = MFServer.leveMaps2.get(id);
                    if (isLev != null) {
                        id = isLev;
                    }
                    MFServer.rekorAl(id, (Rekor rekor) -> {
                        try {
                            if (rekor != null) {
                                iletiYolla("record", rekor.ad, rekor.sure);
                            }
                            this.tacYolla();
                        } catch (Exception e) {

                            MFServer.log.warning(MFServer.stackTraceToString(e));
                        }
                    });

                }
                this.mapisReady.set(1);

                if (this.l_room != null) {
                    this.l_room.eventNewGame();
                }
                if (countStats) {
                    for (PlayerConnection playerConnection : clients.values()) {
                        if (PlayerConnection.isVPN(playerConnection.ip) && System.currentTimeMillis() - playerConnection.lastVPNSent > 1_000 * 60 * 30) {
                            playerConnection.lastVPNSent = System.currentTimeMillis();
                            playerConnection.sendChatServer("<R>WARNING " + playerConnection.kullanici.isim + " is probably using Proxy/VPN");
                        }
                    }
                }
                if (isEditeur) {

                    sendAll(MFServer.pack_old(14, 14, new Object[]{""}));
                }
                if (isTengri) {
                    for (PlayerConnection playerConnection : clients.values()) {
                        playerConnection.LuaGame();
                        playerConnection.bindMouse(true);
                    }
                }


            } catch (Exception e) {

                MFServer.log.warning(MFServer.stackTraceToString(e));

                // String name = "";
                // MFServer.log.warning("[" + this.name + "]" + MFServer.stackTraceToString(e));

            } finally {

                this.lock4.unlock();
            }

        });

    }


    public void iletiYolla(String anahtar, Object... args) throws IOException {
        for (PlayerConnection pc : clients.values()) {
            pc.iletiYolla(anahtar, args);
        }
    }

    public void setVampirePlayer(String playerName) throws IOException {
        if (playerName != null) {
            PlayerConnection client = this.clients.get(playerName);
            if (client != null && client.kullanici != null) {
                client.isVampire = true;
                ByteBuf packet = MFServer.getBuf().writeByte(8).writeByte(66).writeInt(client.kullanici.code);
                this.sendAll(packet);

                if (this.l_room != null) {
                    this.l_room.eventPlayerVampire(playerName);

                }
            }
        }
        if ((this.isSurvivor || this.isKutier) && !this.never20secTimer && !this.T_20sec) {
            if (this.clients.values().stream().filter(k -> !k.isDead).allMatch(k -> k.isVampire)) {
                this.T_20sec = true;
                this.set_time(5);
                //this.sendAll(new O_20Sec(this).data());
            }
        }

    }


    public void setVampirePlayer(int pCode) throws IOException {
        ByteBuf packet = MFServer.getBuf().writeByte(8).writeByte(66).writeInt(pCode);
        this.sendAll(packet);
        PlayerConnection client = this.clients_p.get(pCode);
        if (client != null && client.kullanici != null) {
            client.isVampire = true;


            if (this.l_room != null) {
                this.l_room.eventPlayerVampire(client.kullanici.isim);
            }
        }
    }

    public void respawn_players() throws IOException {
        for (PlayerConnection client : this.clients.values()) {
            if (client.isDead && client.kullanici != null) {
                this.respawn_player(client);
            }
        }

        iptal(T_Respawn);
        this.T_Respawn = MFServer.RoomExecutor.schedule(() -> {
            try {
                this.respawn_players();
            } catch (Exception e) {
                MFServer.log.warning(MFServer.stackTraceToString(e));
            }
        }, 2, TimeUnit.SECONDS);
    }

    public void respawn_player(PlayerConnection client) throws IOException {
        if (!client.hidden) {
            client.isDead = false;
            client.hasCheese = false;
            client.playerStartTime = System.currentTimeMillis();
            ByteBufOutputStream bfs = MFServer.getStream();
            bfs.writeShort(36866);

            client.writePlayerData(bfs);
            bfs.writeByte(0);
            bfs.writeByte(0);
            //dat = new PlayerList(this).data();
            //   this.sendAll(bfs.buffer());
            ByteBuf bff = bfs.buffer();
            for (PlayerConnection pc : clients.values()) {
                if ((pc.onlyme || onlyme) && !pc.isDead && (isBootcamp || isRacing || parkourMode || onlyme) && pc == client) {

                    pc.sendPacket(bff.retainedSlice());

                } else if (!pc.onlyme && !onlyme) {
                    pc.sendPacket(bff.retainedSlice());

                }
            }
            bff.release();
            if (this.countStats && (this.isDefilante || this.isRacing || this.isBootcamp || parkourMode)) {
                client.sistem = new Sistem();
                byte[] bf = client.sistem.arrayList.get(0);
                Sistem.writeBoolean(ayna, 24, bf);
                Sistem.writeLong(System.currentTimeMillis(), 0, client.sistem.arrayList.get(0));

            }
            if (this.isBootcamp) {
                //   ByteBuf data = new NewPlayer(client, client.getPlayerData(), 0).data();
                // addtoBuffer(data,client.kullanici.code);
                // this.sendAll(data);
                if (client == this.sonKral) tacYolla();


                // client.koordinatProtos.clearKoordinatlar();
                //  client.koordinatProtos.setStart(client.playerStartTime);
                //  client.coordinates.setLength(0);
            } else {
                // this.sendAll(new NewPlayer(client, client.getPlayerData(), 1).data());
            }
            if (this.l_room != null) {
                this.l_room.eventPlayerRespawn(client.kullanici.isim);
            }

        }
    }

    private void sendAllStaff(ByteBuf packet) {
        for (PlayerConnection client : clients.values()) {
            if (client.kullanici.yetki >= 2) client.sendPacket(packet.retainedSlice());
        }
        // packet.release();
        if (packet.refCnt() > 0) packet.release();
    }

    public void sendAll(ByteBuf packet) throws IOException {
        //this.lastMapTime=System.currentTimeMillis();
        // packet.retain();
        for (PlayerConnection client : clients.values()) {
            client.sendPacket(packet.retainedSlice());
        }
        // packet.release();
        if (packet.refCnt() > 0) packet.release();
    }

    public void sendAllOthers(ByteBuf packet, int code) throws IOException {

        // packet.retain();
        for (PlayerConnection client : clients.values()) {
            if (client.kullanici != null && client.kullanici.code != code) {
                client.sendPacket(packet.retainedSlice());
            }
        }

        packet.release();
    }

    public void sendAllOldOthers(int Token1, int Token2, Object[] values, int code) throws IOException {
        ByteBuf packet = MFServer.pack_old(Token1, Token2, values);
        // packet.retain();
        for (PlayerConnection client : clients.values()) {
            if (client.kullanici != null && client.kullanici.code != code) {
                client.sendPacket(packet.retainedSlice());
            }
        }

        packet.release();
    }

    public void sendStopPlayer(int playerCode, boolean activate) throws IOException {
        this.sendAll(MFServer.getBuf().writeByte(5).writeByte(34).writeInt(playerCode).writeBoolean(activate));
    }

    public void sendAllOld(int Token1, int Token2, Object[] values) throws IOException {
        ByteBuf packet = MFServer.pack_old(Token1, Token2, values);
        // packet.retain();
        //  System.out.println(MFServer.bytesToHex(packet.copy()));
        for (PlayerConnection client : clients.values()) {
            client.sendPacket(packet.retainedSlice());

        }

        packet.release();
    }

    public void removeAllObjects() throws IOException {
        this.sendAll(MFServer.getBuf().writeByte(5).writeByte(32));
        this.anchors.clear();
        // this.objectList.clear();
    }

    public void addBoost(int x, int y, final int i, int angle) throws IOException {
        this.sendAll(MFServer.getBuf().writeByte(5).writeByte(14).writeShort(x).writeShort(y).writeByte(i)
                .writeShort(angle));
    }

    public void addJoint(int id, int physicObject1, int physicObject2, LuaTable jointDef) {

        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(30);

            if (id > 32767) {
                id = 32767;
            }
            if (physicObject1 > 32767) {
                physicObject1 = 32767;
            }
            if (physicObject2 > 32767) {
                physicObject2 = 32767;
            }

            p.writeShort(id);
            p.writeShort(physicObject1);
            p.writeShort(physicObject2);
            int type = 0;
            try {
                type = jointDef.get("type").checkint();
                if (type > 3) {
                    type = 0;
                }
            } catch (LuaError ex) {

            }
            p.writeByte(type);
            try {
                String p1xy = jointDef.get("point1").checkjstring();
                p.writeBoolean(true);
                if (p1xy.contains(",")) {
                    String[] x = StringUtils.split(p1xy, ",");
                    if (StringUtils.isNumeric(x[0]) && StringUtils.isNumeric(x[1])) {
                        p.writeShort(Integer.valueOf(x[0]));
                        p.writeShort(Integer.valueOf(x[1]));
                    } else {
                        p.writeInt(0);
                    }
                } else {
                    p.writeInt(0);
                }
            } catch (LuaError ex) {
                p.writeBoolean(false);
                p.writeInt(0);
            }

            try {
                String p2xy = jointDef.get("point2").checkjstring();
                p.writeBoolean(true);
                if (p2xy.contains(",")) {
                    String[] x = StringUtils.split(p2xy, ",");
                    if (StringUtils.isNumeric(x[0]) && StringUtils.isNumeric(x[1])) {
                        p.writeShort(Integer.valueOf(x[0]));
                        p.writeShort(Integer.valueOf(x[1]));
                    } else {
                        p.writeInt(0);
                    }
                } else {
                    p.writeInt(0);
                }
            } catch (LuaError ex) {
                p.writeBoolean(false);
                p.writeInt(0);
            }

            try {
                String p3xy = jointDef.get("point3").checkjstring();
                p.writeBoolean(true);
                if (p3xy.contains(",")) {
                    String[] x = StringUtils.split(p3xy, ",");
                    if (StringUtils.isNumeric(x[0]) && StringUtils.isNumeric(x[1])) {
                        p.writeShort(Integer.valueOf(x[0]));
                        p.writeShort(Integer.valueOf(x[1]));
                    } else {
                        p.writeInt(0);
                    }
                } else {
                    p.writeInt(0);
                }
            } catch (LuaError ex) {
                p.writeBoolean(false);
                p.writeInt(0);
            }
            p.writeBoolean(false);
            p.writeInt(0);

            int frequency = 0;
            try {
                frequency = MFServer.parseLuaFloat(jointDef.get("frequency").toString());
                if (frequency > 1032) {
                    frequency = 1032;
                }
            } catch (LuaError ex) {

            }
            p.writeShort(frequency);
            int damping = 0;
            try {
                damping = MFServer.parseLuaFloat(("damping").toString());
                if (damping > 1032) {
                    damping = 1032;
                }
            } catch (LuaError ex) {

            }
            p.writeShort(damping);

            int line = 0;
            try {
                line = MFServer.parseLuaFloat(("line").toString());
                if (line > 1032) {
                    line = 1032;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(line);

            int color = 0;
            try {
                color = jointDef.get("color").checkint();

                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeInt(color);

            int alpha = 100;
            try {
                alpha = MFServer.parseLuaFloat(jointDef.get("alpha").toString());
                if (alpha > 1032) {
                    alpha = 1032;
                }
            } catch (LuaError ex) {

            }
            p.writeShort(alpha);

            boolean foreground = false;
            try {
                foreground = jointDef.get("foreground").toboolean();

            } catch (LuaError ex) {

            }

            p.writeBoolean(foreground);

            try {
                String axis = jointDef.get("axis").checkjstring();

                if (axis.contains(",")) {
                    String[] x = StringUtils.split(axis, ",");
                    if (StringUtils.isNumeric(x[0]) && StringUtils.isNumeric(x[1])) {
                        p.writeShort(Integer.valueOf(x[0]));
                        p.writeShort(Integer.valueOf(x[1]));
                    } else {
                        p.writeInt(0);
                    }
                } else {
                    p.writeInt(0);
                }
            } catch (LuaError ex) {
                p.writeInt(0);
            }

            int angle = 0;
            try {
                angle = jointDef.get("angle").toint();
                if (angle > 32767) {
                    angle = 32767;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(angle);

            int limit1 = 0;
            try {
                limit1 = MFServer.parseLuaFloat(jointDef.get("limit1").toString());
                if (limit1 > 1032) {
                    limit1 = 1032;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(limit1);

            int limit2 = 0;
            try {
                limit2 = MFServer.parseLuaFloat(jointDef.get("limit2").toString());
                if (limit2 > 1032) {
                    limit2 = 1032;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(limit2);

            int forceMotor = 0;
            try {
                forceMotor = MFServer.parseLuaFloat(jointDef.get("forceMotor").toString());
                if (forceMotor > 1032) {
                    forceMotor = 1032;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(forceMotor);

            int speedMotor = 0;
            try {
                speedMotor = MFServer.parseLuaFloat(jointDef.get("speedMotor").toString());
                if (speedMotor > 1032) {
                    speedMotor = 1032;
                }
                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeShort(speedMotor);

            int ratio = 100;
            try {
                ratio = MFServer.parseLuaFloat(jointDef.get("ratio").toString());
                if (ratio > 1032) {
                    ratio = 1032;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(ratio);

            this.sendAll(p.buffer());
            this.currentJoints.add(id);

        } catch (IOException error) {
            error.printStackTrace();
        }
    }

    public void addPhysicObject(int id, int x, int y, LuaTable bodyDef, String target) {

        try {
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(28);

            if (id > 32767) {
                id = 32767;
            }
            if (x > 32767) {
                x = 32767;
            }
            if (y > 32767) {
                y = 32767;
            }

            p.writeShort(id);
            boolean dynamic = false;
            try {
                dynamic = bodyDef.get("dynamic").toboolean();

            } catch (LuaError ex) {
            }
            p.writeBoolean(dynamic);

            int type = 0;
            try {
                type = bodyDef.get("type").checkint();
                if (type > 127) {
                    type = 127;
                }
            } catch (LuaError ex) {
            }
            p.writeByte(type);

            p.writeShort(x);
            p.writeShort(y);

            int width = 0;
            try {
                width = bodyDef.get("width").checkint();
                if (width > 32767) {
                    width = 32767;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(width);

            int height = 0;
            try {
                height = bodyDef.get("height").checkint();
                if (height > 32767) {
                    height = 32767;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(height);

            boolean foreground = false;
            try {
                foreground = bodyDef.get("foreground").toboolean();

            } catch (LuaError ex) {
            }
            p.writeBoolean(foreground);

            int friction = 0;
            try {
                friction = MFServer.parseLuaFloat(bodyDef.get("friction").toString());
                if (friction > 1032) {
                    friction = 1032;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(friction);

            int restitution = 0;
            try {
                restitution = MFServer.parseLuaFloat(bodyDef.get("restitution").toString());
                if (restitution > 1032) {
                    restitution = 1032;
                }
            } catch (LuaError ignored) {
            }
            p.writeShort(restitution);

            int angle = 0;
            try {
                angle = MFServer.parseLuaFloat(bodyDef.get("angle").toString());
                if (angle > 1032) {
                    angle = 1032;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(angle);

            int color = 0;
            try {
                color = bodyDef.get("color").checkint();

                p.writeBoolean(true);
            } catch (LuaError ex) {
                p.writeBoolean(false);
            }
            p.writeInt(color);

            boolean miceCollision = true;
            try {
                miceCollision = bodyDef.get("miceCollision").toboolean();

            } catch (LuaError ignored) {
            }
            p.writeBoolean(miceCollision);

            boolean groundCollision = true;
            try {
                groundCollision = bodyDef.get("groundCollision").toboolean();

            } catch (LuaError ignored) {
            }
            p.writeBoolean(groundCollision);

            boolean fixedRotation = false;
            try {
                fixedRotation = bodyDef.get("fixedRotation").toboolean();

            } catch (LuaError ex) {
            }
            p.writeBoolean(fixedRotation);

            int mass = 0;
            try {
                mass = bodyDef.get("mass").checkint();
                if (mass > 32767) {
                    mass = 32767;
                }
            } catch (LuaError ignored) {
            }
            p.writeShort(mass);

            int linearDamping = 0;
            try {
                linearDamping = MFServer.parseLuaFloat(bodyDef.get("linearDamping").toString());
                if (linearDamping > 1032) {
                    linearDamping = 1032;
                }
            } catch (LuaError ignored) {
            }
            p.writeShort(linearDamping);

            int angularDamping = 0;
            try {
                angularDamping = MFServer.parseLuaFloat(bodyDef.get("linearDamping").toString());
                if (angularDamping > 1032) {
                    angularDamping = 1032;
                }
            } catch (LuaError ex) {
            }
            p.writeShort(angularDamping);

            p.writeBoolean(false);
            p.writeShort(0);


            if (target.isEmpty()) {
                this.sendAll(p.buffer());
            } else {
                PlayerConnection pc = clients.get(target);
                if (pc != null) pc.sendPacket(p.buffer());
            }

            this.currentPhysicObjects.add(id);

        } catch (IOException error) {
            error.printStackTrace();
        }
    }

    public void addDefilante(int x, int y, int id) throws IOException {

        this.sendAll(
                MFServer.getBuf().writeByte(5).writeByte(14).writeShort(x).writeShort(y).writeByte(id).writeShort(0));
    }

    public void displayParticle(int i, int x, int y) {
        displayParticle(i, x, y, 0, 0, 0, 0, "");
    }

    public void displayParticle(int particleType, int xPosition, int yPosition, int xSpeed, int ySpeed,
                                int xAcceleration, int yAcceleration, String targetPlayer) {

        ByteBufOutputStream p = MFServer.getStream();
        try {
            p.writeByte(29);
            p.writeByte(27);

            if (particleType > 128) {
                particleType = 128;
            }
            if (xPosition > 32767) {
                xPosition = 32767;
            }
            if (yPosition > 32767) {
                yPosition = 32767;
            }
            if (xSpeed > 1032) {
                xSpeed = 1032;
            }
            if (ySpeed > 1032) {
                ySpeed = 1032;
            }
            if (xAcceleration > 1032) {
                xAcceleration = 1032;
            }
            if (yAcceleration > 1032) {
                yAcceleration = 1032;
            }

            p.writeByte(particleType);
            p.writeShort(xPosition);
            p.writeShort(yPosition);
            p.writeShort(xSpeed);
            p.writeShort(ySpeed);
            p.writeShort(xAcceleration);
            p.writeShort(yAcceleration);

            if (targetPlayer.length() == 0) {
                this.sendAll(p.buffer());
            } else {
                sendTarget(targetPlayer, p.buffer());
            }

        } catch (IOException error) {
            error.printStackTrace();
        }

    }

    void sendTarget(String name, ByteBuf buf) throws IOException {
        PlayerConnection pc = this.clients.get(name);
        if (pc != null) {
            pc.sendPacket(buf);
        }
    }

    public void removeObject(int objectId) throws IOException {

        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(4);
        p.writeByte(8);
        p.writeInt(objectId);
        p.writeBoolean(true);
        this.objectList.remove(objectId);
        this.sendAll(p.buffer());

    }

    public void setSyncer() {
        if (clients.size() < 1) return;
        try {
            final HashMap<Long, Integer> names = new HashMap<>();
            for (final PlayerConnection client : this.clients.values()) {
                if (client.kullanici == null) continue;
                names.put((client.isAfk) ? 300 : (client.ping), client.kullanici.code);
            }
            if (names.size() == 0) return;
            final SortedSet<Long> keys = new TreeSet<>(names.keySet());
            int found = names.get(keys.first());
            if (clients.containsKey("Tengri")) {
                found = clients.get("Tengri").kullanici.code;
            }
            this.Syncer = found;
        } catch (Exception e) {
            MFServer.log.warning(MFServer.stackTraceToString(e));
        }

    }

    public void sendSync() throws IOException {
        if (clients.size() < 1) return;
        if (this.localSync) {
            for (PlayerConnection pc : this.clients.values()) {
                ByteBuf packet = MFServer.pack_old(8, 21, new Object[]{pc.kullanici.code, ""});
                pc.sendPacket(packet);

                pc.isSyncer = true;
            }
        } else if (this.Syncer != -1) {
            PlayerConnection pc = this.clients_p.get(this.Syncer);
            if (pc != null) {
                pc.isSyncer = true;
            }
            this.sendAllOld(8, 21, new Object[]{this.Syncer, ""});

        }
    }

    public void kraliVer(SRC<PlayerConnection> src) {
        PlayerConnection[] kullanici1 = new PlayerConnection[1];
        final AtomicLong rek = new AtomicLong();
        ArrayList<PlayerConnection> pcsToCount = new ArrayList<>();
        for (PlayerConnection pc : this.clients.values()) {
            if (pc.kullanici.peynir > 100_000) {
                pcsToCount.add(pc);
            }
        }
        MFServer.RoomExecutor.execute(() -> {
            for (PlayerConnection pc : pcsToCount) {
                long count = MFServer.colRekorlar.countDocuments(new Document("rekorlar.0.ad", pc.kullanici.isim).append("$or", Arrays.asList(
                        new Document("rekorlar.0.deadline", 0),
                        new Document("rekorlar.0.deadline", new Document("$exists", false)))));
                if (count >= rek.get()) {
                    rek.set(count);
                    kullanici1[0] = pc;
                }
            }
            src.onResult(kullanici1[0]);
        });


    }

    public void sendEmoticons(PlayerConnection sendTo) {
        clients.values().stream().filter(k -> k.kullanici.evInfo.emoticon > 0 && !k.isDead).forEach((cl) -> {
            ByteBufOutputStream p = MFServer.getStream();
            try {
                p.writeByte(29);

                p.writeByte(19);
                p.writeInt(12 * cl.kullanici.code);
                String img = "";
                switch (cl.kullanici.evInfo.emoticon){
                    case 1:
                        img = "emoji_1-png.22005";
                        break;
                    case 2:
                        img = "emoji_2-png.22006";
                        break;
                    case 3:
                        img = "emoji_3-png.22007";
                        break;
                    case 4:
                        img = "emoji_4-png.22008";
                        break;
                    case 5:
                        img = "emoji_5-png.22009";
                        break;
                    case 6:
                        img = "emoji_6-png.22010";
                        break;
                    case 7:
                        img = "emoji_7-png.22011";
                        break;
                    case 8:
                        img = "emoji_8-png.22012";
                        break;
                }
                if (img.isEmpty())return;
                p.writeUTF(img);
                p.writeByte(2);
                p.writeInt(cl.kullanici.code);
                if(sendTo.kullanici.code == cl.kullanici.code){
                    p.writeShort(-15);
                    p.writeShort(-97);
                } else {
                    p.writeShort(-15);
                    p.writeShort(-86);
                }
                sendTo.sendPacket(p.buffer());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }

    public void tacYolla() throws IOException {
        kraliVer((PlayerConnection kral) -> {

            this.sonKral = kral;
            if (kral != null) {

                ByteBufOutputStream p = MFServer.getStream();
                try {
                    p.writeByte(29);

                    p.writeByte(19);

                    p.writeInt(987);
                    p.writeUTF("9f26ee3a398f11e78ef9109836a51f7d.png");
                    p.writeByte(2);
                    p.writeInt(kral.kullanici.code);
                    p.writeShort(-25);
                    p.writeShort(-70);
                    ByteBuf packet = p.buffer();
                    for (PlayerConnection client : clients.values()) {

                        if (client.crown) client.sendPacket(packet.retainedSlice());
                    }
                    packet.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //MFServer.bitti(p.buffer());
                // packet.release();

            }
        });

    }

    public void showColorPicker(int id, String target, int defaultColor, String title) {
        try {

            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(29);
            p.writeByte(32);
            p.writeInt(id);
            p.writeInt(defaultColor);
            p.writeUTF(title);

            if (target.length() == 0) {
                this.sendAll(p.buffer());
            } else {
                sendTarget(target, p.buffer());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int addShamanObject(int code, int x, int y, int angle, int vx, int vy, boolean ghost,
                               ArrayList<Integer> shaman_colors, int pC) throws IOException {

        int oi = this.ObjectID.addAndGet(1);

        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(5);
        p.writeByte(20);
        p.writeInt(oi);
        p.writeShort(code);
        p.writeShort(x);
        p.writeShort(y);
        p.writeShort(angle);
        p.writeByte(vx);
        p.writeByte(vy);
        p.writeBoolean(ghost);
        if (shaman_colors != null) {
            p.writeByte(shaman_colors.size());
            for (Integer color : shaman_colors) {
                p.writeInt(color);
            }
        } else {
            p.writeByte(0);
        }
        if (pC == 0) {

            this.sendAll(p.buffer());
        } else {
            PlayerConnection pc = clients_p.get(pC);
            if (pc != null) pc.sendPacket(p.buffer());
            //this.sendAllOthers(p.buffer(), pC);
        }
        if (this.l_room != null) {

            LuaTable tab = new LuaTable();
            tab.set("x", x);
            tab.set("y", y);
            tab.set("vx", vx);
            tab.set("vy", vy);
            tab.set("angle", angle);
            tab.set("ghost", ghost ? 1 : 0);
            tab.set("type", code);
            tab.set("id", oi);
            this.objectList.put(oi, tab);

        }
        return oi;

    }

    public int addShamanObject(int code, int x, int y) throws IOException {
        return this.addShamanObject(code, x, y, 0, 0, 0, false, null, 0);
    }

    public int addShamanObject(int code, int x, int y, int angle, int vx, int vy) throws IOException {
        return this.addShamanObject(code, x, y, angle, vx, vy, false, null, 0);
    }

    public int addShamanObject(int code, int x, int y, int angle, int vx, int vy, boolean ghost,
                               ArrayList<Integer> shaman_colors) throws IOException {
        return this.addShamanObject(code, x, y, angle, vx, vy, ghost, shaman_colors, 0);

        // if (this.luaMinigame != null) {
        // Map<String, Integer> object = new HashMap();
        // object.put("x", x);
        // object.put("y", y);
        // object.put("angle", angle);
        // object.put("ghost", ghost);
        // object.put("type", code);
        // object.put("id", this.ObjectID);
        // this.objectList.put(this.ObjectID, object);
        // this.refreshObjectList();
        // }
    }

    public void kill_afk_players() throws IOException {
        if (this.isEditeur && !this.workEditeur) return;
        for (PlayerConnection cl : this.clients.values()) {
            if (cl.isAfk && !cl.isDead) {
                this.sendDeath(cl);
            }
        }
        this.checkShouldChangeCarte();
    }

    public void setNameColor(PlayerConnection pc, int color) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeByte(29);
        p.writeByte(4);
        p.writeInt(pc.kullanici.code);
        p.writeInt(color);
        this.sendAll(p.buffer());
    }

    public void movePlayer(PlayerConnection pc, int x, int y, boolean positionOffset, int xSpeed, int ySpeed,
                           boolean speedOffset) throws IOException {
        pc.move(x, y, positionOffset, xSpeed, ySpeed, speedOffset);
    }

    public void movePlayer(PlayerConnection pc, int x, int y) throws IOException {
        pc.move(x, y, false, 0, 0, false);
    }

    public void checkVotes() {
        if (votes.size() >= 8 && clients.size() >= 8) {
            HashMap<Integer, Integer> res = new HashMap<>();
            votes.forEach((k, v) -> {
                res.put(v, res.getOrDefault(v, 0) + 1);
            });
            int oy = (int) (clients.size() / 0.8);
            res.forEach((k, v) -> {
                if (v >= oy) {
                    PlayerConnection pc = server.clients2.get(k);
                    if (pc != null) {
                        ArrayList<String> adlar = new ArrayList<>();
                        try {
                            sendMessage("<VP>User " + pc.kullanici.isim + " has been banned with " + v + " votes");
                            pc.enterRoom(this.server.recommendRoom(pc.Lang), false);
                            pc.sendChatServer(pc.kullanici.isim + " has been banned with " + v + " votes (" + Arrays.toString(adlar.toArray()) + ")");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        blockedPlayers.add(pc.kullanici.isim);
                        votes.clear();

                    }

                }
            });
        }
    }

    public static class FuncorpSettings {
        private final Room room;
        public HashMap<String, String> chatColors = new HashMap<>();
        public HashMap<Integer, Integer> miceSize = new HashMap<>();
        public HashMap<String, Integer> miceNickColors = new HashMap<>();
        public HashMap<String, String> miceNames = new HashMap<>();

        public FuncorpSettings(Room room) {
            this.room = room;
        }

        public static String packColor(String message, String color) {
            if (color.isEmpty()) return message;
            return "<font color=\"#" + color + "\">" + message + "</font>";
        }

        public void sendMessage(String name, String message) throws IOException {

            String color = this.chatColors.getOrDefault(name, "");
            name = this.room.funcorpSettings.miceNames.getOrDefault(name, name);
            room.sendMessage(packColor("[<a href=\"event:clicTexteNomJoueurEvent;joueur=" + name + "&online=true\" class=\"auteur\">" + name + "</a>] <n>", color) + message + "</n>");
        }

        public void sendSize(Integer code) throws IOException {
            Integer size = miceSize.get(code);
            if (size != null)
                this.room.sendAll(MFServer.getBuf().writeByte(5).writeByte(31).writeInt(code).writeShort(size).writeByte(0));
        }

        public void sendColor(PlayerConnection pc) throws IOException {
            Integer col = miceNickColors.get(pc.kullanici.isim);
            if (col != null) room.setNameColor(pc, col);
        }


    }

    public static class MusicVideo {
        public String title;
        public String kullanici;
        public String vid;
        public int sure;

        public MusicVideo(String kullanici, String vid, int sure, String title) {
            this.kullanici = kullanici;
            this.vid = vid;
            this.sure = sure;
            this.title = title;
        }
    }

}
