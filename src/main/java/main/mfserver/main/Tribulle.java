/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.data_out.ExampleTribulle;
import mfserver.data_out.map_err;
import mfserver.net.PlayerConnection;
import mfserver.util.BenchMark;
import mfserver.util.Dil;
import mfserver.util.Minify;
import mfserver.util.map;
import org.bson.Document;
import org.jooq.lambda.Unchecked;
import org.json.JSONArray;
import veritabani.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

/**
 * @author sevendr
 */
public class Tribulle {

    MFServer server;
    PlayerConnection client;
    long lastInvite;

    public Tribulle(PlayerConnection client) {
        this.client = client;
        this.server = client.server;
    }

    public void arkadasiYolla() throws IOException {
        MFServer.executor.submit(new BenchMark(() -> {
            try {
                this.client.Umay2();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "UMAY_2"));
    }

    public void kabileyiYolla(PlayerConnection client) throws IOException {
        MFServer.executor.submit(() -> {
            try {
                client.tribe.TribeInfo(client, client.acaba);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void kabiledenAtildi(String atan, String atilan, int atilan_id) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeUTF(atilan);
        p.writeUTF(atan);
        this.client.sendTriPacket2(p.buffer(), 93);
    }

    public void rutbeDegistir(String degistiren, String degistirilen, String rutbe, int degistirilen_id, int rank_id)
            throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeUTF(degistiren);
        p.writeUTF(degistirilen);
        p.writeUTF(rutbe);
        this.client.sendTriPacket2(p.buffer(), 124);
    }

    public void receivePM(String sender, String message, int senderLang) throws IOException {
        ByteBufOutputStream p = MFServer.getStream();
        p.writeUTF(sender);
        p.writeInt(senderLang + 1);
        p.writeUTF(MFServer.plusReady(client.kullanici));
        p.writeUTF(message);
        client.sendTriPacket2(p.buffer(), 66);

    }

    public void tribulle(int ID, ByteBufInputStream pbuf) throws IOException {
        // System.out.println(ID);
        final int reqID;
        if (!MFServer.dontcount.contains(ID)) {
            reqID = pbuf.readInt();
        } else {
            reqID = 0;
        }
        if (ID == 28) {// FriendList
            arkadasiYolla();
        } else if (ID == 108) {// Kabile Ekranı
            this.client.acaba = pbuf.readBoolean();
            this.client.tribeOpened = true;
            if (this.client.tribe != null)
                kabileyiYolla(client);
            else
                this.client.sendTriPacket2(new ExampleTribulle(reqID, 17).data(), 109);
        } else if (ID == 110) {// Close Tribe Menu
            this.client.tribeOpened = false;
        } else if (ID == 116) {
            if (client.tribe == null) return;
            if (this.client.tribe.checkPermissions("edit_ranks", client)) {

                int rank_pos = pbuf.readByte();
                String new_name = pbuf.readUTF();
                Rutbe rank = this.client.tribe.sortedRank.get(rank_pos);
                if (rank != null) {
                    rank.name = new_name;
                    MFServer.executor.execute(Unchecked.runnable(() -> {
                        rank.updateOne("code", "name");
                        this.client.tribe.sendAllTribe();
                    }));
                }
            }
        } else if (ID == 46) {// Send ignores

        } else if (ID == 122) {
            if (this.client.tribe == null)
                return;
            int old_pos = pbuf.readByte();
            int new_pos = pbuf.readByte();
            if (this.client.tribe.checkPermissions("edit_ranks", this.client)) {

                Rutbe rank = this.client.tribe.sortedRank.get(old_pos);
                Rutbe rank2 = this.client.tribe.sortedRank.get(new_pos);
                int size = this.client.tribe.Ranks.size();
                if (rank != null && rank2 != null && !rank.blocked && !rank2.blocked) {
                    rank.order = size - new_pos;
                    rank2.order = size - old_pos;
                    MFServer.executor.execute(Unchecked.runnable(() -> {
                        rank.updateOne("code", "order");
                        rank2.updateOne("code", "order");
                        this.client.tribe.sort_ranks();
                        this.client.tribe.sendAllTribe();
                    }));
                }
            }

        } else if (ID == 118) {
            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("edit_ranks", client)) {

                String name = pbuf.readUTF();
                if (this.client.tribe.Ranks.size() < 70) {
                    Rutbe rank = new Rutbe();
                    rank.tribeCode = client.tribe.Code;
                    rank.name = name;
                    rank.o_name = name;

                    rank.blocked = false;
                    rank.droits = new ArrayList<>();
                    for (int i = 0; i < 11; i++) {
                        rank.droits.add(false);
                    }
                    rank.order = client.tribe.Ranks.size();
                    MFServer.executor.execute(Unchecked.runnable(() -> {
                        rank.insertOne();
                        Rutbe newbie = client.tribe.newbie();
                        newbie.order++;
                        newbie.updateOne("code", "order");
                        this.client.sendTriPacket2(new ExampleTribulle(reqID, 1).data(), 119);
                        this.client.tribe.Ranks.put(rank.code, rank);
                        this.client.tribe.sort_ranks();
                        this.client.tribe.sendAllTribe();
                    }));


                }
            }
        } else if (ID == 120) {// Delete rank
            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("edit_ranks", client)) {
                int rank_pos = pbuf.readByte();

                Rutbe rank = this.client.tribe.sortedRank.get(rank_pos);
                Rutbe newbie = this.client.tribe.newbie();
                if (rank != null && !rank.blocked && newbie != null) {
                    this.client.tribe.Ranks.remove(rank.code);
                    this.client.tribe.sortedRank.remove(rank_pos);
                    /// newbie.set_order(newbie.order - 1);
                    MFServer.executor.execute(Unchecked.runnable(() -> rank.deleteOne("code")));
                    this.client.tribe.sort_ranks();
                    for (Kullanici user : this.client.tribe.Members.values()) {
                        if (user.tribeRank == rank.code) {

                            user.tribeRank = newbie.code;
                        }
                    }
                    for (PlayerConnection user : this.client.tribe.clients.values()) {
                        if (user.kullanici.tribeRank == rank.code) {
                            user.kullanici.tribeRank = newbie.code;
                        }
                    }
                    MFServer.executor.execute(Unchecked.runnable(() -> MFServer.colKullanici.updateMany(eq("tribeRank", rank.code), new Document("tribeRank", newbie.code))));
                    this.client.tribe.sendAllTribe();

                }
            }
        } else if (ID == 114) {
            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("edit_ranks", client)) {
                int rank_pos = pbuf.readByte();
                int droit_index = pbuf.readInt();
                boolean izin = !pbuf.readBoolean();
                // this.client.sendTriPacket2(new
                // ExampleTribulle(reqID,5).data(),115);

                Rutbe rank = this.client.tribe.sortedRank.get(rank_pos);
                if (rank != null) {
                    rank.droits.set(droit_index, izin);
                    MFServer.executor.execute(Unchecked.runnable(() -> rank.updateOne("code", "droits")));
                    // this.client.tribe.sort_ranks();
                    this.client.tribe.sendAllTribe();
                }

            }
        } else if (ID == 98) {
            if (this.client.tribe == null)
                return;
            int stat = 20;
            if (this.client.tribe.checkPermissions("message_edit", client)) {
                String mes = pbuf.readUTF();

                stat = 1;

                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 6, new Document("auteur", client.kullanici.isim))));
                this.client.tribe.kabile.ileti = mes;
                this.client.tribe.sendAllTribe();

                MFServer.executor.execute(Unchecked.runnable(() -> this.client.tribe.kabile.updateOne("code", "ileti")));
                // MFServer.instance.col_tribe.updateOne(new Document("_id",
                // this.client.tribe.Code), new Document("$set", new
                // Document("message", mes)), (UpdateResult u, Throwable th) ->
                // {});

            }

            this.client.sendTriPacket2(new ExampleTribulle(reqID, stat).data(), 99);
        } else if (ID == 60) {// Silence
            int typ = pbuf.readByte();
            String mes = pbuf.readUTF();
            if (mes.length() > 10 && client.check_message(mes))
                return;
            this.client.kullanici.sessiz = new Sessiz();
            this.client.kullanici.sessiz.ileti = mes;
            this.client.kullanici.sessiz.tip = typ;
            client.silence2 = false;
            this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, this.client).data(), 61);
            // MFServer.instance.col_tribe.updateOne(new Document("_id",
            // this.client.tribe.Code), new Document("$set", new
            // Document("message", mes)), (UpdateResult u, Throwable th) -> {});

        } else if (ID == 48) {
            if (this.client.room.isQuiz) return;
            String kanal_adi = pbuf.readUTF();
            String mes = pbuf.readUTF();
            mes = MFServer.patternRep.matcher(mes).replaceAll("");
            if (client.room.l_room != null&&client.room.l_room.disabled_names2.contains(client.kullanici.isim)) return;
            if (client.check_message(mes))
                return;
            for (Kanal kanal : client.kanallarim) {
                if (kanal.Name.equals(kanal_adi) && kanal.atabilir) {
                    kanal.sendMessage(mes, client);
                    break;
                }
            }

        } else if (ID == 50) {// tribe chat
            if (this.client.room.isQuiz) return;
            String mes = pbuf.readUTF();
            mes = MFServer.patternRep.matcher(mes).replaceAll("");
            if (client.room.l_room != null&&client.room.l_room.disabled_names2.contains(client.kullanici.isim)) return;
            if (client.check_message(mes))
                return;
            if (this.client.tribe != null) {
                ByteBufOutputStream bs = MFServer.getStream();
                bs.writeUTF(MFServer.plusReady(this.client.kullanici));
                bs.writeUTF(mes);
                this.client.tribe.kanal.sendAllTribulle(bs.buffer(), 65);
                MFServer.bitti(bs.buffer());
            }

        } else if (ID == 54) {// Join channel

            String name = pbuf.readUTF();
            int stat = 8;
            if (!name.contains("~") && name.length() > 2) {
                if (this.server.addClient2Channel(client, name, 0) != null) stat = 1;
            }
            this.client.sendTriPacket2(new ExampleTribulle(reqID, stat).data(), 55);

        } else if (ID == 58) {

            String kanal_adi = pbuf.readUTF();
            for (Kanal kanal : client.kanallarim) {
                if (kanal.Name.equals(kanal_adi) && kanal.Clients.containsKey(client.kullanici.code)) {
                    MFServer.executor.execute(() -> {
                        try {
                            ByteBufOutputStream p = MFServer.getStream();

                            p.writeInt(reqID);

                            p.writeByte(1);
                            p.writeShort(kanal.Clients.size());
                            for (Map.Entry<Integer, PlayerConnection> c : kanal.Clients.entrySet()) {
                                PlayerConnection cl = c.getValue();
                                if (cl == null) {
                                    p.writeUTF(MFServer.plusReady(MFServer.getIsim(c.getKey())));
                                } else {
                                    p.writeUTF(MFServer.plusReady(cl.kullanici));
                                }
                            }
                            client.sendTriPacket2(p.buffer(), 59);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    break;
                }
            }

        } else if (ID == 56) {
            String kanal_adi = pbuf.readUTF();
            for (Kanal kanal : client.kanallarim) {
                if (kanal.Name.equals(kanal_adi) && kanal.Clients.containsKey(client.kullanici.code)) {

                    client.sendTriPacket2(new ExampleTribulle(reqID, 1).data(), 57);
                    break;
                }
            }
        } else if (ID == 52) {
            if (this.client.room.isQuiz) return;
            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            //System.out.println(name);
            String message = MFServer.patternRep.matcher(pbuf.readUTF()).replaceAll("");
            String msg = message;
            MFServer.logExecutor.execute(() -> {
                MFServer.colPMLog.insertOne(new Document("from", client.kullanici.isim).append("to", name).append("room", client.room.name).append("msg", msg).append("time", System.currentTimeMillis()));

            });
            if (name.equals(client.kullanici.isim)) return;
            if (client.room.l_room != null&&client.room.l_room.disabled_names2.contains(client.kullanici.isim)) return;
            if (this.client.check_message(message))
                return;
            if (message.equals(client.last_message))
                return;
            if (message.length() > 50 && client.kullanici.birinci < 10_000) {
                message = message.substring(0, 50);
                client.sendMessage("You have less than <R>10k</R> firsts you can't use more than <R>50</R> characters.");
            }
            int stat = 1;
            String statMessage = "";
            client.last_message = message;
            PlayerConnection pc = this.server.clients.get(name);
            if (pc != null && (!(pc.topsecret||pc.hidden) || pc.friends.contains(client.kullanici.code))) {
                if (pc.kullanici.sessiz != null && pc.kullanici.sessiz.tip != 1) {
                    if (!(pc.kullanici.sessiz.tip == 2 && pc.friends.contains(client.kullanici.code))) {
                        String mes = "";
                        if (pc.kullanici.sessiz.ileti.length() > 0) {
                            mes += pc.kullanici.sessiz.ileti;
                        }
                        statMessage = mes;
                        stat = 25;
                    }
                }
                if (stat == 1) {
                    if (this.client.kullanici.engellenenler.contains(pc.kullanici.code)) {
                        stat = 27;
                    } else if (pc.kullanici.engellenenler.contains(client.kullanici.code)) {
                        stat = 12;
                    } else {
                        String from = MFServer.plusReady(client.kullanici).toLowerCase();
                        String to = MFServer.plusReady(pc.kullanici).toLowerCase();
                        ByteBufOutputStream p = MFServer.getStream();
                        p.writeUTF(from);
                        p.writeInt(pc.LangByte + 1);
                        p.writeUTF(to);
                        p.writeUTF(message);
                        this.client.sendTriPacket2(p.buffer(), 66);
                        pc.TRIBULLE.receivePM(from, message, client.LangByte);
                        if (from.equals(to)) System.out.println(from + " >  " + to + " " + name);

                    }
                }
            } else {
                stat = 12;
            }
            ByteBufOutputStream p = MFServer.getStream();
            p.writeInt(reqID);
            p.writeByte(stat);
            p.writeUTF(statMessage);
            client.sendTriPacket2(p.buffer(), 53);
            String m = message;
            if (pc == null) {
               /* MFServer.logExecutor.execute(()->{
                    int playerCode=MFServer.checkUserInDb(name);
                    if(playerCode!=0&&client.friends.contains(playerCode)){

                        MFServer.session.execute("insert into pm (kime,kimden,ileti,dil,surev) values (?,?,?,?,?) USING TTL 604800;",playerCode,client.kullanici.code,m,client.LangByte,System.currentTimeMillis());

                    }
                });*/
            }

        } else if (ID == 10) {
            int gender = pbuf.readByte();
            ByteBuf data = MFServer.getBuf();
            data.writeByte(gender);
            this.client.sendTriPacket2(data, 12);
            if (gender >= 0 && gender < 3) {
                this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, this.client).data(), 11);
                this.client.kullanici.cinsiyet = gender;
                MFServer.executor.execute(Unchecked.runnable(() -> this.client.kullanici.updateOne("code", "cinsiyet")));
            }
        } else if (ID == 22) {// Evlenme teklifi
            if (System.currentTimeMillis() - lastInvite < 1_000) {
                return;
            }
            lastInvite = System.currentTimeMillis();
            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            int stat = 1;
            if (name.equals(this.client.kullanici.isim))
                stat = 11;
            if (this.client.kullanici.ruhesi != 0)
                stat = 11;
            if (stat == 1) {
                PlayerConnection pc = this.server.clients.get(name);
                if (pc == null)
                    stat = 11;
                else {
                    int c1 = pc.kullanici.code;
                    if (pc.kullanici.ruhesi == 0 && !pc.kullanici.engellenenler.contains(this.client.kullanici.code)) {
                        if (!this.client.friends.contains(c1))
                            MFServer.executor.execute(Unchecked.runnable(() -> this.client.add_friend(c1)));
                        pc.invites2.add(new ArrayList() {
                            {
                                add(reqID);
                                add(client);

                            }
                        });
                        ByteBufOutputStream bs = MFServer.getStream();
                        bs.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
                        pc.sendTriPacket2(bs.buffer(), 38);
                    } else {
                        stat = 4;
                    }
                }
            }

            this.client.sendTriPacket2(new ExampleTribulle(reqID, stat, this.client).data(), 23);
        } else if (ID == 24) {// Evlenme cevabı
            String ad = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            int reply = pbuf.readByte();
            for (ArrayList al : this.client.invites2) {
                PlayerConnection askim = (PlayerConnection) al.get(1);
                if (askim.kullanici != null && MFServer.noTag(askim.kullanici.isim).equals(ad)) {
                    if (reply == 1) {

                        if (askim.kullanici.ruhesi != 0 || client.kullanici.ruhesi != 0) {
                            return;
                        }
                        int c1 = client.kullanici.code;
                        int c2 = askim.kullanici.code;
                        if (!askim.friends.contains(client.kullanici.code)) {
                            MFServer.executor.execute(Unchecked.runnable(() -> askim.add_friend(c1)));
                        }
                        if (!client.friends.contains(askim.kullanici.code)) {
                            MFServer.executor.execute(Unchecked.runnable(() -> client.add_friend(c2)));
                        }
                        askim.kullanici.ruhesi = client.kullanici.code;
                        client.kullanici.ruhesi = askim.kullanici.code;
                        ByteBufOutputStream bs = MFServer.getStream();
                        bs.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
                        askim.sendTriPacket2(bs.buffer(), 39);
                        bs = MFServer.getStream();
                        bs.writeUTF(MFServer.plusReady(askim.kullanici.isim, askim.kullanici.tag));
                        client.sendTriPacket2(bs.buffer(), 39);

                        MFServer.executor.execute(Unchecked.runnable(() -> {
                            askim.kullanici.updateOne("code", "ruhesi");
                            client.kullanici.updateOne("code", "ruhesi");
                            client.Umay();
                            askim.Umay();
                        }));

                        this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, this.client).data(), 25);
                    } else {
                        ByteBufOutputStream bs = MFServer.getStream();
                        bs.writeUTF(MFServer.plusReady(client.kullanici.isim, client.kullanici.tag));
                        askim.sendTriPacket2(bs.buffer(), 40);
                    }
                    askim.sendTriPacket2(new ExampleTribulle((int) al.get(0), reply, this.client).data(), 37);

                    break;
                }
            }
            client.invites2.clear();

        } else if (ID == 26) {// Ayril
            if (this.client.kullanici.ruhesi != 0) {

                PlayerConnection eskisi = server.clients2.get(this.client.kullanici.ruhesi);


                MFServer.executor.execute(Unchecked.runnable(() -> {
                    MFServer.colKullanici.updateOne(and(eq("_id", client.kullanici.ruhesi), eq("ruhesi", client.kullanici.code)), set("ruhesi", 0));
                    MFServer.colKullanici.updateOne(and(eq("_id", client.kullanici.code), eq("ruhesi", client.kullanici.ruhesi)), set("ruhesi", 0));
                    ByteBufOutputStream bs = MFServer.getStream();
                    bs.writeUTF(MFServer.plusReady(MFServer.getIsim(client.kullanici.ruhesi)));
                    bs.writeByte(2);
                    this.client.sendTriPacket2(bs.buffer(), 41);
                    client.kullanici.ruhesi = 0;
                    this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, this.client).data(), 27);
                }));
                if (eskisi != null) {
                    ByteBufOutputStream bs = MFServer.getStream();
                    bs.writeUTF(client.kullanici.isim);
                    bs.writeByte(1);
                    eskisi.sendTriPacket2(bs.buffer(), 41);
                    eskisi.kullanici.ruhesi = 0;
                }

            } else {
                this.client.sendTriPacket2(new ExampleTribulle(reqID, 20, this.client).data(), 27);

            }
        } else if (ID == 78) {
            if (System.currentTimeMillis() - lastInvite < 1_000) {
                return;
            }
            lastInvite = System.currentTimeMillis();
            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("can_invite", client)) {
                String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
                if (this.client.tribe.Members.size() >= 2000) {
                    return;
                }
                if (MFServer.verify_name(name)) {
                    name = MFServer.firstLetterCaps(name);
                    PlayerConnection cl = this.server.clients.get(name);

                    if (cl != null) {
                        if (cl.kullanici.engellenenler.contains(this.client.kullanici.code)) {
                            return;
                        }
                        if (cl.tribe != null) {
                            this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, this.client).data(), 79);

                        } else {
                            cl.invites.add(new ArrayList() {
                                {
                                    add(client.tribe.kabile.isim);
                                    add(reqID);
                                    add(client);
                                }
                            });
                            ByteBufOutputStream p = MFServer.getStream();
                            p.writeUTF(MFServer.plusReady(client.kullanici));
                            p.writeUTF(client.tribe.kabile.isim);
                            cl.sendTriPacket2(p.buffer(), 86);
                        }
                    }
                }
            }

        } else if (ID == 80) {
            String kullanici_adi = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            int reply = pbuf.readByte();
            if (this.client.tribe == null) {
                for (ArrayList invite : this.client.invites) {

                    PlayerConnection cl = (PlayerConnection) invite.get(2);
                    if (cl.kullanici != null && cl.kullanici.isim.equals(kullanici_adi)) {
                        if (cl.tribe != null && reply == 1) {
                            int rnk = cl.tribe.newbie().code;
                            int ti = MFServer.timestamp();
                            // this.server.col_tribulle.deleteOne(new
                            // Document("_id", this.client.kullanici.code), (DeleteResult
                            // dl, Throwable t) -> {

                            try {
                                // MFServer.instance.col_tribulle.insertOne(new
                                // Document("_id",
                                // this.client.kullanici.code).append("name",
                                // this.client.kullanici.isim).append("code",
                                // tcode).append("rank_id",
                                // rnk).append("join_time", ti), (final Void
                                // result, final Throwable th) -> {});
                                MFServer.executor.execute(Unchecked.runnable(() -> {
                                    client.kullanici.tribeRank = rnk;
                                    client.kullanici.tribeJoinTime = ti;
                                    client.kullanici.tribeCode = cl.tribe.Code;
                                    client.kullanici.updateOne("code", "tribeRank", "tribeJoinTime", "tribeCode");
                                    cl.tribe.Members.put(client.kullanici.code, client.kullanici);
                                    this.server.addClient2Tribe(this.client, cl.tribe.Code);

                                    this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 81);
                                    this.client.tribe.sendAllTribe();
                                    kabileyiYolla(client);

                                    MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 2, new Document("auteur", cl.kullanici.isim).append("membreAjoute", client.kullanici.isim))));

                                    ByteBufOutputStream bs = MFServer.getStream();
                                    bs.writeUTF(MFServer.plusReady(client.kullanici));
                                    client.tribe.sendAllTribulle(bs.buffer(), 91);

                                }));

                            } catch (Exception ex) {
                                Logger.getLogger(Tribulle.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else if (reply == 2) {
                            this.client.kullanici.engellenenler.add(cl.kullanici.code);
                        }
                        ByteBufOutputStream pf = MFServer.getStream();
                        pf.writeUTF(MFServer.plusReady(client.kullanici));
                        pf.writeByte(reply);
                        cl.sendTriPacket2(pf.buffer(), 87);
                        this.client.invites.remove(invite);
                        break;
                    }
                }
            }
        } else if (ID == 44) {
            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            MFServer.executor.execute(Unchecked.runnable(() -> {
                int code = MFServer.checkUserInDb(name);
                if (this.client.kullanici.engellenenler.contains(code)) {
                    this.client.kullanici.engellenenler.remove(code);

                    client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 45);

                } else {

                    //client.sendTriPacket2(new ExampleTribulle(reqID, 3, client).data(), 45);
                }
            }));

        } else if (ID == 42) {
            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            MFServer.executor.execute(Unchecked.runnable(() -> {
                int code = MFServer.checkUserInDb(name);
                if (name.equals(client.kullanici.isim) || client.friends.contains(code) || name.length() >= 20) {
                    client.sendTriPacket2(new ExampleTribulle(reqID, 4, client).data(), 43);
                    return;
                }
                int stat = 1;
                if (code != 0) {
                    if (this.client.kullanici.engellenenler.contains(code)) {
                        stat = 4;
                    } else {
                        if (this.client.kullanici.engellenenler.size() > 150) {
                            stat = 7;

                        } else {
                            if (this.client.friends.contains(code)) {
                                client.remove_friend(code);
                            }

                            this.client.kullanici.engellenenler.add(code);
                        }
                    }
                } else {
                    stat = 12;
                }
                client.sendTriPacket2(new ExampleTribulle(reqID, stat, client).data(), 43);
            }));


        } else if (ID == 102) {// Change tribe map
            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("can_edit_map", client)) {
                int mcode = pbuf.readInt();
                MFServer.executor.execute(() -> {
                    map m = Harita.al(mcode);
                    try {
                        if (m.id == 0) {
                            this.client.sendPacket(new map_err(this.client, 16).data());
                        } else if (m.perm != 22) {

                            this.client.sendPacket(new map_err(this.client, 17).data());
                        } else {
                            this.client.tribe.kabile.harita = m.id;
                            MFServer.executor.submit(Unchecked.runnable(() -> {
                                this.client.tribe.kabile.updateOne("code", "harita");

                            }));
                            // this.server.col_tribe.updateOne(new
                            // Document("_id", this.client.tribe.Code), new
                            // Document("$set", new Document("house", m.id)),
                            // (UpdateResult u, Throwable th) -> {});

                            MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 8, new Document("auteur", client.kullanici.isim).append("code", String.valueOf(mcode)))));

                            // ByteBufOutputStream p = MFServer.getStream();

                            // p.writeUTF(client.kullanici.isim);
                            // p.writeInt(m.id);
                            // this.client.tribe.sendAllTribulle("ET_SignaleChangementCodeMaisonTFM",
                            // p.buffer());

                        }
                    } catch (Exception x) {
                        MFServer.log.warning(MFServer.stackTraceToString(x));

                    }
                });

            }
        } else if (ID == 104) {
            if (this.client.tribe == null)
                return;
            int tcode = client.tribe.Code;
            if (this.client.tribe.checkPermissions("can_kick", client)) {
                String member_name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
                Optional<Kullanici> op = this.client.tribe.Members.values().stream()
                        .filter(k -> k.isim.equals(member_name)).findFirst();

                if (op.isPresent()) {
                    Kullanici us = op.get();
                    String name = us.isim;
                    if (client.tribe.Ranks.getOrDefault(us.tribeRank, client.tribe.newbie()).o_name.equals("${trad#TG_9}")) {
                        return;
                    }
                    // this.server.col_tribulle.deleteOne(new Document("_id",
                    // member_id), (DeleteResult dl, Throwable t) -> {});
                    MFServer.executor.submit(Unchecked.runnable(() -> {
                        us.tribeCode = 0;
                        us.tribeJoinTime = 0;
                        us.tribeRank = 0;
                        us.updateOne("code", "tribeRank", "tribeCode", "tribeJoinTime");

                    }));

                    MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(tcode, 3, new Document("auteur", client.kullanici.isim).append("membreExclu", us.isim))));

                    for (PlayerConnection pc : client.tribe.clients.values())
                        pc.TRIBULLE.kabiledenAtildi(MFServer.plusReady(client.kullanici), name, 0);

                    this.client.tribe.Members.remove(us.code);
                    PlayerConnection cl = this.server.clients.get(name);
                    if (cl != null) {
                        this.client.tribe.removeClient(cl);
                        cl.tribe = null;
                    }
                }
            }

        } else if (ID == 84) {
            String triName = pbuf.readUTF().replaceAll("\\<.*?>", "").replace("\\r", "").replace("\\t", "").trim();

            if (this.client.tribe != null)
                return;
            if (client.kullanici.banTribe > System.currentTimeMillis()) {
                client.sendMessage("<R>You have tribe ban :mad:");
                return;
            }
            if (client.susturma != null && client.susturma.surec > 0) {
                client.sendMessage("<R>You have been muted.");
                return;
            }

            if (!(triName.length() < 3 || triName.length() > 50)) {
                if (this.client.kullanici.marketPey >= 500) {
                    MFServer.executor.execute(() -> {

                        try {
                            int code = this.server.checkTribeInDb(triName);

                            if (code == 0) {

                                int ti = MFServer.timestamp();

                                JSONArray jsonArray = new JSONArray(
                                        "[[\"${trad#TG_9}\", true, 1, [false, true, true, true, true, true, true, true, true, true, true]], [\"${trad#TG_8}\", false, 2, [false, false, true, true, true, true, true, true, true, true, true]], [\"${trad#TG_7}\", false, 3, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_6}\", false, 4, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_5}\", false, 5, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_4}\", false, 6, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_3}\", false, 7, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_2}\", false, 8, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_1}\", false, 9, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#TG_0}\", false, 10, [false, false, false, false, false, false, false, false, false, false, false]], [\"${trad#tribu.nouv}\", true, 11, [false, false, false, false, false, false, false, false, false, false, false]]]");

                                Kabile kabile = new Kabile();
                                kabile.harita = 565296;
                                kabile.ileti = "Welcome to Miceforce ^_^";
                                kabile.isim = triName;
                                kabile.insertOne();
                                AtomicInteger ai = new AtomicInteger();

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONArray x = (JSONArray) jsonArray.get(i);

                                    String r_name = x.getString(0);
                                    boolean blocked = x.getBoolean(1);
                                    int ord = x.getInt(2);
                                    JSONArray j_droits = x.getJSONArray(3);
                                    ArrayList<Boolean> droits = new ArrayList<>();
                                    for (int i2 = 0; i2 < j_droits.length(); i2++) {
                                        droits.add(j_droits.getBoolean(i2));
                                    }

                                    Rutbe rank = new Rutbe();
                                    rank.name = r_name;
                                    rank.o_name = r_name;
                                    rank.order = ord;
                                    rank.droits = droits;
                                    rank.blocked = blocked;
                                    rank.tribeCode = kabile.code;
                                    rank.insertOne();
                                    if (ord == 1) {
                                        client.kullanici.tribeRank = rank.code;
                                        client.kullanici.tribeCode = kabile.code;
                                        client.kullanici.tribeJoinTime = ti;
                                        client.kullanici.updateOne("code", "tribeRank", "tribeCode", "tribeJoinTime");
                                    }


                                }
                                this.server.addClient2Tribe(this.client, kabile.code);


                                this.client.kullanici.marketPey -= 500;

                                this.client.sendTriPacket2(
                                        new ExampleTribulle(reqID, 1, client).data(), 85);


                                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 1, new Document("auteur", client.kullanici.isim).append("tribu", triName))));


                            } else {

                                this.client.sendTriPacket2(new ExampleTribulle(reqID, 8, client).data(), 85);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });


                } else {
                    this.client.sendTriPacket2(new ExampleTribulle(reqID, 22, client).data(), 85);

                }
            } else {
                this.client.sendTriPacket2(new ExampleTribulle(reqID, 7, client).data(), 85);

            }

        } else if (ID == 112) {// Change rank

            if (this.client.tribe == null)
                return;
            if (this.client.tribe.checkPermissions("edit_member_rank", client)) {
                String member_name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
                int rank_pos = pbuf.readByte();
                Optional<Kullanici> op = this.client.tribe.Members.values().stream()
                        .filter(k -> k.isim.equals(member_name)).findFirst();
                Rutbe rnk = this.client.tribe.sortedRank.get(rank_pos);
                if (op.isPresent() && rnk != null) {
                    Kullanici us = op.get();
                    String name = us.isim;
                    PlayerConnection cl = this.client.tribe.clients.get(name);
                    if (cl != null) {
                        cl.kullanici.tribeRank = rnk.code;
                    }
                    us.tribeRank = rnk.code;
                    // this.server.col_tribulle.updateOne(new Document("_id",
                    // member_id), new Document("$set", new Document("rank_id",
                    // rank_id)), (UpdateResult u, Throwable t) -> {});
                    MFServer.executor.execute(Unchecked.runnable(() -> us.updateOne("code", "tribeRank")));

                    for (PlayerConnection pc : client.tribe.clients.values())
                        pc.TRIBULLE.rutbeDegistir(MFServer.plusReady(client.kullanici), MFServer.plusReady(us.isim), rnk.name, us.code, rnk.code);

                    // this.client.tribe.sendAllTribulle("ET_SignaleChangementRang",
                    // p.buffer());
                    this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 113);
                    this.client.tribe.sendAllTribe();

                    MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 5, new Document("auteur", client.kullanici.isim).append("cible", us.isim).append("rang", rnk.name))));


                }
            }

        } else if (ID == 128) {// Dissolve
            if (this.client.tribe == null)
                return;
            Rutbe rnk = this.client.tribe.Ranks.get(this.client.kullanici.tribeRank);
            if (rnk != null) {
                if (rnk.o_name.equals("${trad#TG_9}")) {
                    int tcode = this.client.tribe.Code;
                    // this.server.col_tribe_ranks.deleteOne(new
                    // Document("tribe_code", tcode), (DeleteResult dl,
                    // Throwable t) -> {});
                    // this.server.coltribe_history.deleteOne(new
                    // Document("tribe_code", tcode), (DeleteResult dl,
                    // Throwable t) -> {
                    // });this.server.col_tribulle.deleteOne(new
                    // Document("code", tcode), (DeleteResult dl, Throwable t)
                    // -> {});this.server.col_tribe.deleteOne(new
                    // Document("_id", tcode), (DeleteResult dl, Throwable t) ->
                    // {});

                    this.server.closeTribe(tcode);

                    MFServer.executor.submit(() -> {
                        MFServer.colKabile.deleteOne(eq("_id", tcode));
                        MFServer.colRutbe.deleteMany(eq("tribeCode", tcode));
                    });
                    Tribe t = this.client.tribe;
                    // t.sendAllTribulle("ET_SignaleDissolutionTribu",
                    // MFServer.getBuf());
                    for (PlayerConnection cl : t.clients.values()) {
                        t.removeClient(cl);
                        cl.kullanici.tribeCode = 0;
                        cl.kullanici.tribeRank = 0;
                        cl.kullanici.tribeJoinTime = 0;
                        cl.tribe = null;
                    }
                    this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 129);

                }
            }

        } else if (ID == 82) {

            if (this.client.tribe != null
                    && !this.client.tribe.Ranks.get(this.client.kullanici.tribeRank).o_name.equals("${trad#TG_9}")) {

                // this.server.col_tribulle.deleteOne(new Document("_id",
                // this.client.kullanici.code), (DeleteResult dl, Throwable t) -> {});
                //ByteBufOutputStream p = MFServer.getStream();
                //p.writeInt(this.client.kullanici.code);
                // p.writeUTF(client.kullanici.isim);
                // MFServer.bitti(p.buffer());
                // this.client.tribe.sendAllTribulle("ET_SignaleDepartMembre",
                // p.buffer());

                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 4, new Document("membreParti", client.kullanici.isim))));


                this.client.tribe.Members.remove(this.client.kullanici.code);

                this.client.tribe.removeClient(client);
                ByteBufOutputStream bs = MFServer.getStream();
                bs.writeUTF(MFServer.plusReady(client.kullanici));
                client.tribe.sendAllTribulle(bs.buffer(), 92);
                client.kullanici.tribeCode = 0;
                client.tribe = null;
                client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 83);
                MFServer.executor.execute(Unchecked.runnable(() -> client.kullanici.updateOne("code", "tribeRank", "tribeCode", "tribeJoinTime")));


            } else {

                client.sendTriPacket2(new ExampleTribulle(reqID, 4, client).data(), 83);
            }

        } else if (ID == 18) {

            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            MFServer.executor.execute(Unchecked.runnable(() -> {
                int code = MFServer.checkUserInDb(name);
                if (name.equals(this.client.kullanici.isim)) {
                    client.sendTriPacket2(new ExampleTribulle(reqID, 4).data(), 19);
                    return;
                }


                if (code == 0 || code == -1) {

                    client.sendTriPacket2(new ExampleTribulle(reqID, 12).data(), 19);
                } else {
                    if (this.client.friends.contains(code)) {

                        client.sendTriPacket2(new ExampleTribulle(reqID, 4).data(), 19);
                    } else {
                        if (this.client.friends.size() > 500) {
                            client.sendTriPacket2(new ExampleTribulle(reqID, 7).data(), 19);
                        } else {
                            client.kullanici.engellenenler.remove(code);
                            client.sendTriPacket2(new ExampleTribulle(reqID, 1).data(), 19);
                            this.client.add_friend(code);
                            arkadasiYolla();
                            PlayerConnection connection = server.clients2.get(code);
                            if (connection != null) {
                                connection.FriendReq(client.kullanici.code);
                            }
                         /*   if (MFServer.colFriendReq.count(and(eq("p1", client.kullanici.code), eq("p2", code))) == 0) {
                                MFServer.colFriendReq.insertOne(new Document("p1", client.kullanici.code).append("p2", code));
                            }*/
                        }
                    }
                }
            }));


        } else if (ID == 20) {

            String name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            MFServer.executor.execute(Unchecked.runnable(() -> {
                int code = MFServer.checkUserInDb(name);
                if (!this.client.friends.contains(code)) {
                    client.sendTriPacket2(new ExampleTribulle(reqID, 6).data(), 21);
                    return;
                }

                if (this.client.friends.contains(code)) {

                    // ByteBufOutputStream pt = MFServer.getStream();
                    // pt.writeUTF(name);

                    client.sendTriPacket2(new ExampleTribulle(reqID, 1).data(), 21);
                    this.client.friends.remove(code);
                    this.client.remove_friend(code);
                    arkadasiYolla();
                }

            }));


        } else if (ID == 132) {// History
            if (this.client.tribe == null) return;
            int page = pbuf.readInt();
            int limit = pbuf.readInt();
            if (limit > 200) return;
            MFServer.executor.submit(() -> {
                try {
                    long count = MFServer.colKabileGecmisi.count(new Document("tribeCode", client.tribe.Code));

                    ByteBuf asil = MFServer.getBuf();
                    ByteBufOutputStream pt2 = MFServer.getStream();
                    asil.writeInt(reqID);
                    int sayi = 0;
                    Minify minify = new Minify();
                    for (KabileGecmisi kabileGecmisi : MFServer.colKabileGecmisi.find(new Document("tribeCode", client.tribe.Code)).sort(new Document("tarih", -1)).skip(page).limit(limit)) {
                        sayi += 1;
                        pt2.writeInt(kabileGecmisi.tarih);
                        pt2.writeInt(kabileGecmisi.tip);
                        String rang = kabileGecmisi.bilgi.getString("rang");
                        if (rang != null) {
                            rang = Dil.yazi(client.Lang, rang);
                            kabileGecmisi.bilgi.append("rang", rang);
                        }
                        String jj = minify.minify(kabileGecmisi.bilgi.toJson());
                        if (jj.length() > 10_000) jj = jj.substring(0, Math.min(jj.length(), 10_000));
                        pt2.writeUTF(jj);
                    }
                    asil.writeShort(sayi);
                    asil.writeBytes(pt2.buffer());
                    asil.writeInt((int) count);

                    client.sendTriPacket2(asil, 133);

                    MFServer.bitti(pt2.buffer());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        } else if (ID == 126) {// Change chief
            if (this.client.tribe == null) return;
            String member_name = MFServer.noTag(MFServer.firstLetterCaps(pbuf.readUTF()));
            Optional<Kullanici> op = this.client.tribe.Members.values().stream().filter(k -> k.isim.equals(member_name))
                    .findFirst();
            Rutbe rnk = client.tribe.chef();
            Rutbe newbie = client.tribe.newbie();
            if (op.isPresent() && rnk.code == client.kullanici.tribeRank) {
                Kullanici us = op.get();
                String name = us.isim;
                // this.server.col_tribulle.updateOne(new Document("_id",
                // member_id), new Document("$set", new Document("rank_id",
                // rank_id)), (UpdateResult u, Throwable t) -> {});
                us.tribeRank = rnk.code;
                client.kullanici.tribeRank = newbie.code;
                MFServer.executor.execute(Unchecked.runnable(() -> {
                    us.updateOne("code", "tribeRank");
                    client.kullanici.updateOne("code", "tribeRank");
                }));

                PlayerConnection cl = this.client.tribe.clients.get(name);

                if (cl != null) {
                    cl.kullanici.tribeRank = rnk.code;
                }
                this.client.tribe.Members.get(client.kullanici.code).tribeRank = newbie.code;

                ByteBufOutputStream p = MFServer.getStream();
                p.writeUTF(MFServer.plusReady(client.kullanici));
                p.writeUTF(MFServer.plusReady(us));
                p.writeUTF(rnk.name);
                this.client.tribe.sendAllTribulle(p.buffer(), 124);
                p = MFServer.getStream();
                p.writeUTF(MFServer.plusReady(client.kullanici));
                p.writeUTF(MFServer.plusReady(client.kullanici));
                p.writeUTF(newbie.name);
                this.client.tribe.sendAllTribulle(p.buffer(), 124);
                // this.client.tribe.sendAllTribulle("ET_SignaleChangementRang",
                // p.buffer());
                this.client.sendTriPacket2(new ExampleTribulle(reqID, 1, client).data(), 127);
                this.client.tribe.sendAllTribe();

                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 5, new Document("auteur", client.kullanici.isim).append("cible", us.isim).append("rang", rnk.name))));

                MFServer.executor.execute(() -> MFServer.colKabileGecmisi.insertOne(new KabileGecmisi(client.tribe.Code, 5, new Document("auteur", client.kullanici.isim).append("cible", client.kullanici.isim).append("rang", newbie.name))));


            }
        }

    }

}
