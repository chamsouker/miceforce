package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.jooq.lambda.Unchecked;

import java.io.IOException;

public class task extends temel {
    public task(){
        super("task");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        MFServer.executor.execute(Unchecked.runnable(() -> {
            client.gorevEkrani(1,0);
        }));
    }
}
