package mfserver.komutlar;

import mfserver.net.PlayerConnection;

import java.io.IOException;

public class crown extends temel {
    public crown(){
        super("crown");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        client.crown = !client.crown;
        if (client.crown) {
            client.sendMessage("<VP>Crown enabled.");
        } else {
            client.sendMessage("<R>Crown disabled.");
        }
    }
}
