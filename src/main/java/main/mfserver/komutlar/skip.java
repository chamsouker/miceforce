package mfserver.komutlar;

import mfserver.data_out.login_1;
import mfserver.net.PlayerConnection;
import veritabani.Kullanici;

import java.io.IOException;

import static mfserver.net.PlayerConnection.safeTag;

public class skip extends temel {
    public skip() {
        super("skip");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {

        if (client.room.isBootcamp) return;
        if (client.room.isMinigame && client.room.l_room != null && !client.room.l_room.developer.equals("an"))
            return;
        if (System.currentTimeMillis() - client.room.lastSkip < 1000 * 60 * 10) return;
        if (client.room.playerCount() < 2) return;
        client.room.videoVotes.add(client.kullanici.isim);
        double have = client.room.videoVotes.size();
        double req = ((double) client.room.playerCount()) / 1.3;
        if (have / req > 0.5 && have < req && !client.room.videoVotes.contains(client.kullanici.isim)) {
            client.room.iletiYolla("vote_stats", (int) have, (int) req);
        } else if ((int)have >= (int)req) {
            if (client.room.isMusic) {

                client.room.playNext();
                client.room.sendMessage("<VP>Music skipped");
            } else {
                client.room.lastSkip = System.currentTimeMillis();
                client.room.videoVotes.clear();
                client.room.NewRound();
                client.room.sendMessage("<VP>Round has been skipped.");
            }
        } else {
            client.iletiYolla("vote_stats", (int) have, (int) req);
        }


    }
}
