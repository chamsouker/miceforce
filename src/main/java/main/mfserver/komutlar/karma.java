package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;

public class karma extends temel {
    public karma(){
        super("karma");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (args.length == 1 && client.kullanici.yetki >= 2) {
            String name = MFServer.noTag(MFServer.firstLetterCaps(args[0]));
            PlayerConnection pc = client.server.clients.get(name);
            if (pc != null) {
                client.sendMessage("<N>" + name + "'s karma : " + pc.karma);

            } else {

                client.sendMessage("<R>$Joueur_Existe_Pas");
            }
            return;
        }
        client.sendMessage("<N>Karma : " + client.karma);
    }

    public String getDoc(PlayerConnection client){
        if(client.kullanici.yetki>=2)return "command_karma2";
        return doc;
    }
}
