/**
 * *****************************************************************************
 * Copyright (c) 2009-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import mfserver.data_out.AnimZelda;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.luaj.vm2.*;

import java.io.IOException;


public class ExecLib extends TwoArgFunction {

    private L_Room room;

    /**
     * Construct a StringLib, which can be initialized by calling it with a
     * modname string, and a global environment table as arguments using
     * {@link #call(LuaValue, LuaValue)}.
     */
    public ExecLib(L_Room room) {
        this.room = room;
    }

    public static int parseDouble(double num) {
        if (num >= -1 && num <= 1) return ((Number) (num * 100)).intValue();
        return ((Number) num).intValue();
    }

    public LuaValue call(LuaValue modname, LuaValue env) {

        LuaTable string = new LuaTable();
        string.set("addConjuration", new addConjuration());
        string.set("newGame", new newGame());
        string.set("playEmote", new playEmote());
        string.set("playEmote2", new playEmote2());
        string.set("disableMinimalistMode", new disableMinimalistMode());
        string.set("disableDebugCommand", new disableDebugCommand());
        string.set("disableWatchCommand", new disableWatchCommand());
        string.set("disableAutoNewGame", new disableAutoNewGame());
        string.set("disableAutoShaman", new disableAutoShaman());
        string.set("bindKeyboard", new bindKeyboard());
        string.set("addShamanObject", new addShamanObject());
        string.set("chatMessage", new chatMessage());
        string.set("removeObject", new removeObject());
        string.set("setGameTime", new setGameTime());
        string.set("addJoint", new addJoint());
        string.set("addPhysicObject", new addPhysicObject());
        string.set("playerVictory", new playerVictory());
        string.set("removePhysicObject", new removePhysicObject());
        string.set("giveCheese", new giveCheese());
        string.set("removeCheese", new removeCheese());
        string.set("setNameColor", new setNameColor());
        string.set("setUIMapName", new setUIMapName());
        string.set("disableAutoTimeLeft", new disableAutoTimeLeft());
        string.set("disableAfkDeath", new disableAfkDeath());
        string.set("disableAutoScore", new disableAutoScore());
        string.set("giveMeep", new giveMeep());
        string.set("disableAllShamanSkills", new disableAllShamanSkills());
        string.set("respawnPlayer", new respawnPlayer());
        string.set("movePlayer", new movePlayer());
        string.set("killPlayer", new killPlayer());
        string.set("killPlayerById", new killPlayerById());
        string.set("displayParticle", new displayParticle());
        string.set("explosion", new explosion());
        string.set("setPlayerScore", new setPlayerScore());
        string.set("addImage", new addImage());
        string.set("removeImage", new removeImage());
        string.set("snow", new snow());
        string.set("setVampirePlayer", new setVampirePlayer());
        string.set("setShaman", new setShaman());
        string.set("nextShaman", new nextShaman());
        string.set("disablePhysicalConsumables", new disablePhysicalConsumables());
        string.set("animZelda", new animZelda());
        string.set("changeSize", new changeSize());
        string.set("changeName", new changeName());
        string.set("setRoomMaxPlayers", new setRoomMaxPlayers());
        string.set("setRoomPassword", new setRoomPassword());
        string.set("disableChat", new disableChat());
        string.set("disableChat2", new disableChat2());
        string.set("addCollectable", new addCollectable());
        string.set("removeCollectable", new removeCollectable());
        string.set("setSoulmate", new setSoulmate());
        string.set("giveWings", new giveWings());
        string.set("playerCat", new playerCat());
        string.set("grapnel", new grapnel());
        string.set("setShamanMode", new setSamanMode());
        string.set("fishRod", new fishRod());
        string.set("disableIsimRenk", new disableIsimRenk());
        env.get("tfm").set("exec", string);
        return env;
    }

    final class fishRod extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim1 = args.checkjstring(1);

                boolean yes = args.optboolean(2, true);
                room.fishRod(isim1, yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableMinimalistMode extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);
                room.D_minimalist = yes;
                room.sendDisableUclu();
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableWatchCommand extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);
                room.D_watch = yes;
                room.sendDisableUclu();
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableDebugCommand extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                room.D_debug = args.optboolean(1, true);
                room.sendDisableUclu();
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setSamanMode extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim1 = args.checkjstring(1);

                int typ = args.checkint(2);
                room.setSamanMode(isim1, typ);

            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveWings extends VarArgFunction {

        public Varargs invoke(Varargs args) {

            String isim = args.checkjstring(1);
            boolean ac = args.checkboolean(2);
            room.giveWings(isim, ac);

            return LuaValue.NIL;
        }
    }

    final class playerCat extends VarArgFunction {

        public Varargs invoke(Varargs args) {

            String isim = args.checkjstring(1);
            boolean ac = args.checkboolean(2);
            room.playerCat(isim, ac);

            return LuaValue.NIL;
        }
    }

    final class grapnel extends VarArgFunction {

        public Varargs invoke(Varargs args) {

            String isim = args.checkjstring(1);
            int x = args.checkint(2);
            int y = args.checkint(3);
            room.grapnel(isim, x, y);

            return LuaValue.NIL;
        }
    }

    final class setSoulmate extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String isim1 = args.checkjstring(1);
                if (args.isstring(2)) {
                    String isim2 = args.checkjstring(2);
                    room.setSoulmate(isim1, isim2);
                } else {

                    room.removeSoulmate(isim1);
                }
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addCollectable extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                int cap = args.checkint(2);
                int x = args.checkint(3);
                int y = args.checkint(4);
                String name = args.optjstring(5, "");
                room.addCollectable(id, cap, x, y, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class removeCollectable extends VarArgFunction {
        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String name = args.optjstring(2, "");
                room.removeCollectable(id, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class disableChat2 extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                boolean disable = args.optboolean(2, true);
                room.disableChat2(name, disable);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableIsimRenk extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean disable = args.optboolean(1, true);
                room.disableIsimRenk(disable);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class disableChat extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                boolean disable = args.optboolean(2, true);
                room.disableChat(name, disable);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setShaman extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                boolean disable = args.optboolean(2, false);
                boolean all = args.optboolean(3, true);
                room.setShaman(name, disable, all);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setRoomMaxPlayers extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int max = args.checkint(1);
                if (max > 1) {
                    room.room.maxPlayers = max;
                }
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setRoomPassword extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String pw = args.checkjstring(1);
                room.room.password = pw;

            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class changeName extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String oldName = args.checkjstring(1);
                String newName = args.checkjstring(2);
                //System.out.println(room.global.hashCode());
                room.changeName(oldName, newName);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class changeSize extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String playerName = args.checkjstring(1);
                int size = args.checkint(2);
                room.changeSize(playerName, size);
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }

    final class animZelda extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String playerName = args.checkjstring(1);
                int cat = args.optint(2, 0);
                int id = args.optint(2, 0);
                //System.out.println(room.global.hashCode());
                PlayerConnection pc = room.room.clients.get(playerName);

                if (pc != null) room.room.sendAll(new AnimZelda(pc, MFServer.get_id_by_cat(cat, id)).data());
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }

    final class playEmote extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String playerName = args.checkjstring(1);
                int emoteId = args.checkint(2);
                String emoteArg = args.optjstring(3, "");
                //System.out.println(room.global.hashCode());
                room.playEmote(playerName, emoteId, emoteArg);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class playEmote2 extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int pcode = args.checkint(1);
                int emoteId = args.checkint(2);
                String emoteArg = args.optjstring(3, "");
                //System.out.println(room.global.hashCode());
                room.playEmote2(pcode, emoteId, emoteArg);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class nextShaman extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                room.nextShaman(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setVampirePlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                room.room.setVampirePlayer(name);
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }


    final class snow extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int duration = args.optint(1, 60);
                int power = args.optint(2, 10);
                room.snow(true, duration, power);
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }

    final class removeImage extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int code = args.checkint(1);
                String name = args.optjstring(2, "");
                room.removeImage(code, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addImage extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String imageId = args.checkjstring(1);
                String target = args.checkjstring(2);
                int xPosition = args.optint(3, 0);
                int yPosition = args.optint(4, 0);
                String targetPlayer = args.optjstring(5, "");
                int id = args.optint(6, 0);


                return LuaInteger.valueOf(room.addImage(imageId, target, xPosition, yPosition, targetPlayer, id));
            } catch (LuaError er) {
                er.printStackTrace();
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setPlayerScore extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String playerName = args.checkjstring(1);
                int score = args.checkint(2);
                boolean add = args.optboolean(3, false);


                room.setPlayerScore(playerName, score, add);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class explosion extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int xPosition = args.checkint(1);
                int yPosition = args.checkint(2);

                int power = args.checkint(3);
                int radius = args.checkint(4);
                boolean miceOnly = args.optboolean(5, false);


                room.explosion(xPosition, yPosition, power, radius, miceOnly);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class displayParticle extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int particleType = args.checkint(1);
                int xPosition = args.checkint(2);
                int yPosition = args.checkint(3);

                int xSpeed = parseDouble(args.optdouble(4, 0));
                int ySpeed = parseDouble(args.optdouble(5, 0));
                int xAcceleration = parseDouble(args.optdouble(6, 0));//parseLuaFloat(args.optjstring(6, "0"));
                int yAcceleration = parseDouble(args.optdouble(7, 0));
                String targetPlayer = args.optjstring(8, "");
                room.displayParticle(particleType, xPosition, yPosition, xSpeed, ySpeed, xAcceleration, yAcceleration, targetPlayer);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class killPlayerById extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int name = args.checkint(1);
                room.killPlayerById(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class killPlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);


                room.killPlayer(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class movePlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                int x = args.checkint(2);
                int y = args.checkint(3);
                boolean positionOffset = args.optboolean(4, false);
                int xSpeed = args.optint(5, 0);
                int ySpeed = args.optint(6, 0);
                boolean speedOffset = args.optboolean(7, false);


                room.movePlayer(name, x, y, positionOffset, xSpeed, ySpeed, speedOffset);
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }

    final class respawnPlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);

                room.respawnPlayer(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveMeep extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);

                room.giveMeep(name);
            } catch (LuaError er) {
                room.sendError(er);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return LuaValue.NIL;
        }
    }

    final class disableAutoTimeLeft extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAutoTimeLeft(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disablePhysicalConsumables extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disablePhysicalConsumables(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableAfkDeath extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAfkDeath(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableAutoScore extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAutoScore(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableAllShamanSkills extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAllShamanSkills(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setUIMapName extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String message = args.checkjstring(1);

                room.setUIMapName(message);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class playerVictory extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);

                room.playerVictory(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveCheese extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);

                room.giveCheese(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class removeCheese extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);

                room.removeCheese(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setNameColor extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                int color = args.checkint(2);
                room.setNameColor(name, color);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addPhysicObject extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                int x = args.checkint(2);
                int y = args.checkint(3);
                LuaTable bodyDef = args.checktable(4);
                String target = args.optjstring(5,"");
                room.addPhysicObject(id, x, y, bodyDef, target);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class removePhysicObject extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                String target = args.optjstring(2,"");
                room.removePhysicObject(id, target);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addJoint extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                int ground1 = args.checkint(2);
                int physicObject2 = args.checkint(3);
                LuaTable jointDef = args.checktable(4);
                room.addJoint(id, ground1, physicObject2, jointDef);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class setGameTime extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int time = args.checkint(1);
                boolean nit = args.optboolean(2, true);
                room.setGameTime(time, nit);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class removeObject extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int code = args.checkint(1);
                room.removeObject(code);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class chatMessage extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String message = args.checkjstring(1);
                String name = args.optjstring(2, "");
                room.chatMessage(message, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class addShamanObject extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int typ = args.checkint(1);
                int x = args.checkint(2);
                int y = args.checkint(3);
                int angle = args.optint(4, 0);
                int vx = args.optint(5, 0);
                int vy = args.optint(6, 0);
                boolean ghost = args.optboolean(7, false);
                String target = args.optjstring(8, "");
                return LuaInteger.valueOf(room.addShamanObject(typ, x, y, angle, vx, vy, !ghost, target));
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    /**
     * Int xPosition, Int yPosition, Int timeInMillis
     */
    final class addConjuration extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int x = args.checkint(1);
                int y = args.checkint(2);
                int millis = args.checkint(3);
                room.addConjuration(x, y, millis);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class newGame extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String code = args.optjstring(1, "null");
                boolean hemen = args.optboolean(2, true);
                int perm = args.optint(3, -1);
                String name = args.optjstring(4, "_Minigame");
                //System.out.println(room.global.hashCode());
                room.newGame(code, hemen, perm, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class disableAutoNewGame extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAutoNewGame(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableAutoShaman extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean yes = args.optboolean(1, true);

                room.disableAutoShaman(yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class bindKeyboard extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                int code = args.checkint(2);
                boolean down = args.checkboolean(3);
                boolean yes = args.optboolean(4, true);

                room.bindKeyboard(name, code, down, yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

}
