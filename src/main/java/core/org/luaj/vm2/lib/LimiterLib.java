/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luaj.vm2.lib;

/**
 * @author sevendr
 */

import org.luaj.vm2.Varargs;

public class LimiterLib extends DebugLib {

    public L_Room room;


    public LimiterLib(L_Room room) {
        this.room = room;
    }

    @Override
    public void onInstruction(int pc, Varargs v, int top) {
        if (room.get_interrupted()) {
            throw new ScriptInterruptException();
        }
        super.onInstruction(pc, v, top);
    }

    public static class ScriptInterruptException extends RuntimeException {
    }
}
