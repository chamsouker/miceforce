/**
 * *****************************************************************************
 * Copyright (c) 2010-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import org.luaj.vm2.*;

/**
 * Subclass of {@link LibFunction} which implements the lua standard package and
 * module library functions.
 * <p>
 * <h3>Lua Environment Variables</h3>
 * The following variables are available to lua scrips when this library has
 * been loaded:
 * <ul>
 * <li><code>"package.loaded"</code> Lua table of loaded modules.
 * <li><code>"package.path"</code> Search path for lua scripts.
 * <li><code>"package.preload"</code> Lua table of uninitialized preload
 * functions.
 * <li><code>"package.searchers"</code> Lua table of functions that search for
 * object to load.
 * </ul>
 * <p>
 * <h3>Java Environment Variables</h3>
 * These Java environment variables affect the library behavior:
 * <ul>
 * <li><code>"luaj.package.path"</code> Initial value for
 * <code>"package.path"</code>. Default value is <code>"?.lua"</code>
 * </ul>
 * <p>
 * <h3>Loading</h3>
 * Typically, this library is included as part of a call to either
 * {@link org.luaj.vm2.lib.jse.JsePlatform#standardGlobals()} or
 * {@link org.luaj.vm2.lib.jme.JmePlatform#standardGlobals()}
 * <pre> {@code
 * Globals globals = JsePlatform.standardGlobals();
 * System.out.println( globals.get("require").call"foo") );
 * } </pre>
 * <p>
 * To instantiate and use it directly, link it into your globals table via
 * {@link LuaValue#load(LuaValue)} using code such as:
 * <pre> {@code
 * Globals globals = new Globals();
 * globals.load(new JseBaseLib());
 * globals.load(new PackageLib());
 * System.out.println( globals.get("require").call("foo") );
 * } </pre>
 * <h3>Limitations</h3>
 * This library has been implemented to match as closely as possible the
 * behavior in the corresponding library in C. However, the default filesystem
 * search semantics are different and delegated to the bas library as outlined
 * in the {@link BaseLib} and {@link org.luaj.vm2.lib.jse.JseBaseLib}
 * documentation.
 * <p>
 *
 * @see LibFunction
 * @see BaseLib
 * @see org.luaj.vm2.lib.jse.JseBaseLib
 * @see org.luaj.vm2.lib.jse.JsePlatform
 * @see org.luaj.vm2.lib.jme.JmePlatform
 * @see <a href="http://www.lua.org/manual/5.2/manual.html#6.3">Lua 5.2 Package
 * Lib Reference</a>
 */
public class tfm extends TwoArgFunction {

    /**
     * The default value to use for package.path. This can be set with the
     * system property <code>"luaj.package.path"</code>, and is
     * <code>"?.lua"</code> by default.
     */
    /**
     * The globals that were used to load this library.
     */
    Globals globals;

    /**
     * The table for this package.
     */
    LuaTable package_;

    private L_Room room;

    public tfm(L_Room room) {

        this.room = room;
    }

    /**
     * Perform one-time initialization on the library by adding package
     * functions to the supplied environment, and returning it as the return
     * value. It also creates the package.preload and package.loaded tables for
     * use by other libraries.
     *
     * @param modname the module name supplied if this is loaded via 'require'.
     * @param env     the environment to load into, typically a Globals instance.
     */
    public LuaValue call(LuaValue modname, LuaValue env) {

        package_ = new LuaTable();
        env.set("tfm", package_);
        env.set("print", new print());

        return env;
    }

    final class print extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String mes = args.tojstring();
                room.sendLuaMessage(mes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

}
